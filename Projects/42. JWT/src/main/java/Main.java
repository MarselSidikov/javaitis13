import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;

/**
 * 15.09.2021
 * 42. JWT
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main {
    public static void main(String[] args) {
        User user = new User(10L, "sidikov.marsel@gmail.com", "ADMIN", "CONFIRMED");

        String token = JWT.create()
                .withSubject(user.getId().toString())
                .withClaim("email", user.getEmail())
                .withClaim("role", user.getRole())
                .withClaim("state", user.getState())
                .sign(Algorithm.HMAC256("simple_secret_key"));

        System.out.println(token);
    }
}

// eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiIxMCIsInJvbGUiOiJBRE1JTiIsInN0YXRlIjoiQ09ORklSTUVEIiwiZW1haWwiOiJzaWRpa292Lm1hcnNlbEBnbWFpbC5jb20ifQ.qdGX85PoI2ZdR7-RJ-knOBvrb-4D_1zBQz9MnFZQB1w