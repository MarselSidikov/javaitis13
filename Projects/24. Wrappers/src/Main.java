public class Main {

    public static void main(String[] args) {
        // boxing
	    // Integer i1 = new Integer(10);
	    // unboxing
	    // int i2 = i1.intValue();

        // autoboxing
        Integer i1 = 10;
        // autounboxing
        int i2 = i1;
    }
}
