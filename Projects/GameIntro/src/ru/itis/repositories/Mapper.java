package ru.itis.repositories;

/**
 * 17.04.2021
 * GameIntro
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public interface Mapper<T, V> {
    V map(T object);
}
