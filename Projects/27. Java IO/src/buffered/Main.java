package buffered;

import java.io.*;

/**
 * 17.04.2021
 * 27. Java IO
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main {
    public static void main(String[] args) throws Exception {
        Reader reader = new FileReader("input.txt");
        BufferedReader bufferedReader = new BufferedReader(reader);

        Writer writer = new FileWriter("output.txt");
        BufferedWriter bufferedWriter = new BufferedWriter(writer);
        writer.write("Hello!");
    }
}
