package output.stream;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;

/**
 * 17.04.2021
 * 27. Java IO
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main1 {
    public static void main(String[] args) throws Exception {
        OutputStream outputStream = new FileOutputStream("output.txt", true);
        outputStream.write("Marsel".getBytes());
        outputStream.close();
    }
}
