package input.stream;

import java.io.FileInputStream;
import java.io.InputStream;

/**
 * 17.04.2021
 * 27. Java IO
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main3 {
    public static void main(String[] args) throws Exception {
        InputStream inputStream;
        inputStream = new FileInputStream("input.txt");
        byte bytes[] = new byte[3];
        int length = inputStream.read(bytes);
        System.out.println(length);
        System.out.println(new String(bytes));
    }
}
