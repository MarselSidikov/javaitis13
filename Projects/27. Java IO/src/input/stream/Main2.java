package input.stream;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Arrays;

/**
 * 17.04.2021
 * 27. Java IO
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main2 {
    public static void main(String[] args) throws Exception {
        InputStream inputStream;
        inputStream = new FileInputStream("input.txt");
        int bytes[] = new int[20];

        int currentByte = inputStream.read();
        int i = 0;
        // пока не дошли до конца потока
        while (currentByte != -1) {
            // запоминаю считанный байт как элемент массива int
            bytes[i] = currentByte;
            // считываю новый байт как число int
            currentByte = inputStream.read();
            i++;
        }
        // создаю массив байтов, размер которого совпадает с количеством считанных байтов
        byte realBytes[] = new byte[i];
        // конвертирую int в byte
        for (i = 0 ; i < realBytes.length; i++) {
            realBytes[i] = (byte) bytes[i];
        }
        System.out.println(Arrays.toString(realBytes));
        // создаю строку на основе байтов
        String value = new String(realBytes);
        System.out.println(value);
        System.out.println((byte)(255));
    }
}
