package reader;

import java.io.FileReader;
import java.io.Reader;
import java.nio.CharBuffer;

/**
 * 17.04.2021
 * 27. Java IO
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main1 {
    public static void main(String[] args) throws Exception {
        Reader reader = new FileReader("input.txt");
        int code = reader.read();
        while (code != -1) {
            System.out.print((char)code);
            code = reader.read();
        }
    }
}
