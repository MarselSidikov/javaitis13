package writer;

import java.io.File;
import java.io.FileWriter;
import java.io.Writer;

/**
 * 17.04.2021
 * 27. Java IO
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main {
    public static void main(String[] args) throws Exception {
        Writer writer = new FileWriter("output.txt");
        writer.write("Привет!");
        writer.close();
    }
}
