package ru.itis.site.utils;

/**
 * 03.07.2021
 * 40. Spring Boot
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public interface EmailUtil {
    void sendMail(String text, String subject, String to);
}
