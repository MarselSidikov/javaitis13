package ru.itis.site.services;

/**
 * 03.07.2021
 * 40. Spring Boot
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public interface ConfirmService {
    boolean confirm(String confirmId);
}
