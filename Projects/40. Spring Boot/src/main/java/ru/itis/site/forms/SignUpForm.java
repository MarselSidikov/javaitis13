package ru.itis.site.forms;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.itis.site.validation.NotSameNames;
import ru.itis.site.validation.ValidPassword;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

/**
 * 05.06.2021
 * 36. Java Servlet Application
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@NotSameNames(firstNameField = "firstName", lastNameField = "lastName", message = "{error.names.bad}")
public class SignUpForm {

    @Size(min = 4, max = 20)
    private String firstName;
    @Size(min = 4, max = 20)
    private String lastName;

    @Email(message = "{error.email.format}")
    private String email;

    @ValidPassword(message = "{error.password.format}")
    @NotEmpty(message = "{error.password.empty}")
    private String password;
}
