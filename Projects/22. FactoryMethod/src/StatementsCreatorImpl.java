/**
 * 13.03.2021
 * 22. FactoryMethod
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class StatementsCreatorImpl implements DocumentsCreator {

    @Override
    public Document createDocument(String text) {
        return new Statement("ТЕКСТ ЗЯВЛЕНИЯ: " + text);
    }
}
