/**
 * 13.03.2021
 * 22. FactoryMethod
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Certificate implements Document {
    private String text;

    public Certificate(String text) {
        this.text = text;
    }

    @Override
    public String getTitle() {
        return "<СПРАВКА>";
    }

    @Override
    public String getText() {
        return text;
    }
}
