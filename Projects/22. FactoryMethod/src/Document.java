/**
 * 13.03.2021
 * 22. FactoryMethod
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public interface Document {
    String getTitle();
    String getText();
}
