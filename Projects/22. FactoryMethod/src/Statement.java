import javax.print.Doc;
import java.time.LocalDate;

/**
 * 13.03.2021
 * 22. FactoryMethod
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Statement implements Document {

    private String text;

    public Statement(String text) {
        this.text = text;
    }

    @Override
    public String getTitle() {
        return "<ЗАЯВЛЕНИЕ от " + LocalDate.now().toString() + ">";
    }

    @Override
    public String getText() {
        return text;
    }
}
