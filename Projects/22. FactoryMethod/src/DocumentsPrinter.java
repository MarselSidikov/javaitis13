/**
 * 13.03.2021
 * 22. FactoryMethod
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class DocumentsPrinter {
    private DocumentsCreator documentsCreator;

    public DocumentsPrinter(DocumentsCreator documentsCreator) {
        this.documentsCreator = documentsCreator;
    }

    public void printDocument(String text) {
        Document document = documentsCreator.createDocument(text);
        System.out.println(document.getTitle() + " " + document.getText());
    }
}
