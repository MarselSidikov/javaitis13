package ru.itis.result;

import ru.itis.Map;

import java.util.Map.Entry;

/**
 * 24.03.2021
 * 25. Associative Arrays
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class HashMap<K, V> implements Map<K, V> {
    private final static int DEFAULT_SIZE = 10;

    private static class EntryNode<K, V> {
        private K key;
        private V value;
        private EntryNode<K, V> next;

        EntryNode(K key, V value) {
            this.key = key;
            this.value = value;
        }
    }

    private EntryNode<K, V> table[];

    public HashMap() {
        table = new EntryNode[DEFAULT_SIZE];
    }

    @Override
    public void put(K key, V value) {
        // получаем хеш-код ключа, и обрезаем его до размеров массива, получаем индекс
        int index = Math.abs(key.hashCode() % DEFAULT_SIZE);
        // если в данном bucket-е ничего нет
        if (table[index] == null) {
            // кладем туда узел (он первый)
            table[index] = new EntryNode<>(key, value);
        } else {
            // необходимо пройти по всему bucket-у и найти ключ
            EntryNode<K, V> current = table[index]; // запоминаем первый элемент bucket-а
            // идем по всему текущему списку
            while (current != null) {
                // чтобы не тратить время на сравнение по equals, проверим hashCode - он быстрее

                // возможные ситуации -> если хеш-коды равны, не факт, что там один и тот же ключ
                // если хеш-коды разные, то объекты точно разные
                if (current.key.hashCode() != key.hashCode()) {
                    // переходим сразу к следующему узлу
                    current = current.next;
                } else {
                    // если совпали по хеш-коду, нужно проверить, а не тот ли ключ, который нужен?
                    // уточняем по equals
                    if (current.key.equals(key)) {
                        // если этот тот же самый ключ - делаем замену
                        current.value = value;
                        // если ключ заменили - дальше что-то делать нет смысла
                        return;
                    } else {
                        // если ключ не тот - идем дальше
                        current = current.next;
                    }
                }
            }
            // если мы оказались здесь - значит ключ не нашли, то нужно добавить его в bucket
            EntryNode<K,V> newNode = new EntryNode<>(key, value);
            // у нового узла - следующий после него -> первый узел bucket-а
            newNode.next = table[index];
            // теперь новый узел - первый в bucket
            table[index] = newNode;
        }
    }

    @Override
    public V get(K key) {
        // TODO: реализовать
        return null;
    }
}
