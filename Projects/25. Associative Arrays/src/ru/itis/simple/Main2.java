package ru.itis.simple;

public class Main2 {

    public static void main(String[] args) {
        // обычный массив
	    int array[] = new int[5];

	    array[0] = 7; // под ключ 0 положили значение 7
	    array[1] = 10; // под ключ 1 положили значение 10
	    array[2] = 777; // под ключ 2 положили значение 777
	    array[3] = 15;
	    array[4] = 20;

        System.out.println(array[0]); // взяли значение под ключом 0
        System.out.println(array[1]); // взяли значение под ключом 1
        System.out.println(array[2]);
        System.out.println(array[3]);
        System.out.println(array[4]);

    }
}
