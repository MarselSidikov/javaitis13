package ru.itis.site.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import ru.itis.site.dto.AccountDto;
import ru.itis.site.dto.SearchAccountDto;
import ru.itis.site.forms.SignUpForm;
import ru.itis.site.repositories.AccountsRepository;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * 29.05.2021
 * 36. Java Servlet Application
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
@Profile("dev")
@Service
public class AccountsServiceDevImpl implements AccountsService {

    @Override
    public List<AccountDto> getAll() {
        return Arrays.asList(
                AccountDto.builder()
                        .firstName("FAKE FIRST NAME 1")
                        .lastName("FAKE LAST NAME 1")
                        .email("FAKE EMAIL 1")
                        .build(),
                AccountDto.builder()
                        .firstName("FAKE FIRST NAME 2")
                        .lastName("FAKE LAST NAME 2")
                        .email("FAKE EMAIL 2")
                        .build(),
                AccountDto.builder()
                        .firstName("FAKE FIRST NAME 2")
                        .lastName("FAKE LAST NAME 2")
                        .email("FAKE EMAIL 2")
                        .build()
                );
    }

    @Override
    public List<SearchAccountDto> search(String query) {
//        return from(accountsRepository.findByFirstNameOrLastNameContains(query));
        return null;
    }

    @Override
    public AccountDto addUser(SignUpForm form) {
        return null;
    }
}
