package ru.itis.site.controllers.old;//package ru.itis.site.controllers;
//
//
//import org.springframework.web.servlet.ModelAndView;
//import org.springframework.web.servlet.mvc.Controller;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//
///**
// * 11.06.2021
// * 37. MVC
// *
// * @author Sidikov Marsel (First Software Engineering Platform)
// * @version v1.0
// */
//public class SignUpController implements Controller {
//    @Override
//    public ModelAndView handleRequest(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws Exception {
//        if (httpServletRequest.getMethod().equals("POST")) {
//            return new ModelAndView("redirect:/signIn");
//        } else {
//            return new ModelAndView("signUp");
//        }
//    }
//}
