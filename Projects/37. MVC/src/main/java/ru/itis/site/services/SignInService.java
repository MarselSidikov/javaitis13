package ru.itis.site.services;

/**
 * 08.06.2021
 * 36. Java Servlet Application
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public interface SignInService {
    boolean isCorrectCredentials(String email, String password);
}
