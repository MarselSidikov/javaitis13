package ru.itis.site.controllers.old;//package ru.itis.site.controllers;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.web.servlet.ModelAndView;
//import org.springframework.web.servlet.mvc.Controller;
//import ru.itis.site.dto.AccountDto;
//import ru.itis.site.services.AccountsService;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.util.List;
//
///**
// * 11.06.2021
// * 37. MVC
// *
// * @author Sidikov Marsel (First Software Engineering Platform)
// * @version v1.0
// */
//public class UsersController implements Controller {
//
//    @Autowired
//    private AccountsService accountsService;
//
//    @Override
//    public ModelAndView handleRequest(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws Exception {
//        if (httpServletRequest.getMethod().equals("GET")) {
//            List<AccountDto> accounts = accountsService.getAll();
//            ModelAndView modelAndView = new ModelAndView("users");
//            modelAndView.addObject("accounts", accounts);
//            return modelAndView;
//        } return null;
//    }
//}
