package ru.itis.site.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import ru.itis.site.dto.AccountDto;
import ru.itis.site.forms.SignUpForm;
import ru.itis.site.services.AccountsService;

import java.util.List;

/**
 * 14.06.2021
 * 37. MVC
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
@Controller
public class UsersController {

    @Autowired
    private AccountsService accountsService;

//    @RequestMapping(value = "/users", method = RequestMethod.GET)
//    public ModelAndView getUsersPage() {
//        List<AccountDto> accounts = accountsService.getAll();
//        ModelAndView modelAndView = new ModelAndView("users");
//        modelAndView.addObject("accounts", accounts);
//        return modelAndView;
//    }

    @RequestMapping(value = "/users", method = RequestMethod.GET)
    public String getUsersPage(Model model) {
        List<AccountDto> accounts = accountsService.getAll();
        model.addAttribute("accounts", accounts);
        return "users";
    }

    @RequestMapping(value = "/users", method = RequestMethod.POST)
    public String addNewUser(SignUpForm form) {
        accountsService.addUser(form);
        return "redirect:/users";
    }

    @RequestMapping(value = "/api/users", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<AccountDto> getUsers() {
        return accountsService.getAll();
    }

    @RequestMapping(value = "/api/users", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public AccountDto addNewUserFromJson(@RequestBody SignUpForm form) {
        return accountsService.addUser(form);
    }
}
