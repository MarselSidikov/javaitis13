//package ru.itis.site.repositories;
//
//import java.util.List;
//
///**
// * 29.05.2021
// * 36. Java Servlet Application
// *
// * @author Sidikov Marsel (First Software Engineering Platform)
// * @version v1.0
// */
//public interface CrudRepository<T, ID> {
//    List<T> findAll();
//    void save(T entity);
//
//}
