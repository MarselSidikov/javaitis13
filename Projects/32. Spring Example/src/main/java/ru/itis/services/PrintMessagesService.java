package ru.itis.services;

/**
 * 12.05.2021
 * spring-example
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public interface PrintMessagesService {
    void print(String message);
}
