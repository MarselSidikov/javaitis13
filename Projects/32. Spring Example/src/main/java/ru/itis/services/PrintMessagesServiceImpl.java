package ru.itis.services;

import ru.itis.messages.MessageProcessor;
import ru.itis.printers.Printer;

/**
 * 12.05.2021
 * spring-example
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class PrintMessagesServiceImpl implements PrintMessagesService {
    private MessageProcessor messageProcessor;
    private Printer printer;

    public PrintMessagesServiceImpl(MessageProcessor messageProcessor) {
        this.messageProcessor = messageProcessor;
    }

    @Override
    public void print(String message) {
        String newMessage = messageProcessor.process(message);
        printer.print(newMessage);
    }

    public void setPrinter(Printer printer) {
        this.printer = printer;
    }
}
