package ru.itis.messages;

import java.time.LocalTime;

/**
 * 12.05.2021
 * spring-example
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class MessageProcessorTimeImpl implements MessageProcessor {
    public String process(String message) {
        return LocalTime.now().toString() + " " +  message;
    }
}
