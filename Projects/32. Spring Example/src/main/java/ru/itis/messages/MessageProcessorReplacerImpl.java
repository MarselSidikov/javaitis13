package ru.itis.messages;

/**
 * 12.05.2021
 * spring-example
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class MessageProcessorReplacerImpl implements MessageProcessor {
    private String toReplace;

    public MessageProcessorReplacerImpl(String toReplace) {
        this.toReplace = toReplace;
    }

    public String process(String message) {
        return message.replace(toReplace, "");
    }
}
