package ru.itis.printers;

/**
 * 12.05.2021
 * spring-example
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class PrinterErrorImpl implements Printer {

    private String prefix;

    public void print(String message) {
        System.err.println(prefix + " " + message);
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }
}
