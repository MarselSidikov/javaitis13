package ru.itis.printers;

/**
 * 12.05.2021
 * spring-example
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class PrinterStandardImpl implements Printer {
    public void print(String message) {
        System.out.println(message);
    }
}
