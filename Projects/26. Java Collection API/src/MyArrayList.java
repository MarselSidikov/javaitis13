import java.util.ArrayList;
import java.util.Iterator;

/**
 * 07.04.2021
 * 26. Java Collection API
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class MyArrayList<T> implements Iterable<T> {
    private Object elements[];
    private int count = 0;

    public MyArrayList() {
        this.elements = new Object[10];
    }

    public void add(T element) {
        this.elements[count] = element;
        count++;
    }

    private class MyArrayListIterator implements Iterator<T> {

        private int current = 0;

        @Override
        public boolean hasNext() {
            return current < count;
        }

        @Override
        public T next() {
            T value = (T)elements[current];
            current++;
            return value;
        }
    }

    @Override
    public Iterator<T> iterator() {
        return new MyArrayListIterator();
    }
}
