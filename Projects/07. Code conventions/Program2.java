class Program {
	public static void main(String[] args) {
		int a = 5;

		{	
			int b = 2; // a - ok, b - ok
			{
				int c = 3;
			}
			System.out.println(c); // a - ok, b - ok, c - not ok
		}
		int c = 10;
		System.out.println(b); // a - ok, b - not ok, c - ok (not same)
	}
}