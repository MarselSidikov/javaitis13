package ru.itis.education.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import ru.itis.education.dto.AuthDto;
import ru.itis.education.dto.TokenDto;
import ru.itis.education.models.Account;
import ru.itis.education.models.Token;
import ru.itis.education.repositories.AccountsRepository;
import ru.itis.education.repositories.TokensRepository;

import java.util.Optional;
import java.util.UUID;

/**
 * 17.08.2021
 * 41. REST
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
@Service
public class SignInServiceImpl implements SignInService {

    @Autowired
    private AccountsRepository accountsRepository;

    @Autowired
    private TokensRepository tokensRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public TokenDto signIn(AuthDto auth) {
        Optional<Account> accountOptional = accountsRepository.findByEmail(auth.getEmail());

        if (accountOptional.isPresent()) {
            Account account = accountOptional.get();

            if (passwordEncoder.matches(auth.getPassword(), account.getHashPassword())) {
                String value = UUID.randomUUID().toString();
                Token token = Token.builder()
                        .value(value)
                        .account(account)
                        .build();

                tokensRepository.save(token);
                return new TokenDto(value);
            } else {
                throw new IllegalArgumentException("Wrong email/password");
            }
        } else {
            throw new IllegalArgumentException("Wrong email/password");
        }
    }
}
