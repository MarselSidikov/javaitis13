package ru.itis.education.services;

import ru.itis.education.dto.CourseDto;
import ru.itis.education.dto.UserDto;
import ru.itis.education.models.User;

import java.util.List;

/**
 * 07.08.2021
 * 41. REST
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public interface UsersService {
    List<UserDto> getUsers();
    List<CourseDto> getCoursesByUser(Long userId);

    List<CourseDto> deleteCourseFromUser(Long userId, Long courseId);
}
