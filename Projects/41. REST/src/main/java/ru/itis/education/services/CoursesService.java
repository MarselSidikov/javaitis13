package ru.itis.education.services;

import ru.itis.education.dto.CourseDto;
import ru.itis.education.forms.CourseForm;

import java.util.List;

/**
 * 07.08.2021
 * 41. REST
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public interface CoursesService {
    CourseDto addCourse(CourseForm course);

    CourseDto updateCourse(Long courseId, CourseForm course);

    List<CourseDto> search(int page, int size, String sortBy);
}
