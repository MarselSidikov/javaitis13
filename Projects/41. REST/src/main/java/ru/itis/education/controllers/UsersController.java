package ru.itis.education.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import ru.itis.education.dto.CourseDto;
import ru.itis.education.dto.UserDto;
import ru.itis.education.models.Course;
import ru.itis.education.models.User;
import ru.itis.education.repositories.CoursesRepository;
import ru.itis.education.repositories.UsersRepository;
import ru.itis.education.services.UsersService;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Supplier;

import static ru.itis.education.dto.CourseDto.from;

/**
 * 05.08.2021
 * 41. REST
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
@RestController
public class UsersController {

    @Autowired
    private UsersService usersService;

    @PreAuthorize("hasAuthority('ADMIN')")
    @GetMapping(value = "/users", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<UserDto> getUsers(@RequestHeader("Authorization") String auth) {
        return usersService.getUsers();
    }

    @GetMapping(value = "/users/{user-id}/courses")
    public List<CourseDto> getCoursesByUser(@PathVariable("user-id") Long userId) {
        return usersService.getCoursesByUser(userId);
    }

    @DeleteMapping(value = "/users/{user-id}/courses/{course-id}")
    public List<CourseDto> deleteCourseFromUser(@PathVariable("user-id") Long userId,
                                                @PathVariable("course-id") Long courseId) {
        return usersService.deleteCourseFromUser(userId, courseId);
    }

}
