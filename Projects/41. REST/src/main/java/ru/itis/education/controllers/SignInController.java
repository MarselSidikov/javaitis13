package ru.itis.education.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import ru.itis.education.dto.AuthDto;
import ru.itis.education.dto.TokenDto;
import ru.itis.education.services.SignInService;

import javax.annotation.security.PermitAll;

/**
 * 17.08.2021
 * 41. REST
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
@RestController
public class SignInController {

    @Autowired
    private SignInService signInService;

    @PermitAll
    @PostMapping("/signIn")
    public TokenDto signIn(@RequestBody AuthDto auth) {
        return signInService.signIn(auth);
    }
}
