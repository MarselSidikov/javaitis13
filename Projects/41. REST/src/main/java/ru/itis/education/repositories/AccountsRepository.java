package ru.itis.education.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.itis.education.models.Account;

import java.util.Optional;

/**
 * 26.06.2021
 * 37. MVC
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public interface AccountsRepository extends JpaRepository<Account, Long> {
    Optional<Account> findByEmail(String email);
    Optional<Account> findByTokens_Value(String value);
}
