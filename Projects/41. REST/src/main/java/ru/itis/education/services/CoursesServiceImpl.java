package ru.itis.education.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import ru.itis.education.dto.CourseDto;
import ru.itis.education.forms.CourseForm;
import ru.itis.education.models.Course;
import ru.itis.education.repositories.CoursesRepository;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static ru.itis.education.dto.CourseDto.from;

/**
 * 07.08.2021
 * 41. REST
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
@Service
public class CoursesServiceImpl implements CoursesService {

    @Autowired
    private CoursesRepository coursesRepository;

    @Override
    public CourseDto addCourse(CourseForm course) {
        Course newCourse = Course.builder()
                .startDate(LocalDate.parse(course.getStartDate()))
                .finishDate(LocalDate.parse(course.getFinishDate()))
                .title(course.getTitle())
                .build();
        coursesRepository.save(newCourse);
        return from(newCourse);
    }

    @Override
    public CourseDto updateCourse(Long courseId, CourseForm course) {
        Course courseForUpdate = coursesRepository.getById(courseId);

        updateCourse(course, courseForUpdate);
        coursesRepository.save(courseForUpdate);
        return from(coursesRepository.getById(courseId));
    }

    @Override
    public List<CourseDto> search(int page, int size, String sortBy) {
        Sort sort = Sort.by(sortBy);
        PageRequest pageRequest = PageRequest.of(page, size, sort);
        Page<Object[]> coursePage = coursesRepository.search(pageRequest);
        List<CourseDto> result = new ArrayList<>();
        for (Object[] course : coursePage.getContent()) {
            CourseDto courseDto = CourseDto.from(((Course) course[0]));
            courseDto.setLessonsCount((Integer) course[1]);
            result.add(courseDto);
        }
        return result;
    }

    private void updateCourse(CourseForm course, Course courseForUpdate) {
        courseForUpdate.setTitle(course.getTitle());
        courseForUpdate.setStartDate(LocalDate.parse(course.getStartDate()));
        courseForUpdate.setFinishDate(LocalDate.parse(course.getFinishDate()));
    }
}
