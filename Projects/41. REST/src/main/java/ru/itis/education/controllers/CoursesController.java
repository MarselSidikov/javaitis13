package ru.itis.education.controllers;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.itis.education.dto.CourseDto;
import ru.itis.education.forms.CourseForm;
import ru.itis.education.services.CoursesService;

import javax.annotation.security.PermitAll;
import java.util.List;

/**
 * 07.08.2021
 * 41. REST
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
@RestController
@RequestMapping("/courses")
public class CoursesController {

    @Autowired
    private CoursesService coursesService;

    @ApiOperation(value = "Добавление курса")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Успешно добавлено", response = CourseDto.class)})
    @PostMapping
    public CourseDto addCourse(@RequestBody CourseForm course) {
        return coursesService.addCourse(course);
    }

    @PutMapping("/{course-id}")
    public CourseDto updateCourse(@PathVariable("course-id") Long courseId,
                                  @RequestBody CourseForm course) {
        return coursesService.updateCourse(courseId, course);
    }

    @PermitAll
    @GetMapping("/search")
    public List<CourseDto> search(@RequestParam("page") int page,
                                  @RequestParam("size") int size,
                                  @RequestParam("sortBy") String sortBy) {
        return coursesService.search(page, size, sortBy);
    }
}
