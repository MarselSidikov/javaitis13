package list;

/**
 * 27.02.2021
 * 19. LinkedList
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main {
    public static void main(String[] args) {
        LinkedList list = new LinkedList();
        // распечатать n-элементов = 1 + 2 + 3 + ... + n = ((1 + n) / 2) * n -> Kn^2
        list.add(10);
        list.add(20);
        list.add(30);
        list.add(40);
        list.add(50);
        list.add(60);
        list.add(70);

//        for (int i = 0; i < list.size(); i++) {
//            System.out.println(list.get(i));
//        }



    }
}
