package list;

/**
 * 27.02.2021
 * 19. LinkedList
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class LinkedList {
    private static int A;
    // вложенный класс
    public static class Node {
        int value;
        Node next;

        Node(int value) {
            this.value = value;
        }
    }
    // внутренний класс
    public class LinkedListIterator {
        private Node current;

        public LinkedListIterator() {
            this.current = first;
        }

        public boolean hasNext() {
            return current != null;
        }

        public int next() {
            // запомнили текущее значение
            int value = current.value;
            // перешли к следующему
            current = current.next;
            // вернули то, что запомнили
            return value;
        }
    }

    // поле, которое хранит ссылку на первый элемент списка
    private Node first;

    private Node last;

    private int count;

    /**
     * public void add(int value) {
     * // создаем новый узел под новое значение
     * Node newNode = new Node(value);
     * // если в списке еще нет элементов
     * if (first == null) {
     * // значит новый элемент и будет первым
     * first = newNode;
     * } else {
     * // необходимо добраться до конца и положить элемент в конец списка
     * Node current = first;
     * // пока у текущего элемента есть ссылка на следующий
     * while (current.next != null) {
     * // переходим к следующему элементу
     * current = current.next;
     * }
     * // выходим из цикла, когда встали на последнем элемента
     * // следующий после последнего - новый узел
     * current.next = newNode;
     * }
     * }
     **/

    public void add(int value) {
        Node newNode = new Node(value);
        if (first == null) {
            this.first = newNode;
        } else {
            // следующий после последнего - новый узел
            last.next = newNode;
        }
        // новый узел теперь последний
        this.last = newNode;
        this.count++;
    }

    public void addToBegin(int value) {
        // TODO: реализовать
        this.count++;
    }

    public int get(int index) {

        // если индекс = 4
        if (index < count && index > -1) {
            Node current = this.first;
            // отсчитываем элементы
            // i = 0, 1, 2, 3, 4
            for (int i = 0; i < index; i++) {
                current = current.next;
            }
            return current.value;
        }
        System.err.println("Вышли за пределы списка");
        return -1;
    }

    public int size() {
        return count;
    }

    public LinkedListIterator iterator() {
        return new LinkedListIterator();
    }
}
