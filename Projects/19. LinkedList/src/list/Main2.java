package list;


/**
 * 27.02.2021
 * 19. LinkedList
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main2 {
    public static void main(String[] args) {
        LinkedList list = new LinkedList();
        list.add(10);
        list.add(20);
        list.add(30);
        LinkedList list2 = new LinkedList();
        list2.add(77);
        list2.add(88);
        list2.add(99);

//        LinkedList.LinkedListIterator iterator = list.new LinkedListIterator();
//        LinkedList.LinkedListIterator iterator2 = list2.new LinkedListIterator();

        LinkedList.LinkedListIterator iterator = list.iterator();
        LinkedList.LinkedListIterator iterator2 = list2.iterator();

        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }

        while (iterator2.hasNext()) {
            System.out.println(iterator2.next());
        }
    }
}
