/**
 * 27.02.2021
 * 19. LinkedList
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class LinkedList implements List {

    public static class Node {
        int value;
        Node next;

        Node(int value) {
            this.value = value;
        }
    }

    public class LinkedListIterator implements Iterator {
        private Node current;

        public LinkedListIterator() {
            this.current = first;
        }

        @Override
        public boolean hasNext() {
            return current != null;
        }

        @Override
        public int next() {
            int value = current.value;
            current = current.next;

            return value;
        }
    }

    private Node first;

    private Node last;

    private int count;

    @Override
    public void add(int value) {
        Node newNode = new Node(value);
        if (first == null) {
            this.first = newNode;
        } else {
            // следующий после последнего - новый узел
            last.next = newNode;
        }
        // новый узел теперь последний
        this.last = newNode;
        this.count++;
    }

    @Override
    public int get(int index) {

        // если индекс = 4
        if (index < count && index > -1) {
            Node current = this.first;
            // отсчитываем элементы
            // i = 0, 1, 2, 3, 4
            for (int i = 0; i < index; i++) {
                current = current.next;
            }
            return current.value;
        }
        System.err.println("Вышли за пределы списка");
        return -1;
    }

    @Override
    public void removeAt(int index) {
        // TODO: реализовать
    }

    @Override
    public int indexOf(int element) {
        // TODO: реализовать
        return 0;
    }

    @Override
    public int lastIndexOf(int element) {
        // TODO: реализовать
        return 0;
    }

    @Override
    public int size() {
        return count;
    }

    @Override
    public boolean contains(int element) {
        // TODO: реализовать
        return false;
    }

    @Override
    public void removeFirst(int element) {
        // a -> b -> c -> d -> null
        // ^
        // f

        // current = a

        Node current = first;

        // removeFirst(a)
        if (current.value == element) {
            // a -> b -> c -> d -> null
            //      ^
            //      f
            first = first.next;
            return;
        }

        // removeFirst(c)
        // a -> b -> c -> d -> null
        //      ^
        //      c
        while (current.next != null && current.next.value != element) {
            current = current.next;
        }

        // a -> b -> -> d -> null
        //      ^
        //      c
        if (current.next != null) {
            current.next = current.next.next;
        }

        if (current.next == null) {
            this.last = current;
        }

        count--;
    }

    @Override
    public void removeLast(int element) {
        // TODO: реализовать
    }

    @Override
    public void removeAll(int element) {
        // TODO: реализовать
    }

    @Override
    public Iterator iterator() {
        return new LinkedListIterator();
    }
}
