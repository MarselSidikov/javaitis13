public class Main {

    public static void main(String[] args) {
        List list = new LinkedList();
        list.add(7);
        list.add(15);
        list.add(10);
        list.add(15);
        list.add(16);
        list.add(17);
        list.add(18);
        list.add(19);
        list.add(20);
        list.add(26);
        list.add(36);
        list.add(46);
        list.add(56);
        list.add(6546);
        list.add(136);
        list.add(1336);
        list.add(163);

//        list.removeFirst(10);
//        list.removeFirst(7);
        list.removeFirst(16);
        System.out.println(list.get(0));
        System.out.println(list.get(1));
        System.out.println(list.get(2));
        System.out.println(list.get(3));
        System.out.println(list.get(4));

        Iterator iterator = list.iterator();

        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }
    }
}
