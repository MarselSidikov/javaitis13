/**
 * 13.03.2021
 * 21. Collections
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class ArrayList implements List {
    private static final int DEFAULT_SIZE = 10;

    private int elements[];
    private int count;

    public ArrayList() {
        this.elements = new int[DEFAULT_SIZE];
        this.count = 0;
    }

    @Override
    public int get(int index) {
        if (index >= 0 && index < count) {
            return elements[index];
        }
        return -1;
    }

    @Override
    public void removeAt(int index) {
        // TODO: реализовать
    }

    @Override
    public int indexOf(int element) {
        // TODO: реализовать
        return 0;
    }

    @Override
    public int lastIndexOf(int element) {
        // TODO: реализовать
        return 0;
    }

    @Override
    public void add(int element) {
        // если мы хотим добавить элемент, но массив уже заполнен
        if (count == elements.length) {
            int newArray[] = new int[elements.length + elements.length / 2];
            for (int i = 0; i < count; i++) {
                newArray[i] = elements[i];
            }
            elements = newArray;
        }
        elements[count] = element;
        count++;
    }

    @Override
    public int size() {
        return count;
    }

    @Override
    public boolean contains(int element) {
        // TODO: реализовать
        return false;
    }

    @Override
    public void removeFirst(int element) {
        int positionOfRemovingElement = -1;
        for (int i = 0; i < count; i++) {
            if (elements[i] == element) {
                positionOfRemovingElement = i;
                break;
            }
        }
        for (int i = positionOfRemovingElement; i < count; i++) {
            elements[i] = elements[i + 1];
        }
        count--;
    }

    @Override
    public void removeLast(int element) {
        // TODO: реализовать
    }

    @Override
    public void removeAll(int element) {
        // TODO: реализовать
    }

    private class ArrayListIterator implements Iterator {

        private int currentPosition;

        @Override
        public int next() {
            int nextValue = elements[currentPosition];
            currentPosition++;
            return nextValue;
        }

        @Override
        public boolean hasNext() {
            return currentPosition < count;
        }
    }
    @Override
    public Iterator iterator() {
        return new ArrayListIterator();
    }
}
