package ru.itis.game.sockets.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 24.05.2021
 * 33. Sockets
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
@Data
@NoArgsConstructor
@Builder
@AllArgsConstructor
public class UsernamePasswordDto {
    private String nickname;
    private String password;
    private String type;
}
