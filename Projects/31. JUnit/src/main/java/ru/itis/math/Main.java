package ru.itis.math;

/**
 * 07.05.2021
 * junit-example
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main {
    public static void main(String[] args) {
        NumbersUtil numbersUtil = new NumbersUtil();

        System.out.println(numbersUtil.gcd(18, 12));
    }
}
