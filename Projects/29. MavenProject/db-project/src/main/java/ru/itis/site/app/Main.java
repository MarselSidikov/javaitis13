package ru.itis.site.app;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import ru.itis.site.models.Account;
import ru.itis.site.repositories.AccountsRepository;
import ru.itis.site.repositories.AccountsRepositoryJdbcImpl;
import ru.itis.site.utils.db.CustomDataSource;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Scanner;

/**
 * 24.04.2021
 * 28. DB
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main {
    public static void main(String[] args) throws Exception {
//        DataSource dataSource = new CustomDataSource("postgres", "qwerty007", "jdbc:postgresql://localhost:5432/javaitis13");

        HikariConfig config = new HikariConfig();
        config.setJdbcUrl("jdbc:postgresql://localhost:5432/javaitis13");
        config.setDriverClassName("org.postgresql.Driver");
        config.setUsername("postgres");
        config.setPassword("qwerty007");
        config.setMaximumPoolSize(50);

        DataSource dataSource = new HikariDataSource(config);

        AccountsRepository accountsRepository = new AccountsRepositoryJdbcImpl(dataSource);
        System.out.println(accountsRepository.findAll());
        System.out.println(accountsRepository.findById(5L));

        Scanner scanner = new Scanner(System.in);
        scanner.nextLine();

        new Thread(() -> {
            for (int i = 0; i < 1000; i++) {
                accountsRepository.findAll();
            }
        }).start();

        new Thread(() -> {
            for (int i = 0; i < 1000; i++) {
                accountsRepository.findById(5L);
            }
        }).start();

        new Thread(() ->
        {
            for (int i = 0; i < 1000; i++) {
                Account marsel = accountsRepository.findById(1L);
                marsel.setLastName("Гудайдиев"); // Good idea
                accountsRepository.update(marsel);
            }
        }).start();

    }
}
