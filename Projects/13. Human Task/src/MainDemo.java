/**
 * 10.02.2021
 * 13. Human Task
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class MainDemo {
    public static void main(String[] args) {
        // я точно знаю, что все числа от 1 до 10
        // как найти число, которое повторяется чаще всего?
        int numbers[] = {1, 1, 5, 6, 6, 6, 5, 10, 2, 8, 8, 9, 8, 1};

        int counts[] = new int[11];

        for (int i = 0; i < numbers.length; i++) {
            int currentNumber = numbers[i];
            counts[currentNumber]++;
        }

    }
}
