package ru.itis.education.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.itis.education.models.User;

/**
 * 05.08.2021
 * 41. REST
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public interface UsersRepository extends JpaRepository<User, Long> {
}
