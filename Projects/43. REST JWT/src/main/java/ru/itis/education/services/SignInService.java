package ru.itis.education.services;

import ru.itis.education.dto.AuthDto;
import ru.itis.education.dto.TokenDto;

/**
 * 17.08.2021
 * 41. REST
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public interface SignInService {
    TokenDto signIn(AuthDto auth);
}
