package ru.itis.education.forms;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 07.08.2021
 * 41. REST
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
@ApiModel(description = "Форма курса")
@Data
public class CourseForm {
    @ApiModelProperty(value = "Начало курса")
    private String startDate;
    @ApiModelProperty(value = "Конец курса")
    private String finishDate;
    @ApiModelProperty(value = "Название курса")
    private String title;
}
