package ru.itis.education.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

/**
 * 05.08.2021
 * 41. REST
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "employee")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    public enum Role {
        STUDENT, TEACHER
    }

    @Enumerated(value = EnumType.STRING)
    private Role role;

    private String email;
    private String fullName;

    public boolean isTeacher() {
        return this.role.equals(Role.TEACHER);
    }
}
