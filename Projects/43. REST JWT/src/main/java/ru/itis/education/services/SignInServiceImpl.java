package ru.itis.education.services;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import ru.itis.education.dto.AuthDto;
import ru.itis.education.dto.TokenDto;
import ru.itis.education.models.Account;
import ru.itis.education.repositories.AccountsRepository;

import java.util.Optional;

/**
 * 17.08.2021
 * 41. REST
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
@Service
public class SignInServiceImpl implements SignInService {

    @Autowired
    private AccountsRepository accountsRepository;

    @Value("${jwt.secret}")
    private String secretKey;


    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public TokenDto signIn(AuthDto auth) {
        Optional<Account> accountOptional = accountsRepository.findByEmail(auth.getEmail());

        if (accountOptional.isPresent()) {
            Account account = accountOptional.get();

            if (passwordEncoder.matches(auth.getPassword(), account.getHashPassword())) {
                String token = JWT.create()
                        .withSubject(account.getId().toString())
                        .withClaim("email", account.getEmail())
                        .withClaim("role", account.getRole().toString())
                        .withClaim("state", account.getState().toString())
                        .sign(Algorithm.HMAC256(secretKey));
                return new TokenDto(token);
            } else {
                throw new IllegalArgumentException("Wrong email/password");
            }
        } else {
            throw new IllegalArgumentException("Wrong email/password");
        }
    }
}
