package ru.itis.education.dto;

import lombok.Data;

/**
 * 17.08.2021
 * 41. REST
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
@Data
public class AuthDto {
    private String email;
    private String password;
}
