package ru.itis.education.repositories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import ru.itis.education.models.Course;
import ru.itis.education.models.User;

import java.util.List;

/**
 * 05.08.2021
 * 41. REST
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public interface CoursesRepository extends JpaRepository<Course, Long> {
    List<Course> findAllByTeacher(User teacher);
    List<Course> findAllByStudents_Id(Long id);
    @Query("select course, course.lessons.size as lessonsCount from Course course ")
    Page<Object[]> search(Pageable pageable);
}
