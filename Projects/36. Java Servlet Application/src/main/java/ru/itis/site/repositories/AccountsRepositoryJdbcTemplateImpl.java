package ru.itis.site.repositories;

import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;
import ru.itis.site.models.Account;

import java.util.*;

/**
 * 29.05.2021
 * 36. Java Servlet Application
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
@Repository
public class AccountsRepositoryJdbcTemplateImpl implements AccountsRepository {

    //language=SQL
    private static final String SQL_SEARCH = "select * from account where first_name ilike (:query) or last_name ilike (:query)";

    //language=SQL
    private static final String SQL_SELECT_ALL = "select * from account";

    //language=SQL
    private static final String SQL_FIND_BY_EMAIL = "select * from account where email = :email";

    //language=SQL
    private static final String SQL_INSERT_USER = "insert into account(first_name, last_name, email, hash_password) " +
            "values (:firstName, :lastName, :email, :hashPassword)";

    private final NamedParameterJdbcTemplate namedJdbcTemplate;

    public AccountsRepositoryJdbcTemplateImpl(NamedParameterJdbcTemplate namedJdbcTemplate) {
        this.namedJdbcTemplate = namedJdbcTemplate;
    }

    private RowMapper<Account> accountRowMapper = (row, rowNumber) ->
            Account.builder()
                    .id(row.getLong("id"))
                    .firstName(row.getString("first_name"))
                    .lastName(row.getString("last_name"))
                    .experience(row.getInt("experience"))
                    .email(row.getString("email"))
                    .hashPassword(row.getString("hash_password"))
                    .build();

    @Override
    public List<Account> findByFirstNameOrLastNameContains(String name) {
        Map<String, Object> params = Collections.singletonMap("query", "%" + name + "%");
        return namedJdbcTemplate.query(SQL_SEARCH, params, accountRowMapper);
    }

    @Override
    public Optional<Account> findByEmail(String email) {
        try {
            Account account = namedJdbcTemplate.queryForObject(SQL_FIND_BY_EMAIL, Collections.singletonMap("email", email), accountRowMapper);
            return Optional.of(account);
        } catch (EmptyResultDataAccessException e) {
            return Optional.empty();
        }
    }

    @Override
    public List<Account> findAll() {
        return namedJdbcTemplate.query(SQL_SELECT_ALL, accountRowMapper);
    }

    @Override
    public void save(Account entity) {
        Map<String, Object> params = new HashMap<>();
        params.put("firstName", entity.getFirstName());
        params.put("lastName", entity.getLastName());
        params.put("email", entity.getEmail());
        params.put("hashPassword", entity.getHashPassword());
        namedJdbcTemplate.update(SQL_INSERT_USER, params);
    }
}
