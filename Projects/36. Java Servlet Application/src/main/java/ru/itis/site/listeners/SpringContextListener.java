package ru.itis.site.listeners;

import com.zaxxer.hikari.HikariDataSource;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.itis.site.config.ApplicationConfig;
import ru.itis.site.services.AccountsService;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import javax.sql.DataSource;

/**
 * 05.06.2021
 * 36. Java Servlet Application
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
@WebListener
public class SpringContextListener implements ServletContextListener {

    private ApplicationContext springContext;

    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        // при запуске контекста вы получили его
        // контекст сервлетов имеет доступ ко всем сервлетам и наоборот
        ServletContext servletContext = servletContextEvent.getServletContext();
        this.springContext = new AnnotationConfigApplicationContext(ApplicationConfig.class);
        // положил в контекст сервлетов атрибут контекст спринга
        servletContext.setAttribute("springContext", springContext);

    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        ((HikariDataSource)this.springContext.getBean(DataSource.class)).close();
    }
}
