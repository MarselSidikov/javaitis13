package ru.itis.site.filters;

import lombok.SneakyThrows;
import ru.itis.site.forms.SignUpForm;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.Map;

/**
 * 05.06.2021
 * 36. Java Servlet Application
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */

public class FormConverterFilter implements Filter {
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    @SneakyThrows
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        if (request.getMethod().equals("POST")) {
            Map<String, String[]> body = request.getParameterMap();
            Class<SignUpForm> signUpFormClass = SignUpForm.class;
            SignUpForm form = signUpFormClass.newInstance();
            // получаете все параметры запроса
            for (String paramName : body.keySet()) {
                // во все поля вставить значения параметров
                // signUpFormClass.getDeclaredField(paramName);
            }
            // кладете созданную форму как атрибут запроса
            request.setAttribute("form", form);
        }
        chain.doFilter(servletRequest, servletResponse);
    }

    @Override
    public void destroy() {

    }
}
