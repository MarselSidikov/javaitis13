package ru.itis.site.services;

import ru.itis.site.forms.SignUpForm;

/**
 * 05.06.2021
 * 36. Java Servlet Application
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public interface SignUpService {
    void signUp(SignUpForm form);
}
