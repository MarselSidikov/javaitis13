package ru.itis.site.servlets;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 01.06.2021
 * 36. Java Servlet Application
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
@WebServlet("/profile")
public class ProfileServlet extends HttpServlet {
//    @Override
//    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//        String color = request.getParameter("color");
//        String colorStyle = "style=\"color:";
//
//        if (color == null) {
//
//            String cookiesHeader = request.getHeader("cookie");
//
//            if (cookiesHeader != null) {
//                String cookies[] = cookiesHeader.split("; ");
//
//                for (String cookie : cookies) {
//                    if (cookie.split("=")[0].equals("color")) {
//                        color = cookie.split("=")[1];
//                    }
//                }
//            }
//
//            if (color == null) {
//                color = "black";
//            }
//        } else {
//            response.setHeader("set-cookie", "color=" + color);
//        }
//
//        colorStyle = colorStyle + color + "\"";
//
//        response.getWriter().println("<h1 " +  colorStyle + ">Marsel!</h1>");
//    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String color = request.getParameter("color");
        String colorStyle = "style=\"color:";

        if (color == null) {

            Cookie[] cookies = request.getCookies();

            if (cookies != null) {

                for (Cookie cookie : cookies) {
                    if (cookie.getName().equals("color")) {
                        color = cookie.getValue();
                    }
                }
            }

            if (color == null) {
                color = "black";
            }
        } else {
            response.addCookie(new Cookie("color", color));
        }

        colorStyle = colorStyle + color + "\"";

        response.getWriter().println("<h1 " +  colorStyle + ">Marsel!</h1>");
    }
}
