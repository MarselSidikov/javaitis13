package ru.itis.site.servlets;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.stereotype.Component;
import ru.itis.site.config.ApplicationConfig;
import ru.itis.site.dto.SearchAccountDto;
import ru.itis.site.services.AccountsService;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

/**
 * 29.05.2021
 * 36. Java Servlet Application
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
@WebServlet("/admin/search")
public class SearchServlet extends HttpServlet {

    private AccountsService accountsService;

    @Override
    public void init(ServletConfig servletConfig) throws ServletException {
        // внутри сервлета получают доступ к контексту сервлетов
        ServletContext servletContext = servletConfig.getServletContext();
        ApplicationContext springContext = (ApplicationContext) servletContext.getAttribute("springContext");
        this.accountsService = springContext.getBean(AccountsService.class);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PrintWriter writer = response.getWriter();
            String query = request.getParameter("query");
            List<SearchAccountDto> accounts = accountsService.search(query);
            writer.println(accounts);
    }
}
