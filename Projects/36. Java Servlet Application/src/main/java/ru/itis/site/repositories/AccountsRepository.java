package ru.itis.site.repositories;

import ru.itis.site.models.Account;

import java.util.List;
import java.util.Optional;

/**
 * 29.05.2021
 * 36. Java Servlet Application
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public interface AccountsRepository extends CrudRepository<Account, Long> {
    List<Account> findByFirstNameOrLastNameContains(String name);
    Optional<Account> findByEmail(String email);
}
