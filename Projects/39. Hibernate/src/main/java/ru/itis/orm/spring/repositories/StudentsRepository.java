package ru.itis.orm.spring.repositories;

import ru.itis.orm.spring.models.Student;

/**
 * 24.06.2021
 * 39. Hibernate
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public interface StudentsRepository {
    void save(Student student);
}
