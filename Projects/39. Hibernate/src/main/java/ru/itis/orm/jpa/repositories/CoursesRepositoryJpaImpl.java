package ru.itis.orm.jpa.repositories;

import ru.itis.orm.jpa.models.Course;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import java.util.List;

/**
 * 24.06.2021
 * 39. Hibernate
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class CoursesRepositoryJpaImpl implements CoursesRepository {

    private EntityManager entityManager;

    public CoursesRepositoryJpaImpl(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    public void save(Course course) {
        EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        entityManager.persist(course);
        transaction.commit();
    }

    public List<Course> findAllByStudentsCount(int count) {
        return entityManager.createQuery("select course from Course course where course.students.size = ?1",
                Course.class)
                .setParameter(1, count)
                .getResultList();
    }
}
