package ru.itis.orm.hibernate.models;

import lombok.*;

/**
 * 23.06.2021
 * 39. Hibernate
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString(exclude = "course")
public class Lesson_ {
    private Long id;
    private String name;

    private Course_ course;
}
