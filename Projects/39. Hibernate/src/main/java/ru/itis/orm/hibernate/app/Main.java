package ru.itis.orm.hibernate.app;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;
import ru.itis.orm.hibernate.models.Course_;
import ru.itis.orm.hibernate.models.Lesson_;

/**
 * 23.06.2021
 * 39. Hibernate
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main {
    public static void main(String[] args) {
        Configuration configuration = new Configuration();
        configuration.configure("hibernate\\hibernate.cfg.xml");

        SessionFactory sessionFactory = configuration.buildSessionFactory();

        Session session = sessionFactory.openSession();

        Course_ java = Course_.builder()
                .title("Java")
                .build();

        Lesson_ sqlLesson = Lesson_.builder()
                .course(java)
                .name("Урок по SQL")
                .build();

        Lesson_ springLesson = Lesson_.builder()
                .course(java)
                .name("Урок по Spring")
                .build();

        Lesson_ hibernateLesson = Lesson_.builder()
                .course(java)
                .name("Урок по Hibernate")
                .build();

        session.save(java);
        session.save(sqlLesson);
        session.save(springLesson);
        session.save(hibernateLesson);
        session.close();

        session = sessionFactory.openSession();

        Query<Course_> query = session.createQuery("from Course course where course.title = 'Java'", Course_.class);
        Course_ courseFromDb = query.getSingleResult();
        System.out.println(courseFromDb);
        session.close();


    }
}
