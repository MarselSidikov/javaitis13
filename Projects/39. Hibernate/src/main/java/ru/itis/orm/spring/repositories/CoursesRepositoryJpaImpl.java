package ru.itis.orm.spring.repositories;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.itis.orm.spring.models.Course;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * 24.06.2021
 * 39. Hibernate
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
@Repository
public class CoursesRepositoryJpaImpl implements CoursesRepository {

    @PersistenceContext
    private EntityManager entityManager;

    @Transactional
    public void save(Course course) {
        entityManager.persist(course);
    }

    public List<Course> findAllByStudentsCount(int count) {
        return entityManager.createQuery("select course from Course course where course.students.size = ?1",
                Course.class)
                .setParameter(1, count)
                .getResultList();
    }
}
