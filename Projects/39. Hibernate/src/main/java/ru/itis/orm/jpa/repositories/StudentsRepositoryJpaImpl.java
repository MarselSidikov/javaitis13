package ru.itis.orm.jpa.repositories;

import ru.itis.orm.jpa.models.Student;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

/**
 * 24.06.2021
 * 39. Hibernate
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class StudentsRepositoryJpaImpl implements StudentsRepository {
    private EntityManager entityManager;

    public StudentsRepositoryJpaImpl(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    public void save(Student student) {
        EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        entityManager.persist(student);
        transaction.commit();
    }
}
