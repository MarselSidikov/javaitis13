package ru.itis.orm.spring.repositories;

import ru.itis.orm.spring.models.Course;

import java.util.List;

/**
 * 24.06.2021
 * 39. Hibernate
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public interface CoursesRepository {
    void save(Course course);
    List<Course> findAllByStudentsCount(int count);
}
