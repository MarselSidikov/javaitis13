package ru.itis.orm.spring.repositories;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.itis.orm.spring.models.Student;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * 24.06.2021
 * 39. Hibernate
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
@Repository
public class StudentsRepositoryJpaImpl implements StudentsRepository {

    @PersistenceContext
    private EntityManager entityManager;

    @Transactional
    public void save(Student student) {
        entityManager.persist(student);
    }
}
