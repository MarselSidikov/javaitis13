package ru.itis.site.repositories;

import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabase;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;
import ru.itis.site.models.Account;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.hamcrest.collection.IsEmptyCollection.empty;
import static com.github.npathai.hamcrestopt.OptionalMatchers.isPresentAndIs;

/**
 * 08.05.2021
 * jdbc-template-example
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
@DisplayNameGeneration(value = DisplayNameGenerator.ReplaceUnderscores.class)
class AccountsRepositoryJdbcTemplateImplTest {

    private EmbeddedDatabase embeddedDatabase;

    private AccountsRepositoryJdbcTemplateImpl accountsRepository;

    @BeforeEach
    void setUp() {
        embeddedDatabase = new EmbeddedDatabaseBuilder()
                .setType(EmbeddedDatabaseType.H2)
                .addScripts("sql\\schema.sql", "sql\\data.sql")
                .build();
        accountsRepository = new AccountsRepositoryJdbcTemplateImpl(embeddedDatabase);
    }

    @AfterEach
    public void shutdown() {
        embeddedDatabase.shutdown();
    }

    @Nested
    class FindOperations {
        @Test
        void find_all_returns_not_empty_list() {
            List<Account> accounts = accountsRepository.findAll();
            assertThat(accounts, is(not(empty())));
        }

        @ParameterizedTest(name = "method returns {0}")
        @MethodSource("ru.itis.site.repositories.AccountsRepositoryJdbcTemplateImplTest#expectedAccountsProvider")
        void find_all_returns_correct_objects(List<Account> expectedAccounts) {
            List<Account> accounts = accountsRepository.findAll();
            assertThat(accounts, contains(expectedAccounts.toArray()));
        }

        @ParameterizedTest(name = "method returns {0}")
        @MethodSource("ru.itis.site.repositories.AccountsRepositoryJdbcTemplateImplTest#expectedAccountProvider")
        void find_by_id_returns_correct_object(Account expectedAccount) {
            Optional<Account> accountOptional = accountsRepository.findById(1L);
            assertThat(accountOptional, isPresentAndIs(expectedAccount));
        }
    }

    @Nested
    class ModifyOperations {
        @ParameterizedTest(name = "method correct save {0}")
        @MethodSource("ru.itis.site.repositories.AccountsRepositoryJdbcTemplateImplTest#expectedAccountForSaveProvider")
        void save_works_correct(Account expectedAccount) {
            accountsRepository.save(expectedAccount);
            Optional<Account> accountOptional = accountsRepository.findById(4L);
            assertThat(accountOptional, isPresentAndIs(expectedAccount));
        }

    }

    private static Stream<Account> expectedAccountForSaveProvider() {
        return Stream.of(Account.builder()
                .id(4L)
                .firstName("USER_4_FIRST_NAME")
                .lastName("USER_5_LAST_NAME")
                .isActive(true)
                .build());
    }

    private static Stream<Account> expectedAccountProvider() {
        return Stream.of(Account.builder()
                .id(1L)
                .firstName("USER_1_FIRST_NAME")
                .lastName("USER_1_LAST_NAME")
                .isActive(true)
                .build());
    }

    private static Stream<List<Account>> expectedAccountsProvider() {
        List<Account> accounts = Arrays.asList(
                Account.builder()
                        .id(1L)
                        .firstName("USER_1_FIRST_NAME")
                        .lastName("USER_1_LAST_NAME")
                        .isActive(true)
                        .build(),
                Account.builder()
                        .id(2L)
                        .firstName("USER_2_FIRST_NAME")
                        .lastName("USER_2_LAST_NAME")
                        .isActive(true)
                        .build(),
                Account.builder()
                        .id(3L)
                        .firstName("USER_3_FIRST_NAME")
                        .lastName("USER_3_LAST_NAME")
                        .isActive(true)
                        .build());
        return Stream.of(accounts);
    }
}