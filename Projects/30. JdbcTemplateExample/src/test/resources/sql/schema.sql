create table account (
    id int auto_increment primary key,
    first_name varchar(30),
    last_name varchar(30),
    is_active boolean
);