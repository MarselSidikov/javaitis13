package ru.itis.site.repositories;

import java.util.List;
import java.util.Optional;

/**
 * 05.05.2021
 * jdbc-template-example
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public interface CrudRepository<T, ID> {
    List<T> findAll();
    // тут написано что возвращается T, а если он не найден, вернется что?
    // вернется ли null?
    // произойдет исключение?
    // а может вернется пустой объект у которого все поля null?
    Optional<T> findById(ID id);

    void save(T account);

    void update(T account);

    void delete(T account);

    void deleteById(ID id);
}
