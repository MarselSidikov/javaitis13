package ru.itis.site.repositories;

import ru.itis.site.models.Account;

/**
 * 05.05.2021
 * jdbc-template-example
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public interface AccountsRepository extends CrudRepository<Account, Long> {
}
