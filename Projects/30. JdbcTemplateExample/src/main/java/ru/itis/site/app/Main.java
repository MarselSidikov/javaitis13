package ru.itis.site.app;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import ru.itis.site.models.Account;
import ru.itis.site.repositories.AccountsRepository;
import ru.itis.site.repositories.AccountsRepositoryJdbcTemplateImpl;

import javax.sql.DataSource;
import java.util.Optional;

/**
 * 05.05.2021
 * jdbc-template-example
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main {
    public static void main(String[] args) {

        ApplicationContext context = new ClassPathXmlApplicationContext("context.xml");
        AccountsRepository accountsRepository = context.getBean(AccountsRepository.class);

        Account account = Account.builder()
                .firstName("TEMP")
                .lastName("TEMP2")
                .isActive(true)
                .build();
        accountsRepository.save(account);
        System.out.println(account);

//        Account account = accountsRepository.findById(9L);
//        account.setFirstName("ГЛАВНЫЙ");
//        accountsRepository.update(account);

//        System.out.println(accountsRepository.findAll());

//        Optional<Account> accountOptional = accountsRepository.findById(155L);
//
//        if (accountOptional.isPresent()) {
//            Account account = accountOptional.get();
//            System.out.println(account);
//        } else {
//            System.out.println("не нашли объекта");
//        }

    }
}
