import java.util.Scanner;

class Program {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);

		int number = scanner.nextInt();

		if (number < 1) {
			System.err.println("Incorrect number");
			System.exit(255);
		}

		if (number == 2 | number == 3) {
			System.out.println("PRIME");
		}

		int x = 2;

		boolean isPrime = true;
		int iterations = 0;
		while((x * x) < number) {
			if (number % x == 0) {
				isPrime = false;
				break;
			}
			x++;
			iterations++;
		}

		if (isPrime) {
			System.out.println("PRIME");
		} else {
			System.out.println("NOT PRIME");
		}
		System.out.println("ITERATIONS: " + iterations);
	}
}