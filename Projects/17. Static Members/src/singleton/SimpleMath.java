package singleton;

/**
 * 25.02.2021
 * 17. Static Members
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class SimpleMath {
    private Logger logger;

    public SimpleMath(Logger logger) {
        this.logger = logger;
    }

    public int div(int a, int b) {
        if (b == 0) {
            logger.error("Деление на ноль. Возвращено значение -1");
            return -1;
        }
        logger.info("Частное рассчитано, возвращен результат");
        return a / b;
    }
}
