package singleton;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

/**
 * 25.02.2021
 * 17. Static Members
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
// logger - механизм, который ведет журнал событий и ошибок в системе
public class Logger {
    // единственное глобальное поле, которое хранит единственный экземпляр класса
    private static Logger instance;

    static {
        instance = new Logger();
    }

    private LocalDateTime creationTime;
    private String loggerName;

    private Logger() {
        creationTime = LocalDateTime.now();
        this.loggerName = "LOGGER: " + creationTime.toString();
    }

    public static Logger getLogger() {
//        if (instance == null) {
//            instance = new Logger();
//        }
        return instance;
    }

    public void info(String message) {
        System.out.println(loggerName + " INFO [ " + LocalTime.now() + "] " + message);
    }

    public void error(String message) {
        System.err.println(loggerName + " ERROR [ " + LocalTime.now() + "] " + message);
    }
}
