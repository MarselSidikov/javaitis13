package singleton;

import java.util.Scanner;

/**
 * 25.02.2021
 * 17. Static Members
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class SimpleScannerReader {
    private Logger logger;
    private Scanner scanner;

    public SimpleScannerReader(Logger logger) {
        this.logger = logger;
        this.scanner = new Scanner(System.in);
    }

    public int getNumber() {
        int result =  scanner.nextInt();
        logger.info("Получено значение с консоли");
        return result;
    }
}
