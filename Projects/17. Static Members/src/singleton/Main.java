package singleton;

/**
 * 25.02.2021
 * 17. Static Members
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main {
    public static void main(String[] args) {
//        Logger logger = new Logger();
//        Logger logger1 = new Logger();
        Logger logger = Logger.getLogger();
        Logger logger1 = Logger.getLogger();
        SimpleMath simpleMath = new SimpleMath(logger);
        SimpleScannerReader simpleReader = new SimpleScannerReader(logger1);

        int firstNumber = simpleReader.getNumber();
        int secondNumber = simpleReader.getNumber();
        int sumResult = simpleMath.div(firstNumber, secondNumber);

        System.out.println(sumResult);
    }
}
