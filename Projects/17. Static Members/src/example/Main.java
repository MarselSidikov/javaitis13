package example;

import com.sun.javafx.util.Logging;

public class Main {
// примеры использования своих статических членов класса
    public static void main(String[] args) {
//    	example.SomeClass.CONST = 100;
		SomeClass.someStaticMethod();
		System.out.println(SomeClass.a);
	    SomeClass object1 = new SomeClass();
	    object1.b = 10;
	    object1.a = 100;
	    SomeClass object2 = new SomeClass();
	    object2.b = 15;
		object2.a = 200;
	    SomeClass object3 = new SomeClass();
	    object3.b = 20;
		object3.a = 300;

		SomeClass.a = 500;

        System.out.println(object1.b);
        System.out.println(object1.a);
        System.out.println(object2.b);
        System.out.println(object2.a);
        System.out.println(object3.b);
        System.out.println(object3.a);

        object1.someMethod();
        object1.someStaticMethod(); // так делать не надо

    }
}
