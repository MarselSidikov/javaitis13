package example;

import java.util.Random;

/**
 * 25.02.2021
 * 17. Static Members
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class SomeClass {
    // статическая константа
    public static final int CONST = 100;
    // статическое поле
    public static int a = 400;
    int b;

    // статический инициализатор (статический конструктор)
    static {
        Random random = new Random();
        a = random.nextInt(100);
    }
    // обычный метод
    void someMethod() {
        System.out.println("Some Method");
        System.out.println(a); // имеет доступ к полю a
        System.out.println(b); // имеет доступ к полю b
    }

    static void someStaticMethod() {
        System.out.println("Some Static method");
        System.out.println(a); // имеет доступ к полю а
//        System.out.println(b); // не имеет доступ к полю b, потому что это поле требует создания объекта
    }
}
