package example;

/**
 * 25.02.2021
 * 17. Static Members
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main2 {
    // статические члены классов в Java
    public static void main(String[] args) {
        double d = Math.PI;
        double sqrt = Math.sqrt(d);
        System.out.println(Character.isDigit('9'));
    }
}
