package ru.itis.solution;

import ru.itis.solution.framework.DocumentsFramework;

import java.time.LocalDate;

/**
 * 11.05.2021
 * 31. Reflection
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main {
    public static void main(String[] args) {
        DocumentsFramework framework = new DocumentsFramework();
        Statement statement = framework.generateText(Statement.class,
                "Сидиков Марсель",
                LocalDate.of(1994, 2, 2));
        Act act = framework.generateText(Act.class,
                LocalDate.of(2020, 10, 1),
                "Сверка", "Провели сверку документов");

        System.out.println(statement);
        System.out.println(act);

    }
}
