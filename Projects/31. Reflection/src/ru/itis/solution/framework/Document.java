package ru.itis.solution.framework;

/**
 * 11.05.2021
 * 31. Reflection
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public interface Document {
    String getTitle();
}
