package ru.itis.solution;

import ru.itis.solution.framework.Default;
import ru.itis.solution.framework.Document;

import java.time.LocalDate;
import java.util.StringJoiner;

/**
 * 11.05.2021
 * 31. Reflection
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Act implements Document {
    private LocalDate date;
    private String name;
    private String description;

    public Act(LocalDate date, String name, String description) {
        this.date = date;
        this.name = name;
        this.description = description;
    }

    @Default("ОБЭП")
    private String from;

    @Override
    public String getTitle() {
        return "Акт";
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Act.class.getSimpleName() + "[", "]")
                .add(getTitle())
                .add("date=" + date)
                .add("from=" + from)
                .add("name='" + name + "'")
                .add("description='" + description + "'")
                .toString();
    }
}
