package ru.itis.example;

import java.util.StringJoiner;

/**
 * 11.05.2021
 * 31. Reflection
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Unit {
    private String name;
    private double hp;
    private double mana;

    public Unit() {
    }

    public Unit(String name, double hp, double mana) {
        this.name = name;
        this.hp = hp;
        this.mana = mana;
    }

    public Unit(String name) {
        this.name = name;
    }

    public void manaUp(double manaValue) {
        this.mana += manaValue;
    }

    public void damage(int damageValue) {
        this.hp -= calculateDamage(damageValue);
    }

    public String getName() {
        return name;
    }

    public double getHp() {
        return hp;
    }

    public double getMana() {
        return mana;
    }

    private double calculateDamage(int damageValue) {
        return damageValue / 10.0;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Unit.class.getSimpleName() + "[", "]")
                .add("name='" + name + "'")
                .add("hp=" + hp)
                .add("mana=" + mana)
                .toString();
    }
}
