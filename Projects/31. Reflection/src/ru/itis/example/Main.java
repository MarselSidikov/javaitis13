package ru.itis.example;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

public class Main {

    // Class - класс, который содержит информацию о другом классе
    // Method - класс, который содержит информацию о методах класса (их можно вызывать)
    // Constructor - класс, который содержит информацию о конструкторах класса (с помощью них можно создавать объекты)
    public static void main(String[] args) throws Exception {
        // получение класса как объекта
        // ~ получить информацию о самом классе
	    Class<?> unitClass = Unit.class;
	    // получит массив методов, которые есть в классе Unit
	    Method[] methods = unitClass.getDeclaredMethods();
	    // распечатаем информацию об этих методах
        for (Method method : methods) {
            System.out.println(method.getName() + " " + method.getReturnType());
        }
        // создаю объект на основе пустого (без параметров) конструктора
        Unit newUnit = (Unit)unitClass.newInstance();
        System.out.println(newUnit);
        // получение конструктор
        Constructor<?> unitConstructor = unitClass.getConstructor(String.class);
        Unit anotherUnit = (Unit)unitConstructor.newInstance("Марсель");
        System.out.println(anotherUnit);

        Constructor<?> anotherUnitConstructor = unitClass.getConstructor(String.class, Double.TYPE, Double.TYPE);
        Unit someUnit = (Unit) anotherUnitConstructor.newInstance("Айрат", 1000, 500);
        System.out.println(someUnit);

        Unit unit = new Unit("Максим", 850, 1000);
        Method damageMethod = unitClass.getMethod("damage", int.class);
        damageMethod.invoke(unit, 100);
        System.out.println(unit);

        Field[] fields = unitClass.getDeclaredFields();
        for (Field field : fields) {
            System.out.println(field.getName() + " " + field.getType());
        }

        Field field = unitClass.getDeclaredField("name");
        field.setAccessible(true);
        field.set(unit, "Максимилиан");
        System.out.println(unit);
    }
}
