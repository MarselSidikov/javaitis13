package ru.itis.freemarker;

import freemarker.cache.FileTemplateLoader;
import freemarker.template.Configuration;
import freemarker.template.Template;

import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 15.06.2021
 * 38. Freemarker Example
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main {
    public static void main(String[] args) throws Exception {
        Configuration configuration = new Configuration(Configuration.VERSION_2_3_20);
        configuration.setDefaultEncoding("UTF-8");
        configuration.setTemplateLoader(new FileTemplateLoader(new File("src/main/resources")));

        Template template = configuration.getTemplate("template_for_web.ftlh");

        User user = User.builder()
                .id(1L)
                .age(null)
                .name("Марсель")
                .build();

        List<User> users = new ArrayList<>();
        users.add(User.builder().id(2L).name("Айрат").age(22).build());
        users.add(User.builder().id(3L).name("Виктор").age(24).build());
        users.add(User.builder().id(4L).name("Даниил").age(21).build());
        users.add(User.builder().id(5L).name("Максим").age(22).build());
        users.add(User.builder().id(5L).name("Алия").age(20).build());

        Map<String, Object> attributes = new HashMap<>();
        attributes.put("user", user);
        attributes.put("users", users);

        FileWriter fileWriter = new FileWriter("output.html");
        template.process(attributes, fileWriter);
    }
}
