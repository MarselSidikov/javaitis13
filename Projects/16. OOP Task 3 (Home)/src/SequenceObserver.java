/**
 * 20.02.2021
 * 16. OOP Task 3 (Home)
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
// печатает события, которые происходят в конкретном Sequence
// ДОБАВЛЕН ЭЛЕМЕНТ - значение
public class SequenceObserver {
    public void logAddElement(int value) {
        System.out.println("Был добавлен элемент " + value);
    }
}
