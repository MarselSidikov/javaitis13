/**
 * 20.02.2021
 * 16. OOP Task 3 (Home)
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
// класс для работы с последовательностью чисел
public class Sequence {
    // добавить число в последовательность
    // удалить число из последовательности по индексу
    // 3, 4, 5 -> removeByIndex(1) -> 3, 5
    // удалить число из последовательности по значению
    // 3, 4, 5 -> removeByValue(5) -> 3, 4
    // получить элемент по индексу
    // вставить элемент в индекс - replace()
    // вставить элемент со сдвигом всех остальных - insert()
    private SequenceObserver observer;

    public Sequence(SequenceObserver observer) {
        this.observer = observer;
    }

    public void add(int value) {
        // ...
        observer.logAddElement(value);
    }
}
