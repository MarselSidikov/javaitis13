package object;

import java.util.Scanner;

/**
 * 10.03.2021
 * 20. String
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class MainEquals {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Human marsel = new Human("Марсель", "Сидиков", 27, 1.85);
        Human marsel2 = new Human("Марсель", "Сидиков", 27, 1.85);
        Human marsel3 = new Human("Марсель", "Сидиков", 27, 1.85);
        Human marsel4 = new Human("Марсель", "Сидиков", 27, 1.85);
//        System.out.println(marsel == marsel2);
//        System.out.println(marsel.equals(marsel2));
//        System.out.println(marsel.equals(scanner));
        EqualsUtils equalsUtils = new EqualsUtils();
        System.out.println(equalsUtils.allEquals("Привет", "Привет", "Привет", "Привет"));
        System.out.println(equalsUtils.allEquals(marsel, marsel2, marsel3, marsel4));

    }
}
