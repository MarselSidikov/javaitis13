package object;

import java.util.Objects;

/**
 * 10.03.2021
 * 20. String
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Car {
    private String model;
    private double fuel;

    public Car(String model, double fuel) {
        this.model = model;
        this.fuel = fuel;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Car car = (Car) o;
        return Double.compare(car.fuel, fuel) == 0 &&
                Objects.equals(model, car.model);
    }

}
