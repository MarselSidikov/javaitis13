package object;

import java.util.Scanner;

/**
 * 10.03.2021
 * 20. String
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class MainToString {
    public static void main(String[] args) {
        Human human = new Human("Марсель", "Сидиков", 27, 1.85);
        System.out.println(human); // human.toString
        String humanString = human.toString();
        System.out.println(humanString);
    }
}
