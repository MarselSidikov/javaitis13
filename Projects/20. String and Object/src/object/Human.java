package object;

import java.util.Objects;
import java.util.StringJoiner;

/**
 * 10.03.2021
 * 20. String
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Human {
    private String firstName;
    private String lastName;
    private int age;
    private double height;

    public Human(String firstName, String lastName, int age, double height) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
        this.height = height;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public int getAge() {
        return age;
    }

    public double getHeight() {
        return height;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Human.class.getSimpleName() + "[", "]")
                .add("firstName='" + firstName + "'")
                .add("lastName='" + lastName + "'")
                .add("age=" + age)
                .add("height=" + height)
                .toString();
    }

//    public boolean equals(Human that) {
//        if (that == null) {
//            return false;
//        }
//        return this.age == that.age
//                && this.firstName.equals(that.firstName)
//                && this.lastName.equals(that.lastName)
//                && this.height == that.height;
//    }

    @Override
    public boolean equals(Object object) {
        if (object == null) {
            return false;
        }

        if (this == object) { // сравнение с самим собой
            return true;
        }

        if (!(object instanceof Human)) { // если object не Human
            return false;
        }
        // выполняем преобразования для сравнения
        Human that = (Human)object; // явное нисходящее преобразование
        return this.age == that.age
                && this.firstName.equals(that.firstName)
                && this.lastName.equals(that.lastName)
                && this.height == that.height;
    }

//
//    @Override
//    public boolean equals(Object o) {
//        if (this == o) return true;
//        if (o == null || getClass() != o.getClass()) return false;
//        Human human = (Human) o;
//        return age == human.age &&
//                Double.compare(human.height, height) == 0 &&
//                Objects.equals(firstName, human.firstName) &&
//                Objects.equals(lastName, human.lastName);
//    }
}
