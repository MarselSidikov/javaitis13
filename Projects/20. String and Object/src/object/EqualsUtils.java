package object;

/**
 * 10.03.2021
 * 20. String
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class EqualsUtils {
    public boolean allEquals(Object ... objects) {
        for (int i = 0; i < objects.length - 1; i++) {
            if (!objects[i].equals(objects[i+1])) { // Object.equals(Object)
                return false;
            }
        }
        return true;
    }
}
