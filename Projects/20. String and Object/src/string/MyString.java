package string;

/**
 * 10.03.2021
 * 20. String
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class MyString {
    private final char value[];

    public MyString(char[] value) {
        this.value = value;
    }

//    public void setValue(char value[]) {
//        this.value = value;
//    }

    void replace(int i, char c) {
        value[i] = c;
    }

    public char[] getValue() {
        return value;
    }
}
