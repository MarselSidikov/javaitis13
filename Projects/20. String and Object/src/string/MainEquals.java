package string;

/**
 * 10.03.2021
 * 20. String
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class MainEquals {
    public static void main(String[] args) {
        // s1 и s2 -> один и тот же объект
        String s1 = "Марсель";
        String s2 = "Марсель";

        // s3 и s2 - это разные объекты
        String s3 = new String("Марсель");
        String s4 = new String("Марсель");
        System.out.println(s1 == s2);
        System.out.println(s3 == s4);
        System.out.println(s1.equals(s4));
    }
}
