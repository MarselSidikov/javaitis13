package string;

import java.util.Arrays;

/**
 * 10.03.2021
 * 20. String
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class MainMyString {
    public static void main(String[] args) {
        MyString myString = new MyString(new char[]{'М', 'а', 'р', 'c', 'э', 'л', 'ь'});
        myString.replace(4, 'e');
        System.out.println(Arrays.toString(myString.getValue()));
    }
}
