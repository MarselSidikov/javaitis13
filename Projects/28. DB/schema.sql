-- создание таблицы
create table account
(
    -- идентификатор строки - первичный ключ (идентификатор строки, по умолчанию - уникальный, генерируется базой данных
    id         bigserial primary key,
    -- имя (макс. длина - 20, не может быть пустой)
    first_name varchar(20)        not null,
    last_name  varchar(20)        not null,
    email      varchar(20) unique not null,
    -- опыт, по умолчанию 3, есть проверка на то, чтобы опыт не был меньше 3-х лет
    experience integer default 3 check (experience >= 3),
    is_active  bool    default false
);

-- изменения размера колонки
alter table account
    alter column email type varchar(30);
-- добавляем новую колонку в  таблицу
alter table account
    add column phone varchar(15);
-- добавляем новое ограничение
alter table account
    add constraint check_max_experience check (experience <= 60);
-- добавление данных в таблицу
insert into account(first_name, last_name, email)
values ('Марсель', 'Сидиков', 'sidikov.marsel@gmail.com');

insert into account(first_name, last_name, email, experience)
values ('Айрат', 'Мухутдинов', 'airat@mukhutdinov.ru', 4);

insert into account(first_name, last_name, email, experience, is_active)
values ('Даниил', 'Вдовинов', 'daniil@vdovinov.com', 3, true);

insert into account(first_name, last_name, email)
values ('Максим', 'Поздеев', 'maxpozdeev@mail.ru');

-- обновление данных в  таблице
update account
set phone = '+78372824941'
where id = 1;

update account
set is_active = true
where id = 5;

-- дочерняя таблица
create table car
(
    id       bigserial primary key,
    color    varchar(20),
    model    varchar(20),
    number   varchar(20),
    owner_id bigint,
    foreign key (owner_id) references account (id)
);

insert into car(color, model, number, owner_id)
values ('Черный', 'BMV 3', 'о777аа16', 2);
insert into car(color, model, number, owner_id)
values ('Красный', 'Lada Largus', 'о111аа16', 2);
insert into car(color, model, number, owner_id)
values ('Серый', 'Renault', 'о222аа16', 2);
insert into car(color, model, number, owner_id)
values ('Голубой', 'Bugatti', 'a001aa01', 4);
insert into car(color, model, number)
values ('Белая', 'Solaris', 'у898aa116');
insert into car(color, model, number)
values ('Синяя', 'LADA', 'o111aa116');

update car
set owner_id = 5
where id = 6;
-- многие ко многим - третья таблица
create table driver_car
(
    driver_id bigint,
    car_id    bigint,
    foreign key (driver_id) references account (id),
    foreign key (car_id) references car (id)
);

insert into driver_car(driver_id, car_id)
values (2, 2);
insert into driver_car(driver_id, car_id)
values (1, 3);
insert into driver_car(driver_id, car_id)
values (5, 4);
insert into driver_car(driver_id, car_id)
values (4, 6);
insert into driver_car(driver_id, car_id)
values (1, 6);
insert into driver_car(driver_id, car_id)
values (5, 6);
insert into driver_car(driver_id, car_id)
values (4, 5);

--- получение всех колонок всех пользователей

select *
from account;

--- получение только имен всех пользователей

select first_name
from account;

--- получение имен всех пользователей, но колонка должна называться просто name

select first_name as name
from account;

--- получение имени-фамилии всех пользователей, у которых нет номера телефона
select first_name, last_name
from account
where phone isnull;

--- получение имени всех пользователей, у которых стаж больше 3-х лет
select first_name
from account
where experience > 3;

--- получить имена всех владельцев, у которых есть хотя бы одна машина
select first_name
from account
where id in (
    select distinct owner_id
    from car
    where owner_id notnull);

--- получить имена владельцев машин, у которых более 1-го водителя
select first_name
from account a
where a.id in (
    select owner_id --- возвращает id владельцев, у машин которых более 1-го водителя
    from car c
    where c.id in (
        select car_id -- возвращает id машин у которых более 1-го водителя
        from (
                 --- возвращает id машин и количество их водителей
                 select car_id,
                        count(driver_id)
                            as drivers_count
                 from driver_car
                 group by car_id) cd
        where cd.drivers_count > 1));

-- left join

select *
from account a
         left join car c on a.id = c.owner_id;

-- right join

select *
from account a
         right join car c on a.id = c.owner_id;

-- inner join

select *
from account a
         inner join car c on a.id = c.owner_id;

-- full outer join

select *
from account a
         full outer join car c on a.id = c.owner_id;