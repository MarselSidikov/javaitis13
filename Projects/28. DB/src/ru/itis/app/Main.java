package ru.itis.app;

import ru.itis.models.Account;
import ru.itis.repositories.AccountsRepository;
import ru.itis.repositories.AccountsRepositoryJdbcImpl;

import java.sql.Connection;
import java.sql.DriverManager;

/**
 * 24.04.2021
 * 28. DB
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main {
    public static void main(String[] args) throws Exception {
        Connection connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/javaitis13",
                "postgres", "qwerty007");

        AccountsRepository accountsRepository = new AccountsRepositoryJdbcImpl(connection);
        System.out.println(accountsRepository.findAll());
        System.out.println(accountsRepository.findById(5L));

//        Account account = new Account("Иван", "Уланов", true);
//        accountsRepository.save(account);
//        System.out.println(account);

        Account marsel = accountsRepository.findById(1L);
        marsel.setLastName("Гудайдиев"); // Good idea
        accountsRepository.update(marsel);

    }
}
