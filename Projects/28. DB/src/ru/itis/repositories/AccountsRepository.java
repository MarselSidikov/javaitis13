package ru.itis.repositories;

import ru.itis.models.Account;

import java.util.List;

/**
 * 24.04.2021
 * 28. DB
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
// DAO - Data Access Object -> CRUD-интерфейс + реализации
public interface AccountsRepository {
    List<Account> findAll();
    Account findById(Long id);

    void save(Account account);

    void update(Account account);

    void delete(Account account);
    void deleteById(Long id);
}
