package extended;

import java.util.*;

/**
 * 20.03.2021
 * 23. Generics
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main {

    // raw type - параметризованный тип, используемый без реального параметра типа
    // можно передать что угодно, потому что такой тип - предок всех подтипов
    // мы не знаем, какого реально типа там объекты
    public static void rawList(List list) {
        for (int i = 0; i < list.size(); i++) {
            Object value = list.get(i);
            Animal animal = (Animal)value;
            animal.who();
        }
    }
    // несмотря на то, что Object - это предок всех классов
    // в обобщенный им тип нельзя передать обобщения, в качестве парамера которых указаны его потомки
    public static void objectsList(List<Object> objects) {
        for (int i = 0; i < objects.size(); i++) {
            Object value = objects.get(i);
            Animal animal = (Animal)value;
            animal.who();
        }
    }
    // почему я не могу сюда передать список волков?
    public static void animalsList(List<Animal> list) {
        for (int i = 0; i < list.size(); i++) {
            Animal animal = list.get(i);
            animal.who();
        }
        // в этом методе есть возможность положить тигра
        // а если у меня был список волков?
        // я поломаю структуру списка
        list.add(new Tiger());
    }

    // List<?> аналогичен List, предок всех списков
    public static void wildcardList(List<?> list) {
        for (int i = 0; i < list.size(); i++) {
            Object object = list.get(i);
            System.out.println(object);
        }
        // вы не можете повредить список
        //list.add(new Animal());
        //list.add(new Tiger());
        //list.add(new Wolf());
        //list.add(new Scanner(System.in));
        //list.add(new Object());
    }

    // UPPER BOUNDS
    // принимаю списки, где есть потомки или сами собаки
    // положить в этот список ничего не могу
    public static void upperBoundsList(List<? extends Dog> dogs) {
        for (int i = 0; i < dogs.size(); i++) {
            // мы можем получить собаку
            // потому что гарантируется, что в списке
            // либо собаки, либо потомки собак, и поэтому
            // сработает восходящее преобразование
            Dog dog = dogs.get(i);
            dog.who();
        }
        // почему не могу добавить?
        // а вдруг список был из хороших мальчиков
        // я не должен иметь возможность добавить сюда собаку
//        dogs.add(new Dog());
//        dogs.add(new Wolf());
//        dogs.add(new GoodBoy());
    }

    // LOWER BOUNDS
    public static void lowerBounds(List<? super Wolf> list) {
        for (int i = 0; i < list.size(); i++) {
            // но не можем получить ничего, кроме Object
//            Wolf wolf = list.get(i);
//            GoodBoy goodBoy = list.get(i);
//            Dog dog = list.get(i);
//            Animal animal = list.get(i);

            // почему нельзя получить?
            // потому что вы не знаете, кто там может быть
            Object o = list.get(i);
        }
//        list.add(new Animal());
        // можем добавлять только потомков волка
        // почему можем добавть? Потому что тут точно есть предок волка
        // и вполне корректно его добавить в список
        // если был список волков, добавим туда собаку, хорошего мальчика, волка
        list.add(new Wolf());
        list.add(new Dog());
        list.add(new GoodBoy());
    }

    public static void main(String[] args) {
        Animal animal = new Animal();
        Tiger tiger = new Tiger();
        Wolf wolf = new Wolf();
        Dog dog = new Dog();
        GoodBoy goodBoy = new GoodBoy();

        List<Animal> animals = new ArrayList<>();
        animals.add(animal);
        animals.add(tiger);
        animals.add(wolf);
        animals.add(dog);
        animals.add(goodBoy);
        List<Tiger> tigers = new ArrayList<>();
        tigers.add(tiger);
        List<Wolf> wolves = Arrays.asList(wolf, dog);
//        wolves.add(new Tiger());
        List<Dog> dogs = Arrays.asList(dog, goodBoy);
        List<GoodBoy> goodBoys = Arrays.asList(goodBoy);

        rawList(animals);
        rawList(tigers);
        rawList(wolves);
        rawList(dogs);
        rawList(goodBoys);

        List<Scanner> scanners = new ArrayList<>();
        scanners.add(new Scanner(System.in));
//        rawList(scanners); - работает, но ломает программу

//        objectsList(animals);
//        objectsList(scanners);
//        objectsList(Arrays.asList(new Object())); - ломает логику

        animalsList(animals);
//        animalsList(wolves);

        wildcardList(animals);
        wildcardList(wolves);
        wildcardList(dogs);

        upperBoundsList(dogs);
        upperBoundsList(goodBoys);
//        upperBoundsList(wolf);
        lowerBounds(animals);
        lowerBounds(wolves);
//        lowerBounds(dogs);
        lowerBounds(Arrays.asList(new Object()));
    }
}
