package extended;

/**
 * 20.03.2021
 * 23. Generics
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Animal {
    public void who() {
        System.out.println("Животное");
    }
}
