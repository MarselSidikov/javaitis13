package extended;

/**
 * 20.03.2021
 * 23. Generics
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class GoodBoy extends Dog {
    @Override
    public void who() {
        System.out.println("Хороший мальчик!");
    }
}
