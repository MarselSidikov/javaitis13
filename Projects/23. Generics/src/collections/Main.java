package collections;

import java.util.Scanner;

/**
 * 17.03.2021
 * 23. Generics
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main {
    public static void main(String[] args) {
        // List<String> - создание объектной переменной типа, использование параметризованного типа
        // вместо V я подставил конкретный тип - String
        List<String> strings = new LinkedList<>();
        strings.add("Hello!");
        strings.add("Marsel!");
//        strings.add(5);

        String value = strings.get(0);
        System.out.println(value);
        // Использование параметризованного типа
        // Вместо T подставил Scanner
        ArrayList<Scanner> scanners = new ArrayList<>();
        scanners.add(new Scanner(System.in));
//        scanners.add("hewe");

        Scanner scanner = scanners.get(0);

        ArrayList<Integer> integers = new ArrayList<>(); //?
        integers.add(100);
        integers.add(300);
        integers.add(400);

        int i = integers.get(0);
    }
}
