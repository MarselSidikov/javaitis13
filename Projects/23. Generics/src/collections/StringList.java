package collections;

/**
 * 17.03.2021
 * 23. Generics
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
// class StringList - объявление типа
    // List<String> - использование, String -> V
public class StringList implements List<String> {
    @Override
    public String get(int index) {
        return null;
    }

    @Override
    public void removeFirst(String element) {

    }

    @Override
    public void add(String element) {

    }

    @Override
    public int size() {
        return 0;
    }
}
