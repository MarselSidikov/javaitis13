package collections;

/**
 * 27.02.2021
 * 19. LinkedList
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class LinkedList<D> implements List<D> {

    // Node<C> - объявление параметризованного типа
    public static class Node<C> {
        C value;
        Node<C> next;

        Node(C value) {
            this.value = value;
        }
    }
    // использование с конкретной подстановкой
    private Node<D> first;

    private Node<D> last;

    private int count;

    @Override
    public void add(D value) {
        Node<D> newNode = new Node<>(value);
        if (first == null) {
            this.first = newNode;
        } else {
            // следующий после последнего - новый узел
            last.next = newNode;
        }
        // новый узел теперь последний
        this.last = newNode;
        this.count++;
    }

    @Override
    public D get(int index) {

        // если индекс = 4
        if (index < count && index > -1) {
            Node<D> current = this.first;
            // отсчитываем элементы
            // i = 0, 1, 2, 3, 4
            for (int i = 0; i < index; i++) {
                current = current.next;
            }
            return current.value;
        }
        System.err.println("Вышли за пределы списка");
        return null;
    }

    @Override
    public int size() {
        return count;
    }


    @Override
    public void removeFirst(D element) {
        // a -> b -> c -> d -> null
        // ^
        // f

        // current = a

        Node<D> current = first;

        // removeFirst(a)
        if (current.value == element) {
            // a -> b -> c -> d -> null
            //      ^
            //      f
            first = first.next;
            return;
        }

        // removeFirst(c)
        // a -> b -> c -> d -> null
        //      ^
        //      c
        while (current.next != null && current.next.value != element) {
            current = current.next;
        }

        // a -> b -> -> d -> null
        //      ^
        //      c
        if (current.next != null) {
            current.next = current.next.next;
        }

        if (current.next == null) {
            this.last = current;
        }

        count--;
    }
}
