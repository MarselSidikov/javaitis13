package collections;

/**
 * 13.03.2021
 * 21. Collections
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
// class ArrayList<T> - объявление параметризованного типа
// implements List<T> - использование параметризованного типа, вместо буквы V подставили T
public class ArrayList<T> implements List<T> {
    private static final int DEFAULT_SIZE = 10;
    // int -> Integer -> Object
    private T elements[];
    private int count;

    public ArrayList() {
        this.elements = (T[])new Object[DEFAULT_SIZE];
        this.count = 0;
    }

    @Override
    public T get(int index) {
        if (index >= 0 && index < count) {
            return elements[index];
        }
        return null;
    }

    @Override
    public void add(T element) {
        if (isFull()) {
            resize();
        }
        addLast(element);
    }

    private void addLast(T element) {
        elements[count] = element;
        count++;
    }

    private boolean isFull() {
        return count == elements.length;
    }

    private void resize() {
        T newArray[] = (T[])new Object[elements.length + elements.length / 2];
        for (int i = 0; i < count; i++) {
            newArray[i] = elements[i];
        }
        elements = newArray;
    }

    @Override
    public int size() {
        return count;
    }

    @Override
    public void removeFirst(T element) {
        int positionOfRemovingElement = -1;
        for (int i = 0; i < count; i++) {
            if (elements[i].equals(element)) {
                positionOfRemovingElement = i;
                break;
            }
        }
        for (int i = positionOfRemovingElement; i < count; i++) {
            elements[i] = elements[i + 1];
        }
        count--;
    }
}
