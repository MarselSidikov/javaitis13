package collections;

/**
 * 13.03.2021
 * 21. Collections
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
// объявление параметризованного типа
public interface Collection<E> {
    /**
     * Добавление элемента в коллекцию
     * @param element добавляемый элемент
     */
    void add(E element);

    /**
     * Возвращает количество элементов коллекции
     * @return количество элементов, 0 - если коллекция пустая
     */
    int size();
}
