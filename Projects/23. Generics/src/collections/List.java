package collections;

/**
 * 13.03.2021
 * 21. Collections
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
// interface List<V> - объявление параметризованного типа
// extends Collection<V> - использование параметризованного типа,
public interface List<V> extends Collection<V> {
    /**
     * Получение элемента коллекции по индексу
     * @param index индекс элемента
     * @return элемент, размещенный под этим индексом. Если элемент не найден - возвращается -1
     */
    V get(int index);

    void removeFirst(V element);
}
