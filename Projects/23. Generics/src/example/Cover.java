package example;

/**
 * 17.03.2021
 * 23. Generics
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
// example.Cover - параметризированный тип, для него можно указать в качестве параметра другой тип
public class Cover<T> {
    // phone указывает на объект любого типа
    private T phone;

    public void setPhone(T phone) {
        this.phone = phone;
    }

    public T getPhone() {
        return this.phone;
    }
}
