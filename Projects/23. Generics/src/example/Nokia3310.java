package example;

/**
 * 17.03.2021
 * 23. Generics
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Nokia3310 {
    public void call() {
        System.out.println("Идет вызов...");
    }
}
