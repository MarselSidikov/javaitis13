package example;

public class Main {

    public static void main(String[] args) {
	    Cover<Nokia3310> nokiaCover = new Cover<>();
	    Nokia3310 nokia3310 = new Nokia3310();

//	    cover.setPhone(iPhone);

		nokiaCover.setPhone(nokia3310);

	    Nokia3310 nokiaFromCover = nokiaCover.getPhone();
	    nokiaFromCover.call();

	    Cover<IPhone> iPhoneCover = new Cover<>();
		IPhone iPhone = new IPhone();

//		iPhoneCover.setPhone(nokia3310);
		iPhoneCover.setPhone(iPhone);

		IPhone iPhoneFromCover = iPhoneCover.getPhone();
		iPhoneFromCover.createPhoto();
    }
}
