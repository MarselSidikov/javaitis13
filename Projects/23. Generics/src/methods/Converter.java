package methods;

import java.util.ArrayList;
import java.util.List;

/**
 * 20.03.2021
 * 23. Generics
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Converter {
    public <X, Y> List<Y> convert(List<X> elements, ConvertFunction<X, Y> convertFunction) {
        List<Y> result = new ArrayList<>();
        for (int i = 0; i < elements.size(); i++) {
            X element = elements.get(i);
            Y convertedElement = convertFunction.convert(element);
            result.add(convertedElement);
        }
        return result;
    }
}
