package methods;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * 20.03.2021
 * 23. Generics
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main {
    public static void main(String[] args) {
        Converter converter = new Converter();
        List<String> strings = Arrays.asList("Марсель", "cидиков", "Привет", "как дела");
        List<Boolean> result = converter.convert(strings, element -> Character.isUpperCase(element.charAt(0)));
        System.out.println(result);
    }
}
