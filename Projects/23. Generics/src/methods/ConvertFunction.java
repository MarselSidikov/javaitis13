package methods;

/**
 * 20.03.2021
 * 23. Generics
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public interface ConvertFunction<X, Y> {
    Y convert(X element);
}
