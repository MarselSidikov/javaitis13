class Program4 {
	public static void main(String[] args) {
		int number = 435678;

		int digitsSum = 0;

		while (number != 0) {
			digitsSum = digitsSum + number % 10;
			number = number / 10;
		}

		System.out.println(digitsSum);
	}
	
}