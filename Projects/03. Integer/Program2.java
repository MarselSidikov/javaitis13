class Program2 {
	public static void main(String[] args) {
		int number = 435217;

		/**
		int remainder = number % 2; // 0, 1

		boolean isEven = (remainder == 0); // true, false

		if (isEven) {
			System.out.println("EVEN");
		} else {
			System.out.println("ODD");
		}
		**/

		if (number % 2 == 0) {
			System.out.println("EVEN");
		} else {
			System.out.println("ODD");
		}
	}
}