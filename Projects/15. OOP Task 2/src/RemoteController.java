/**
 * 20.02.2021
 * 15. OOP Task 2
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class RemoteController {
    private TV tv;

    public RemoteController(TV tv) {
        this.tv = tv;
    }

    // делегирование
    public void on(int channelNumber) {
        tv.on(channelNumber);
    }
}
