/**
 * 20.02.2021
 * 15. OOP Task 2
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Program {
    private String name;

    public Program(String name) {
        this.name = name;
    }

    public void show() {
        System.out.println(name);
    }
}
