package com.akvelon.orm.models;

import lombok.*;

/**
 * 02.06.2022
 * 23. Hibernate
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString(exclude = "course")
public class Lesson {

    private Long id;

    private String name;

    private Course course;
}
