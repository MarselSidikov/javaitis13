package com.akvelon.orm.app;

import com.akvelon.orm.models.Course;
import com.akvelon.orm.models.Student;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import java.util.ArrayList;

/**
 * 03.06.2022
 * 23. Hibernate
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public class Main4 {
    public static void main(String[] args) {
        Configuration configuration = new Configuration();
        configuration.configure("hibernate\\hibernate.cfg.xml");
        SessionFactory sessionFactory = configuration.buildSessionFactory();
        Session session = sessionFactory.openSession();

        Transaction transaction = session.beginTransaction();
        Course java = session.find(Course.class, 1L);
        Student marsel = session.find(Student.class, 4L);

        marsel.getCourses().remove(java);


//        Student marsel = Student.builder()
//                .firstName("Марсель")
//                .lastName("Сидиков")
//                .courses(new ArrayList<>())
//                .build();
//
//        Student airat = Student.builder()
//                .firstName("Айрат")
//                .lastName("Мухутдинов")
//                .courses(new ArrayList<>())
//                .build();
//
//        Student aliia = Student.builder()
//                .firstName("Алия")
//                .lastName("Сидикова")
//                .courses(new ArrayList<>())
//                .build();
//
//        marsel.getCourses().add(course);
//        airat.getCourses().add(course);
//        aliia.getCourses().add(course);
//
//        session.save(marsel);
//        session.save(airat);
//        session.save(aliia);

        transaction.commit();
        session.close();
    }
}
