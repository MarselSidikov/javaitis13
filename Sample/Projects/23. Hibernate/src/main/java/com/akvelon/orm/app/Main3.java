package com.akvelon.orm.app;

import com.akvelon.orm.models.Course;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import java.util.List;

/**
 * 03.06.2022
 * 23. Hibernate
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public class Main3 {
    public static void main(String[] args) {
        Configuration configuration = new Configuration();
        configuration.configure("hibernate\\hibernate.cfg.xml");
        SessionFactory sessionFactory = configuration.buildSessionFactory();
        Session session = sessionFactory.openSession();

//        List<Course> courses = session.createQuery("select course from Course course", Course.class)
//                .getResultList();
//        System.out.println(courses);
        Course course = session.find(Course.class, 1L);
        System.out.println(course);
        System.out.println(course.getLessons());
        session.close();
    }
}
