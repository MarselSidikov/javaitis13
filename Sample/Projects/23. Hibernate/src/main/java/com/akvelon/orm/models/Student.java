package com.akvelon.orm.models;

import lombok.*;

import java.util.List;
import java.util.Set;

/**
 * 02.06.2022
 * 23. Hibernate
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString(exclude = "courses")
@EqualsAndHashCode(exclude = "courses")
public class Student {

    private Long id;

    private String firstName;
    private String lastName;

    private Set<Course> courses;
}
