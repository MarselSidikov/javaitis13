package com.akvelon.orm.app;

import com.akvelon.orm.models.Course;
import com.akvelon.orm.models.Lesson;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import java.util.Arrays;

/**
 * 02.06.2022
 * 23. Hibernate
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public class Main {
    public static void main(String[] args) {
        Configuration configuration = new Configuration();
        configuration.configure("hibernate\\hibernate.cfg.xml");
        SessionFactory sessionFactory = configuration.buildSessionFactory();
        Session session = sessionFactory.openSession();

        Transaction transaction = session.beginTransaction();

        Lesson spring = Lesson.builder()
                .name("spring")
                .build();

        Lesson threads = Lesson.builder()
                .name("threads")
                .build();

        Lesson indexes = Lesson.builder()
                .name("indexes")
                .build();

        Lesson queries = Lesson.builder()
                .name("queries")
                .build();

        Lesson tables = Lesson.builder()
                .name("tables")
                .build();

        Lesson divs = Lesson.builder()
                .name("divs")
                .build();

        session.save(spring);
        session.save(threads);
        session.save(indexes);
        session.save(queries);
        session.save(tables);
        session.save(divs);

        Course java = Course.builder()
                .title("Java")
                .lessons(Arrays.asList(spring, threads))
                .build();

        Course sql = Course.builder()
                .title("SQL")
                .lessons(Arrays.asList(indexes, queries))
                .build();

        Course html = Course.builder()
                .title("HTML")
                .lessons(Arrays.asList(tables, divs))
                .build();

        session.save(java);
        session.save(sql);
        session.save(html);

        transaction.commit();

        session.close();
    }
}
