package com.akvelon.orm.models;

import lombok.*;

import java.util.List;
import java.util.Set;

/**
 * 02.06.2022
 * 23. Hibernate
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString(exclude = {"lessons", "students"})
@EqualsAndHashCode(exclude = {"lessons", "students"})
public class Course {

    private Long id;

    private String title;

    private Set<Student> students;

    private List<Lesson> lessons;
}
