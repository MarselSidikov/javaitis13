package com.akvelon.orm.app;

import com.akvelon.orm.models.Student;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

/**
 * 02.06.2022
 * 23. Hibernate
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public class Main2 {
    public static void main(String[] args) {
        Configuration configuration = new Configuration();
        configuration.configure("hibernate\\hibernate.cfg.xml");
        SessionFactory sessionFactory = configuration.buildSessionFactory();
        Session session = sessionFactory.openSession();

        Transaction transaction = session.beginTransaction();
        Student student = Student.builder()
                .firstName("Марсель")
                .lastName("Сидиков")
                .build();

        session.save(student);

        student.setFirstName("Marsel");
        student.setLastName("Sidikov");

        transaction.commit();
        session.close();
    }
}
