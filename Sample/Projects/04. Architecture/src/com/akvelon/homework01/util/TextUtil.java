package com.akvelon.homework01.util;

import java.util.Arrays;
import java.util.List;

/**
 * 11.04.2022
 * 04. Architecture
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public class TextUtil {
    public static List<String> tokenizeBySpace(String text) {
        return Arrays.asList(text.split(" "));
    }
}
