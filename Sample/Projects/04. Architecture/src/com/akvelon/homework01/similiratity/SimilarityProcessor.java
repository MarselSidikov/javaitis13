package com.akvelon.homework01.similiratity;

/**
 * 11.04.2022
 * 04. Architecture
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public interface SimilarityProcessor {
    double calculate(String firstText, String secondText);
}
