package com.akvelon.homework01.similiratity;

import java.util.List;
import java.util.Set;

/**
 * 11.04.2022
 * 04. Architecture
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public class SimilarityProcessorCosineImpl implements SimilarityProcessor {

    private final Tokenizer tokenizer;

    public SimilarityProcessorCosineImpl(Tokenizer tokenizer) {
        this.tokenizer = tokenizer;
    }

    @Override
    public double calculate(String firstText, String secondText) {
        List<String> wordOfFirstText = tokenizer.tokenize(firstText);
        List<String> wordOfSecondText = tokenizer.tokenize(secondText);
        return wordOfFirstText.size() / wordOfSecondText.size();
    }

    private Set<String> createVocabulary(List<String> ... texts) {
        return null;
    }
}
