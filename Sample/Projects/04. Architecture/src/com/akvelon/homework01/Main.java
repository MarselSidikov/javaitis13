package com.akvelon.homework01;

import com.akvelon.homework01.similiratity.SimilarityProcessor;
import com.akvelon.homework01.similiratity.SimilarityProcessorCosineImpl;
import com.akvelon.homework01.similiratity.Tokenizer;
import com.akvelon.homework01.util.TextUtil;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
	    Scanner scanner = new Scanner(System.in);
        String text1 = scanner.nextLine();
        String text2 = scanner.nextLine();

        Tokenizer tokenizer = TextUtil::tokenizeBySpace;

        SimilarityProcessor processor = new SimilarityProcessorCosineImpl(tokenizer);

        System.out.println(processor.calculate(text1, text2));
    }
}
