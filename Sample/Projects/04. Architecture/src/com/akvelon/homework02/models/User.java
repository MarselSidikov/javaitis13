package com.akvelon.homework02.models;

import java.util.ArrayList;
import java.util.List;

/**
 * 11.04.2022
 * 04. Architecture
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public class User {

    public User() {
        this.courses = new ArrayList<>();
    }

    private List<Course> courses;

    public List<Course> getCourses() {
        return courses;
    }
}
