package com.akvelon.homework02.repositories;

import com.akvelon.homework02.models.Course;

import java.util.List;
import java.util.Optional;

/**
 * 11.04.2022
 * 04. Architecture
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public class CoursesRepositoryArrayListImpl implements CoursesRepository {
    @Override
    public void save(Course entity) {

    }

    @Override
    public void update(Course entity) {

    }

    @Override
    public void delete(Course entity) {

    }

    @Override
    public Optional<Course> findById(Long aLong) {
        return Optional.empty();
    }

    @Override
    public List<Course> findAll() {
        return null;
    }
}
