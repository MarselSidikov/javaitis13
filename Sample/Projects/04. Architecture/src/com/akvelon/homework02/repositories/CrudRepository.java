package com.akvelon.homework02.repositories;

import java.util.List;
import java.util.Optional;

/**
 * 11.04.2022
 * 04. Architecture
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public interface CrudRepository<T, ID> {
    void save(T entity);
    void update(T entity);
    void delete(T entity);

    Optional<T> findById(ID id);
    List<T> findAll();

}
