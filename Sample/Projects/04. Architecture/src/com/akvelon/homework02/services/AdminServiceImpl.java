package com.akvelon.homework02.services;

import com.akvelon.homework02.models.Course;
import com.akvelon.homework02.models.User;
import com.akvelon.homework02.repositories.CoursesRepository;
import com.akvelon.homework02.repositories.UsersRepository;

/**
 * 11.04.2022
 * 04. Architecture
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public class AdminServiceImpl implements AdminService {

    private final UsersRepository usersRepository;

    private final CoursesRepository coursesRepository;

    public AdminServiceImpl(UsersRepository usersRepository, CoursesRepository coursesRepository) {
        this.usersRepository = usersRepository;
        this.coursesRepository = coursesRepository;
    }

    @Override
    public void addCourseToUser(Long courseId, Long userId) {
        User user = usersRepository.findById(userId).orElseThrow(IllegalArgumentException::new);
        Course course = coursesRepository.findById(courseId).orElseThrow(IllegalArgumentException::new);

        user.getCourses().add(course);
        usersRepository.save(user);
    }
}
