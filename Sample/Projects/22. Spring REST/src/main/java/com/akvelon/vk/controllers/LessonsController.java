package com.akvelon.vk.controllers;

import com.akvelon.vk.dto.LessonDto;
import com.akvelon.vk.services.LessonsService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 06.06.2022
 * 22. Spring REST
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
@RequiredArgsConstructor
@RestController
@RequestMapping("/lessons")
public class LessonsController {

    private final LessonsService lessonsService;

    @GetMapping()
    public ResponseEntity<List<LessonDto>> getLessons() {
        return ResponseEntity.ok(lessonsService.getLessons());
    }
}
