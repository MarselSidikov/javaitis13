package com.akvelon.vk.services;

import com.akvelon.vk.dto.LessonDto;

import java.util.List;

/**
 * 06.06.2022
 * 22. Spring REST
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public interface LessonsService {
    List<LessonDto> getLessons();
}
