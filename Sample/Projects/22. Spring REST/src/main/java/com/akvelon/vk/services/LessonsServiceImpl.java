package com.akvelon.vk.services;

import com.akvelon.vk.dto.LessonDto;
import com.akvelon.vk.repositories.LessonsRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.akvelon.vk.dto.LessonDto.from;

/**
 * 06.06.2022
 * 22. Spring REST
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
@RequiredArgsConstructor
@Service
public class LessonsServiceImpl implements LessonsService {

    private final LessonsRepository lessonsRepository;

    @Override
    public List<LessonDto> getLessons() {
        return from(lessonsRepository.findAll());
    }
}
