package com.akvelon.vk.services;

import com.akvelon.vk.dto.UserDto;
import com.akvelon.vk.dto.UsersPage;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

/**
 * 31.05.2022
 * 22. Spring REST
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
@Service
public class UsersServiceFakeImpl implements UsersService {

    private final Random random = new Random();

    private static final List<UserDto> users = new ArrayList<>();

    public UsersServiceFakeImpl() {
        users.add(UserDto.builder().id(1L).email("email1@gmail.com").build());
        users.add(UserDto.builder().id(2L).email("email2@gmail.com").build());
        users.add(UserDto.builder().id(3L).email("email3@gmail.com").build());
    }

    @Override
    public UsersPage getUsers(Integer page) {
        return UsersPage.builder()
                .data(users)
                .totalPages(10)
                .size(3)
                .build();
    }

    @Override
    public UserDto addUser(UserDto user) {
        user.setId((long) random.nextInt(100));
        users.add(user);
        return user;
    }

    @Override
    public UserDto getUser(Long userId) {
        return getById(userId);
    }

    @Override
    public UserDto update(Long userId, UserDto newData) {
        UserDto user = getById(userId);
        user.setEmail(newData.getEmail());
        return user;
    }

    @Override
    public void deleteUser(Long userId) {
        users.removeIf(user -> user.getId().equals(userId));
    }

    private UserDto getById(Long userId) {
        return users.stream().filter(user -> user.getId().equals(userId)).limit(1).findFirst()
                .orElseThrow(IllegalArgumentException::new);
    }
}
