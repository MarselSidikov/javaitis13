package com.akvelon.vk.services;

import com.akvelon.vk.dto.UserDto;
import com.akvelon.vk.dto.UsersPage;

/**
 * 31.05.2022
 * 22. Spring REST
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public interface UsersService {
    UsersPage getUsers(Integer page);

    UserDto addUser(UserDto user);

    UserDto getUser(Long userId);

    UserDto update(Long userId, UserDto newData);

    void deleteUser(Long userId);
}
