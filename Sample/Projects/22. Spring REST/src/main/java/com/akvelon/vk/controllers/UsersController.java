package com.akvelon.vk.controllers;

import com.akvelon.vk.dto.PostDto;
import com.akvelon.vk.dto.PostsPage;
import com.akvelon.vk.dto.UserDto;
import com.akvelon.vk.dto.UsersPage;
import com.akvelon.vk.services.UsersService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * 31.05.2022
 * 22. Spring REST
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
@RequiredArgsConstructor
@RestController
@RequestMapping("/users")
public class UsersController {

    private final UsersService usersService;

    @GetMapping
    public ResponseEntity<UsersPage> getUsers(@RequestParam("page") Integer page) {
        return ResponseEntity
                .ok(usersService.getUsers(page));
    }

    @PostMapping
    public ResponseEntity<UserDto> addUser(@RequestBody UserDto user) {
        return ResponseEntity
                .status(HttpStatus.CREATED)
                .body(usersService.addUser(user));
    }

    @PutMapping("/{user-id}")
    public ResponseEntity<UserDto> updateUser(@PathVariable("user-id") Long userId, @RequestBody UserDto newData) {
        return ResponseEntity
                .status(HttpStatus.ACCEPTED)
                .body(usersService.update(userId, newData));
    }

    @GetMapping("/{user-id}")
    public ResponseEntity<UserDto> getUser(@PathVariable("user-id") Long userId) {
        return ResponseEntity
                .ok(usersService.getUser(userId));
    }

    @DeleteMapping("/{user-id}")
    public ResponseEntity<?> deleteUser(@PathVariable("user-id") Long userId) {
        usersService.deleteUser(userId);
        return ResponseEntity.status(HttpStatus.ACCEPTED)
                .build();
    }

    @GetMapping("/{user-id}/posts")
    public ResponseEntity<PostsPage> getUserPosts(@PathVariable("user-id") Long userId) {
        return null;
    }

    @PostMapping("/{user-id}/posts/favorites")
    public ResponseEntity<PostDto> addPostToFavorites(@PathVariable("user-id") Long userId,
                                                      @RequestBody PostDto post) {
        return null;
    }
}
