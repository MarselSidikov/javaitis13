<html>
<head>
    <title>Contracts</title>
</head>
<script>
    function sendContract(title, price) {
        let json = {
            "title": title,
            "price": price
        };

        fetch('/addContract', {
            method: 'POST',
            body: JSON.stringify(json),
            headers: {
                'Content-Type': 'application/json'
            }
        }).then((response) => {
            return response.json();
        }).then((contracts) => {
            let table = document.getElementById('table_with_contracts');

            table.innerHTML = '<tr> ' +
                '            <th>Title</th> ' +
                '            <th>Price</th> ' +
                '        </tr>';

            for (let i = 0; i < contracts.length; i++) {
                let row = table.insertRow(-1);
                let titleCell = row.insertCell(0);
                let priceCell = row.insertCell(1);

                titleCell.innerHTML = contracts[i].title;
                priceCell.innerHTML = contracts[i].price;
            }
        });
    }
</script>
<body>
<div>
    <label>Title
        <input id="title" placeholder="Enter title of contract...">
    </label>
    <label>Price
        <input id="price" placeholder="Enter price of contract...">
    </label>
    <button onclick="sendContract(
        document.getElementById('title').value,
        document.getElementById('price').value)">Send...</button>
</div>
<div>
    <table id="table_with_contracts">
        <tr>
            <th>Title</th>
            <th>Price</th>
        </tr>
    </table>
</div>
</body>
</html>
