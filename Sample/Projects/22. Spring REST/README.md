# Spring REST

## REST BEST PRACTICES

* [Ссылка 1](https://stackoverflow.blog/2020/03/02/best-practices-for-rest-api-design/#h-use-nouns-instead-of-verbs-in-endpoint-paths)
* [Ссылка 2](https://www.freecodecamp.org/news/rest-api-best-practices-rest-endpoint-design-examples/)
* [Ссылка 3](https://www.freecodecamp.org/news/rest-api-best-practices-rest-endpoint-design-examples/)

0) Обмен JSON
1) Используем существительные вместо глаголов в URL
2) Используем множественные числа для обозначения коллекций
3) Используем вложенности для демонстрации отношений между сущностями
4) Фильтрация, сортировка и пагинация
5) GET-method не должен менять состояние сущности

## Дополнительно

Обработка ошибок и единый формат сообщений 
Версионность
Документация
HATEOAS