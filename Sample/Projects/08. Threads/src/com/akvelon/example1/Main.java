package com.akvelon.example1;

import java.util.Scanner;

/**
 * 19.04.2022
 * 08. Threads
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        scanner.nextLine();

        Egg egg = new Egg();
        Hen hen = new Hen();

        egg.start();
        hen.start();

        try {
            egg.join();
            hen.join();
        } catch (InterruptedException e) {
            throw new IllegalStateException(e);
        }

        for (int i = 0; i < 1_000_000; i++) {
            System.out.println(Thread.currentThread().getName() + " " + i);
        }

    }
}
