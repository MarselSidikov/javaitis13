package com.akvelon.example1;

/**
 * 19.04.2022
 * 08. Threads
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public class Egg extends Thread {

    public Egg() {
        super("Egg");
    }

    @Override
    public void run() {
        for (int i = 0; i < 1_000_000; i++) {
            System.out.println(Thread.currentThread().getName() + " " + i);
        }
    }
}
