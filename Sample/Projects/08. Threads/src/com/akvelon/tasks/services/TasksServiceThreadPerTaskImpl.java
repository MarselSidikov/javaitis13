package com.akvelon.tasks.services;

/**
 * 19.04.2022
 * 08. Threads
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
class TasksServiceThreadPerTaskImpl implements TasksService {

    private static int counter = 0;

    @Override
    public void submit(Runnable task) {
        counter++;
        Thread thread = new Thread(task, String.valueOf(counter));
        thread.start();
    }
}
