package com.akvelon.tasks;

import com.akvelon.tasks.services.Services;
import com.akvelon.tasks.services.TasksService;

import java.io.*;
import java.net.URL;
import java.util.List;
import java.util.Scanner;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * 19.04.2022
 * 08. Threads
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public class Main {

    public static void saveFile(String fileName) {
        try {
            URL url = new URL(fileName);
            InputStream in = new BufferedInputStream(url.openStream());
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            byte[] buf = new byte[1024];
            int n;
            while (-1 != (n = in.read(buf))) {
                out.write(buf, 0, n);
            }
            out.close();
            in.close();
            byte[] response = out.toByteArray();
            String newFileName = Thread.currentThread().getName() + "_" + UUID.randomUUID() + ".png";
            FileOutputStream fos = new FileOutputStream("images\\" + newFileName);
            fos.write(response);
            fos.close();
            System.out.println(Thread.currentThread().getName() + " завершил скачивание");
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    public static void main(String[] args) {
        TasksService tasksService = Services.workerThread();

        Scanner scanner = new Scanner(System.in);
        scanner.nextLine();

        try {
            BufferedReader reader = new BufferedReader(new FileReader("links.txt"));
            List<String> links = reader.lines().collect(Collectors.toList());

            for (int i = 0; i < links.size(); i++) {
                scanner.nextLine();
                final int index = i;
                tasksService.submit(()-> saveFile(links.get(index)));
            }

        } catch (FileNotFoundException e) {
            throw new IllegalStateException(e);
        }
    }
}
