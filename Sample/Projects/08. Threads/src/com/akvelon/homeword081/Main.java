package com.akvelon.homeword081;

import java.util.Random;
import java.util.Scanner;

/**
 * 19.04.2022
 * 08. Threads
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Random random = new Random();

        int threadsCount = scanner.nextInt();
        int numbersCount = scanner.nextInt();

        int[] array = new int[numbersCount];

        for (int i = 0; i < array.length; i++) {
            array[i] = random.nextInt(1000);
        }

        int numbersSum = 0;

        for (int i = 0; i < array.length; i++) {
            numbersSum += array[i];
        }

        System.out.println("Expected - " + numbersSum);
    }
}
