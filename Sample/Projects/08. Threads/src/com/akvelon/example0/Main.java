package com.akvelon.example0;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        scanner.nextLine();

        Thread current = Thread.currentThread();
        System.out.println(current.getName());

        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            throw new IllegalStateException(e);
        }
    }
}
