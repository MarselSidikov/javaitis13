package com.akvelon.notification;

import java.util.Random;

/**
 * 19.04.2022
 * 08. Threads
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public class Product {
    private final Random random = new Random();

    private boolean isReady;

    public void consume() {
        this.isReady = false;
        try {
            Thread.sleep(random.nextInt(10000));
        } catch (InterruptedException e) {
            throw new IllegalStateException(e);
        }
    }

    public void produce() {
        this.isReady = true;
        try {
            Thread.sleep(random.nextInt(10000));
        } catch (InterruptedException e) {
            throw new IllegalStateException(e);
        }
    }

    public boolean isConsumed() {
        return !isReady;
    }

    public boolean isProduced() {
        return isReady;
    }
}
