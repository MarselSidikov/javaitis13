package com.akvelon.notification;

/**
 * 19.04.2022
 * 08. Threads
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public class Producer extends Thread {
    private final Product product;

    public Producer(Product product) {
        super("Producer");
        this.product = product;
    }

    @Override
    public void run() {
        while(true) {
            synchronized (product) {
                System.out.println("Producer проверяет продукт");
                // пока продукт не использован
                while (!product.isConsumed()) {
                    // уходим в ожидание на этом продукте
                    try {
                        System.out.println("Producer ушел в ожидание");
                        product.wait();
                    } catch (InterruptedException e) {
                        throw new IllegalStateException(e);
                    }
                }

                if (product.isConsumed()) {
                    System.out.println("Producer убедился, что можно его подготовить");
                    product.produce();
                    System.out.println("Producer подготовил продукт");
                    product.notify();
                    System.out.println("Producer оповестил Consumer");
                }
            }
        }
    }
}
