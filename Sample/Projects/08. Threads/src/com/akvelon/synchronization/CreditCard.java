package com.akvelon.synchronization;

/**
 * 19.04.2022
 * 08. Threads
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public class CreditCard {
    private int amount;

    public CreditCard(int amount) {
        this.amount = amount;
    }

    public boolean hasMoney(int sum) {
        return amount >= sum;
    }

    public void buy(int sum) {
        if (sum > amount) {
            throw new IllegalStateException("Нет денег");
        }
        this.amount -= sum;
    }
}
