package com.akvelon.vk.filters;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

import static com.akvelon.vk.interceptors.CreationCsrfTokenInterceptor.CSRF_TOKEN_NAME;

/**
 * 13.06.2022
 * spring-boot-mvc
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public class CsrfFilter implements Filter {

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;

        HttpSession session = request.getSession(false);

        if (request.getMethod().equals("POST")) {
            if (session != null) {
                String token = (String) session.getAttribute(CSRF_TOKEN_NAME);
                String tokenFromRequest = request.getParameter(CSRF_TOKEN_NAME);

                if (token.equals(tokenFromRequest)) {
                    filterChain.doFilter(servletRequest, servletResponse);
                } else {
                    response.sendRedirect("/login?error");
                }
            } else {
                response.sendRedirect("/login?error");
            }
            return;
        }

        filterChain.doFilter(servletRequest, servletResponse);
    }
}
