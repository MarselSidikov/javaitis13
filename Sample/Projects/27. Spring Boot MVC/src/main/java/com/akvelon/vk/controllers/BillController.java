package com.akvelon.vk.controllers;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * 13.06.2022
 * spring-boot-mvc
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
@Slf4j
@RequestMapping("/bill")
@Controller
public class BillController {

    @PostMapping
    public String sendMoney(@RequestParam("sum") Integer sum, @RequestParam("to") String to) {
        log.info("send {} sum to {}", sum, to);
        // TODO: запретить передачу токена в URL
        return "redirect:/bill";
    }

    @GetMapping
    public String getBillPage() {
        return "bill";
    }
}
