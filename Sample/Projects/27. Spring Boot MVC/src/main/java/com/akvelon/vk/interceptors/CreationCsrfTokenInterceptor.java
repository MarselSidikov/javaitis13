package com.akvelon.vk.interceptors;

import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.Objects;
import java.util.UUID;

/**
 * 13.06.2022
 * spring-boot-mvc
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
@Component
public class CreationCsrfTokenInterceptor implements HandlerInterceptor {

    public static final String CSRF_TOKEN_NAME = "_csrfToken";

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
        HttpSession session = request.getSession();

        String token = (String) session.getAttribute(CSRF_TOKEN_NAME);

        if (token == null) {
            token = UUID.randomUUID().toString();
            session.setAttribute(CSRF_TOKEN_NAME, token);
        }

        Objects.requireNonNull(modelAndView).addObject(CSRF_TOKEN_NAME, token);
    }
}
