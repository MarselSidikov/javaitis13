package com.akvelon.vk.config;

import com.akvelon.vk.filters.AuthenticationFilter;
import com.akvelon.vk.filters.CsrfFilter;
import com.akvelon.vk.filters.LoginFilter;
import com.akvelon.vk.interceptors.CreationCsrfTokenInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * 13.06.2022
 * spring-boot-mvc
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
@Configuration
public class ApplicationConfig implements WebMvcConfigurer {

    @Autowired
    private CreationCsrfTokenInterceptor creationCsrfTokenInterceptor;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(creationCsrfTokenInterceptor);
    }

    @Bean
    public FilterRegistrationBean<AuthenticationFilter> authenticationFilter() {
        FilterRegistrationBean<AuthenticationFilter> authenticationFilterRegistration = new FilterRegistrationBean<>();

        authenticationFilterRegistration.setFilter(new AuthenticationFilter()
                .protectUrl("/bill")
                .protectUrl("/profile"));

        authenticationFilterRegistration.addUrlPatterns("/*");
        authenticationFilterRegistration.setOrder(Ordered.HIGHEST_PRECEDENCE);

        return authenticationFilterRegistration;
    }

    @Bean
    public FilterRegistrationBean<LoginFilter> loginFilter() {
        FilterRegistrationBean<LoginFilter> loginFilterRegistration = new FilterRegistrationBean<>();

        loginFilterRegistration.setFilter(LoginFilter.builder()
                        .loginProcessingUrl(new AntPathRequestMatcher("/login/**", "POST"))
                        .usernameParameter("email")
                        .passwordParameter("password")
                        .successRedirectUrl("/bill")
                        .unsuccessfulRedirectUrl("/login?error")
                .build());

        loginFilterRegistration.addUrlPatterns("/*");
        loginFilterRegistration.setOrder(Ordered.HIGHEST_PRECEDENCE);

        return loginFilterRegistration;
    }

    @Bean
    public FilterRegistrationBean<CsrfFilter> csrfFilter() {
        FilterRegistrationBean<CsrfFilter> csrfFilterRegistration = new FilterRegistrationBean<>();

        csrfFilterRegistration.setFilter(new CsrfFilter());
        csrfFilterRegistration.addUrlPatterns("/*");
        csrfFilterRegistration.setOrder(Ordered.HIGHEST_PRECEDENCE);

        return csrfFilterRegistration;
    }
}
