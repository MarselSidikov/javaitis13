package com.akvelon.vk.filters;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * 13.06.2022
 * spring-boot-mvc
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class LoginFilter implements Filter {

    private AntPathRequestMatcher loginProcessingUrl;
    private String usernameParameter;
    private String passwordParameter;
    private String unsuccessfulRedirectUrl;
    private String successRedirectUrl;

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;

        if (loginProcessingUrl.matches(request)) {
            String username = request.getParameter(usernameParameter);
            String password = request.getParameter(passwordParameter);

            if (username.equals("sidikov.marsel@gmail.com") && password.equals("qwerty007")) {
                HttpSession session = request.getSession(true);
                session.setAttribute("isAuthenticated", true);
                response.sendRedirect(successRedirectUrl);
            } else {
                response.sendRedirect(unsuccessfulRedirectUrl);
            }
        } else {
            filterChain.doFilter(request, response);
        }
    }
}
