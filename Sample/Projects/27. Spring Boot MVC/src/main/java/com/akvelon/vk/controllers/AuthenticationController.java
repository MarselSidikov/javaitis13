package com.akvelon.vk.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 13.06.2022
 * spring-boot-mvc
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
@RequestMapping("/login")
@Controller
public class AuthenticationController {

    @GetMapping
    public String getLoginPage() {
        return "login";
    }
}
