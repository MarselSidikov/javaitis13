package com.akvelon.vk.filters;

import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.util.AntPathMatcher;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * 13.06.2022
 * spring-boot-mvc
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public class AuthenticationFilter implements Filter {

    private final List<AntPathRequestMatcher> matchers = new ArrayList<>();

    public AuthenticationFilter protectUrl(String url) {
        this.matchers.add(new AntPathRequestMatcher(url));
        return this;
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;


        if (matchers.stream().anyMatch(matcher -> matcher.matches(request))) {
            HttpSession session = request.getSession(false);
            if (session != null) {
                Boolean isAuthenticated = (Boolean) session.getAttribute("isAuthenticated");

                if (isAuthenticated != null && isAuthenticated) {
                    filterChain.doFilter(request, response);
                } else {
                    response.sendRedirect("/login?error");
                }
                return;
            } else {
                response.sendRedirect("/login?error");
                return;
            }
        }
        filterChain.doFilter(servletRequest, servletResponse);
    }
}
