# Spring MVC

![image](img/1.png)

## Аннотации и классы

### MVC

* `ModelAndView` - содержит атрибуты и название `view`
* `Model` - используется только для атрибутов

* `@RequestParam` - получение параметра запроса
* `@RequestHeader` - получение заголовка запроса
* `@CookieValue` - для получения значения конкретной `Cookie`
* `@RequestBody` - для получения тела запроса
* `@ResponseBody` - явно указываем, что возвращает тело ответа, а не название `view`

## Путь запроса в Spring MVC

```
DispatcherServlet
    - handlerMappings [RequestMappgingHandlerMapping, BeanNameUrlHandlerMapping, ...]
    - doService(request, response)
        - doDispatch(request, response)
            HandlerExecutionChain getHandler(request)
                AbstractHandlerMapping
                handler = HandlerExecutionChain getHandler(request)
                    - HandlerMethod
            HandlerAdapter handlerAdapter = getHandlerAdapter(handler);
                - returnValueHandlers  [..., ViewNameMethodReturnValueHandler, RequestResponseBodyMethodProcessor ...]
            modelAndView = handlerAdapter.handle(request, response, handler);
                ServletInvocableHandlerMethod  
                    - invokeAndHandle(request, modelAndViewContainer)
                        HandlerMethodReturnValueHandlerComposite
                        - handleReturnValue(...)
                            - selectHandler()
                        - handleReturnValue
                            JSON
                            RequestResponseBodyMethodProcessor [messageConverters]
            renderView()/convert()
```