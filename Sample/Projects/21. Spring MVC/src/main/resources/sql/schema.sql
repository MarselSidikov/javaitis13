create table file (
    id serial primary key ,
    original_file_name varchar(255),
    storage_file_name varchar(255),
    size bigint,
    mime_type varchar(50),
    description varchar(255)
);