package com.akvelon.shop.services.impl;

import com.akvelon.shop.dto.FileDto;
import com.akvelon.shop.models.FileInfo;
import com.akvelon.shop.repositories.FilesRepository;
import com.akvelon.shop.services.FilesService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.UUID;

/**
 * 24.05.2022
 * 19. Servlet Application
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
@Service
public class FilesServiceImpl implements FilesService {

    private final String storagePath;

    private final FilesRepository filesRepository;

    public FilesServiceImpl(@Value("${storage.path}") String storagePath, FilesRepository filesRepository) {
        this.storagePath = storagePath;
        this.filesRepository = filesRepository;
    }

    @Override
    public void upload(FileDto file) {
        String fileName = file.getFileName();
        String extension = fileName.substring(fileName.lastIndexOf("."));

        FileInfo fileInfo = FileInfo.builder()
                .description(file.getDescription())
                .mimeType(file.getMimeType())
                .originalFileName(fileName)
                .size(file.getSize().longValue())
                .storageFileName(UUID.randomUUID() + extension)
                .build();

        filesRepository.save(fileInfo);
        try {
            Files.copy(file.getFileStream(), Paths.get(storagePath + "\\" + fileInfo.getStorageFileName()));
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }

    }

    @Override
    public void writeFileToStream(String storageFileName, OutputStream outputStream) {
        try {
            Files.copy(Paths.get(storagePath + "\\" + storageFileName), outputStream);
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    public FileDto getFileInformation(String storageFileName) {
        FileInfo fileInfo = filesRepository.findByStorageFileName(storageFileName).orElseThrow(IllegalArgumentException::new);

        return FileDto.builder()
                .description(fileInfo.getDescription())
                .fileName(fileInfo.getOriginalFileName())
                .mimeType(fileInfo.getMimeType())
                .size(fileInfo.getSize().intValue())
                .build();
    }


}
