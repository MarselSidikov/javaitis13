package com.akvelon.shop.repositories;

import com.akvelon.shop.models.User;

import java.util.List;

/**
 * 19.05.2022
 * JavaConfig
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public interface UsersRepository extends CrudRepository<User, Long> {
    List<User> findAllByEmailLike(String email);

    List<User> findAll(Integer pageNumber, int pageSize);
}
