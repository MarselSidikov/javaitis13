package com.akvelon.shop.services.impl;

import com.akvelon.shop.dto.UserDto;
import com.akvelon.shop.models.User;
import com.akvelon.shop.repositories.UsersRepository;
import com.akvelon.shop.services.SearchService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.akvelon.shop.dto.UserDto.from;

/**
 * 23.05.2022
 * 19. Servlet Application
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
@RequiredArgsConstructor
@Service
public class SearchServiceImpl implements SearchService {

    private final UsersRepository usersRepository;

    @Override
    public List<UserDto> searchByEmail(String email) {
        return from(usersRepository.findAllByEmailLike(email));
    }
}
