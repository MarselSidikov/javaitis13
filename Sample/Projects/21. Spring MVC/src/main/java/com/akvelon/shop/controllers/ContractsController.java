package com.akvelon.shop.controllers;

import com.akvelon.shop.dto.ContractDto;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

/**
 * 29.05.2022
 * 21. Spring MVC
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
@Controller
public class ContractsController {

    // TODO: просто для примера!
    private static List<ContractDto> contracts = new ArrayList<>();

    @RequestMapping(value = "/contracts", method = RequestMethod.GET)
    public String getContractsPage() {
        return "contracts";
    }

    @RequestMapping(value = "/addContract", method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<ContractDto> addContracts(@RequestBody ContractDto contract) {
        contracts.add(contract);
        return contracts;
    }
}
