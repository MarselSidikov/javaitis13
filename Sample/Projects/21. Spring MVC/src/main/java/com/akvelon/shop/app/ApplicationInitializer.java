package com.akvelon.shop.app;

import com.akvelon.shop.config.ApplicationConfig;
import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;

/**
 * 27.05.2022
 * 21. Spring MVC
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public class ApplicationInitializer implements WebApplicationInitializer {

    @Override
    public void onStartup(ServletContext servletContext) throws ServletException {
        // поднимаем конфигурацию на основе аннотации для Web-приложений на Spring
        AnnotationConfigWebApplicationContext springWebContext = new AnnotationConfigWebApplicationContext();
        // привязать контекст нашего приложения
        springWebContext.register(ApplicationConfig.class);
        // создаем объект ServletContextListener-а

        ContextLoaderListener listener = new ContextLoaderListener(springWebContext);
        servletContext.addListener(listener);

        // создать DispatcherServlet
        ServletRegistration.Dynamic dispatcherServlet = servletContext.addServlet("dispatcher",
                new DispatcherServlet(springWebContext));

        dispatcherServlet.setLoadOnStartup(1);
        dispatcherServlet.addMapping("/");
    }
}
