package com.akvelon.shop.dto;

import lombok.Data;

/**
 * 24.05.2022
 * 19. Servlet Application
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
@Data
public class ContractDto {
    private String title;
    private Double price;
}
