package com.akvelon.shop.services;

import com.akvelon.shop.dto.UserDto;
import com.akvelon.shop.models.User;

import java.util.List;

/**
 * 23.05.2022
 * 19. Servlet Application
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public interface SearchService {
    List<UserDto> searchByEmail(String email);
}
