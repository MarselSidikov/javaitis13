package com.akvelon.shop.controllers;

import com.akvelon.shop.dto.AddUserDto;
import com.akvelon.shop.dto.UserDto;
import com.akvelon.shop.models.User;
import com.akvelon.shop.services.UsersService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 27.05.2022
 * 21. Spring MVC
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
@RequiredArgsConstructor
@Controller
@RequestMapping("/users")
public class UsersController {

    private final UsersService usersService;

    @RequestMapping(method = RequestMethod.GET)
    public String getUsersPage(@RequestParam("page") Integer page, Model model) {
            List<UserDto> users = usersService.getAll(page);

            model.addAttribute("users", users);
            return "users";
    }

    @RequestMapping(method = RequestMethod.POST)
    public String addNewUser(AddUserDto user) {
        usersService.addUser(user);
        return "redirect:/users?page=0";
    }

    @RequestMapping("/{user-id}")
    public String getUserPage(@PathVariable("user-id") Long id, Model model,
                              @RequestHeader("User-Agent") String userAgent,
                              @CookieValue("JSESSIONID") String sessionId,
                              HttpServletResponse response,
                              HttpServletRequest request) {
        System.out.println(userAgent);
        System.out.println(sessionId);
        model.addAttribute("user", usersService.getUser(id));
        return "user";
    }
}
