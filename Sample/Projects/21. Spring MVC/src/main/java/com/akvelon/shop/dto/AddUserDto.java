package com.akvelon.shop.dto;

import lombok.Data;

/**
 * 27.05.2022
 * 21. Spring MVC
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
@Data
public class AddUserDto {
    private String email;
    private String password;
}
