package com.akvelon.shop.repositories.impl;

import com.akvelon.shop.models.User;
import com.akvelon.shop.repositories.UsersRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;

import java.util.*;

/**
 * 19.05.2022
 * JavaConfig
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
@RequiredArgsConstructor
@Repository
public class UsersRepositorySpringJdbcImpl implements UsersRepository {

    private static final String USERS_TABLE_NAME = "account";

    //language=SQL
    private static final String SQL_SELECT_BY_EMAIL_LIKE = "select * from " + USERS_TABLE_NAME + " where email ilike :email";

    //language=SQL
    private static final String SQL_SELECT_BY_ID = "select * from " + USERS_TABLE_NAME + " where id = :id";

    //language=SQL
    private static final String SQL_SELECT_ALL = "select * from " + USERS_TABLE_NAME + " order by id limit :limit offset :offset";


    private final NamedParameterJdbcTemplate jdbcTemplate;

    private static final RowMapper<User> userRowMapper = (row, rowNum) ->
            User.builder()
                .id(row.getLong("id"))
                .email(row.getString("email"))
                .password(row.getString("password"))
            .build();

    @Override
    public void save(User entity) {
        Map<String, Object> params = new HashMap<>();

        params.put("email", entity.getEmail());
        params.put("password", entity.getPassword());

        SimpleJdbcInsert simpleJdbcInsert = new SimpleJdbcInsert(jdbcTemplate.getJdbcTemplate().getDataSource());

        entity.setId(simpleJdbcInsert
                .withTableName(USERS_TABLE_NAME)
                        .usingGeneratedKeyColumns("id")
                .executeAndReturnKey(params).longValue());
    }

    @Override
    public Optional<User> findById(Long id) {
        SqlParameterSource parameterSource = new MapSqlParameterSource( Collections.singletonMap("id", id));
        try {
            return Optional.ofNullable(jdbcTemplate.queryForObject(SQL_SELECT_BY_ID, parameterSource, userRowMapper));
        } catch (EmptyResultDataAccessException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public List<User> findAllByEmailLike(String email) {
        return jdbcTemplate.query(SQL_SELECT_BY_EMAIL_LIKE,
                Collections.singletonMap("email", "%" + email + "%"),
                userRowMapper);
    }

    @Override
    public List<User> findAll(Integer pageNumber, int pageSize) {
        Map<String, Object> params = new HashMap<>();
        params.put("limit", pageSize);
        params.put("offset", pageNumber * pageSize);

        return jdbcTemplate.query(SQL_SELECT_ALL,
                params,
                userRowMapper);
    }
}
