package com.akvelon.shop.repositories;

import java.util.Optional;

/**
 * 19.05.2022
 * JavaConfig
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public interface CrudRepository<T, ID> {
    void save(T entity);

    Optional<T> findById(ID id);
}
