package com.akvelon.shop.services;

import com.akvelon.shop.dto.AddUserDto;
import com.akvelon.shop.dto.UserDto;

import java.util.List;

/**
 * 23.05.2022
 * 19. Servlet Application
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public interface UsersService {
    List<UserDto> getAll(Integer pageNumber);

    void addUser(AddUserDto user);

    UserDto getUser(Long id);
}
