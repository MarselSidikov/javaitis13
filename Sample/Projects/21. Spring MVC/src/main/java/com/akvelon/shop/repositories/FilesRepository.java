package com.akvelon.shop.repositories;

import com.akvelon.shop.models.FileInfo;

import java.util.Optional;

/**
 * 24.05.2022
 * 19. Servlet Application
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public interface FilesRepository extends CrudRepository<FileInfo, Long>  {
    Optional<FileInfo> findByStorageFileName(String storageFileName);
}
