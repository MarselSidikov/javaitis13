package com.akvelon.shop.respoitories;

import io.zonky.test.db.postgres.embedded.DatabasePreparer;
import io.zonky.test.db.postgres.embedded.EmbeddedPostgres;
import io.zonky.test.db.postgres.embedded.FlywayPreparer;
import io.zonky.test.db.postgres.embedded.PreparedDbProvider;
import io.zonky.test.db.postgres.junit.EmbeddedPostgresRules;
import io.zonky.test.db.postgres.junit.SingleInstancePostgresRule;
import io.zonky.test.db.postgres.junit5.EmbeddedPostgresExtension;
import io.zonky.test.db.postgres.junit5.PreparedDbExtension;
import io.zonky.test.db.postgres.junit5.SingleInstancePostgresExtension;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;

import javax.sql.DataSource;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.function.Consumer;

import static org.junit.jupiter.api.Assertions.*;

/**
 * 12.05.2022
 * 16. DataBase Test
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
class DiscountsRepositoryJdbcTemplateImplTest {

    private DiscountsRepositoryJdbcTemplateImpl discountsRepository;

    @BeforeEach
    void setUp() {
        PreparedDbProvider preparedDbProvider = PreparedDbProvider.forPreparer(dataSource -> {
            Connection connection = dataSource.getConnection();
            connection.createStatement().executeUpdate("create table discount\n" +
                    "(\n" +
                    "    id bigserial primary key,\n" +
                    "    type        varchar(20),\n" +
                    "    percents double precision\n" +
                    ");" +
                    "" +
                    "insert into discount(type, percents)\n" +
                    "values ('PERCENTS', 50);\n" +
                    "insert into discount(type, percents)\n" +
                    "values ('PERCENTS', 10);");
        });

        try {
            discountsRepository =
                    new DiscountsRepositoryJdbcTemplateImpl(preparedDbProvider.createDataSource());
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Test
    void smokeTest() {
        System.out.println(discountsRepository.applyAllDiscounts(Arrays.asList(50.0, 90.0, 100.0)));
    }
}