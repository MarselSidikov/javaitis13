create table discount
(
    id bigserial primary key,
    type        varchar(20),
    percents double precision
);