import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

import java.sql.*;

/**
 * 13.05.2022
 * 16. DataBase Test
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public class Main3 {
    public static void main(String[] args) throws Exception {
        HikariConfig config = new HikariConfig();
        config.setJdbcUrl("jdbc:postgresql://localhost:5432/akvelon_db");
        config.setUsername("postgres");
        config.setPassword("qwerty007");
        config.setMaximumPoolSize(20);

        HikariDataSource dataSource = new HikariDataSource(config);

        Connection connection = dataSource.getConnection();
        PreparedStatement preparedStatement = connection.prepareStatement("select * from discount where id > ?");
        PreparedStatement preparedStatement1 = connection.prepareStatement("select * from discount where id < ?");
        preparedStatement.setLong(1, 0);
        preparedStatement1.setLong(1, 1);

        preparedStatement.execute();
        preparedStatement.execute();
        preparedStatement.execute();
        preparedStatement.execute();
        preparedStatement.execute();
        preparedStatement.execute();
//        preparedStatement.execute();
//        preparedStatement.execute();
        preparedStatement1.execute();
        preparedStatement1.execute();
//        preparedStatement1.execute();
//        preparedStatement1.execute();
//        preparedStatement1.execute();
//        preparedStatement1.execute();
//        preparedStatement1.execute();

        printCurrentPreparedStatements(connection, "End");
        int i = 0;
    }

    private static void printCurrentPreparedStatements(Connection connection, String additionalMessage) throws SQLException {
        Statement getPreparedStatementsStatement = connection.createStatement();
        ResultSet preparedStatements = getPreparedStatementsStatement.executeQuery("select * from pg_prepared_statements");

        while (preparedStatements.next()) {
            System.out.println(preparedStatements.getString("name") + " " + preparedStatements.getString("statement") + " " + additionalMessage);
        }

    }
}
