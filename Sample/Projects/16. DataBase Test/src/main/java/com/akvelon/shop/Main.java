package com.akvelon.shop;

import com.akvelon.shop.dto.OrdersPricesDto;
import com.akvelon.shop.respoitories.DiscountsRepository;
import com.akvelon.shop.respoitories.DiscountsRepositoryJdbcTemplateImpl;
import com.akvelon.shop.services.DiscountsService;
import com.akvelon.shop.services.DiscountsServiceImpl;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

import java.util.Arrays;

/**
 * 12.05.2022
 * 16. DataBase Test
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public class Main {
    public static void main(String[] args) {

        HikariConfig config = new HikariConfig();
        config.setJdbcUrl("jdbc:postgresql://localhost:5432/akvelon_db");
        config.setUsername("postgres");
        config.setPassword("qwerty007");
        config.setMaximumPoolSize(20);

        HikariDataSource dataSource = new HikariDataSource(config);


        DiscountsRepository discountsRepository = new DiscountsRepositoryJdbcTemplateImpl(dataSource);
        DiscountsService discountsService = new DiscountsServiceImpl(discountsRepository);

        System.out.println(discountsService.applyDiscounts(OrdersPricesDto.builder()
                .prices(Arrays.asList(50.0, 90.0, 100.0))
                .build()));
    }

}
