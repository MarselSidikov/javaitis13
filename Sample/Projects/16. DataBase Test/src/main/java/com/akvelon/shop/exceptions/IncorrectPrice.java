package com.akvelon.shop.exceptions;

/**
 * 12.05.2022
 * 16. DataBase Test
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public class IncorrectPrice extends RuntimeException {
    public IncorrectPrice(String message) {
        super(message);
    }
}
