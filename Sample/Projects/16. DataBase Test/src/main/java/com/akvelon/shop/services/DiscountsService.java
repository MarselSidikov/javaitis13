package com.akvelon.shop.services;

import com.akvelon.shop.dto.DiscountsForPricesDto;
import com.akvelon.shop.dto.OrdersPricesDto;

/**
 * 09.05.2021
 * shop-service
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public interface DiscountsService {

    DiscountsForPricesDto applyDiscounts(OrdersPricesDto ordersPrices);
}
