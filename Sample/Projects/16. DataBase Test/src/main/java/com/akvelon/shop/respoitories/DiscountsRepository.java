package com.akvelon.shop.respoitories;

import com.akvelon.shop.dto.DiscountsForPricesDto;

import java.util.List;

/**
 * 12.05.2022
 * 16. DataBase Test
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public interface DiscountsRepository {
    DiscountsForPricesDto applyAllDiscounts(List<Double> prices);
}
