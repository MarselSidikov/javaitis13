package com.akvelon.shop.services;

import com.akvelon.shop.dto.DiscountsForPricesDto;
import com.akvelon.shop.dto.OrdersPricesDto;
import com.akvelon.shop.exceptions.IncorrectPrice;
import com.akvelon.shop.respoitories.DiscountsRepository;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * 09.05.2021
 * shop-service
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class DiscountsServiceImpl implements DiscountsService {

    private final DiscountsRepository discountsRepository;

    public DiscountsServiceImpl(DiscountsRepository discountsRepository) {
        this.discountsRepository = discountsRepository;
    }

    @Override
    public DiscountsForPricesDto applyDiscounts(OrdersPricesDto ordersPrices) {
        if (ordersPrices.getPrices().stream().anyMatch(value -> value < 0)) {
            throw new IncorrectPrice("Price must be positive");
        }
        return discountsRepository.applyAllDiscounts(ordersPrices.getPrices());
    }
}
