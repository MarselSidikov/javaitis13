package com.akvelon.repositories;

import com.akvelon.models.Pay;

import javax.sql.DataSource;
import javax.xml.crypto.Data;
import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;

/**
 * 04.05.2022
 * 14. Jdbc Example
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public class PaysRepositoryJdbcImpl implements PaysRepository {

    //language=SQL
    private static final String SQL_INSERT = "insert into pay (date, description) values (?, ?);";

    //language=SQL
    private static final String SQL_SELECT_BY_ID = "select * from pay where id = ?;";

    //language=SQL
    private static final String SQL_SELECT_ALL = "select * from pay;";

    private final DataSource dataSource;

    public PaysRepositoryJdbcImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    private static final Function<ResultSet, Pay> payRowMapper = row -> {
        try {
            return Pay.builder()
                    .date(LocalDate.parse(row.getString("date")))
                    .description(row.getString("description"))
                    .id(row.getLong("id"))
                    .build();
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    };

    @Override
    public void save(Pay entity) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_INSERT, Statement.RETURN_GENERATED_KEYS)) {

            statement.setString(1, entity.getDate().toString());
            statement.setString(2, entity.getDescription());

            statement.executeUpdate();

            try (ResultSet generatedId = statement.getGeneratedKeys()) {
                if (generatedId.next()) {
                    entity.setId(generatedId.getLong("id"));
                } else throw new SQLException("Can't retrieve id");
            }
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public void update(Pay entity) {

    }

    @Override
    public Optional<Pay> findById(Long id) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_SELECT_BY_ID)) {

            statement.setLong(1, id);

            try (ResultSet resultSet = statement.executeQuery()) {
                if (resultSet.next()) {
                    return Optional.of(payRowMapper.apply(resultSet));
                } else {
                    return Optional.empty();
                }
            }

        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public List<Pay> findAll() {
        try (Connection connection = dataSource.getConnection();
             Statement statement = connection.createStatement();
             ResultSet resultSet = statement.executeQuery(SQL_SELECT_ALL)) {

            List<Pay> pays = new ArrayList<>();

            while (resultSet.next()) {
                pays.add(payRowMapper.apply(resultSet));
            }

            return pays;
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public void delete(Pay entity) {

    }

    @Override
    public void deleteById(Long aLong) {

    }
}
