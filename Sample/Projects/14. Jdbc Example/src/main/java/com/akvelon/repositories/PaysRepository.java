package com.akvelon.repositories;

import com.akvelon.models.Pay;

/**
 * 04.05.2022
 * 14. Jdbc Example
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public interface PaysRepository extends CrudRepository<Pay, Long> {
}
