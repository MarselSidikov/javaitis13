package com.akvelon;

import com.akvelon.models.Pay;
import com.akvelon.repositories.PaysRepository;
import com.akvelon.repositories.PaysRepositoryJdbcImpl;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

import java.time.LocalDate;

/**
 * 06.05.2022
 * 14. Jdbc Example
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public class Main2 {
    public static void main(String[] args) {
        HikariConfig config = new HikariConfig();
        config.setJdbcUrl("jdbc:postgresql://localhost:5432/akvelon_db");
        config.setUsername("postgres");
        config.setPassword("qwerty007");
        config.setMaximumPoolSize(20);

        HikariDataSource dataSource = new HikariDataSource(config);

        PaysRepository paysRepository = new PaysRepositoryJdbcImpl(dataSource);

        Pay pay = Pay.builder()
                .date(LocalDate.now())
                .description("Первый платеж")
                .build();

        Pay pay1 = Pay.builder()
                .date(LocalDate.now())
                .description("Второй платеж")
                .build();

        paysRepository.save(pay);
        paysRepository.save(pay1);

        System.out.println(paysRepository.findById(pay1.getId()));
        System.out.println(paysRepository.findAll());
    }
}
