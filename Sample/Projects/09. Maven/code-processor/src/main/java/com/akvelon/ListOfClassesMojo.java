package com.akvelon;

/*
 * Copyright 2001-2005 The Apache Software Foundation.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.project.MavenProject;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

@Mojo(name = "list-of-classes", defaultPhase = LifecyclePhase.COMPILE)
public class ListOfClassesMojo extends AbstractMojo {

    @Parameter(defaultValue = "${project}", required = true, readonly = true)
    private MavenProject project;

    @Parameter(defaultValue = "${project.build.outputDirectory}", required = true)
    private String outputFolderFileName;

    @Parameter(name = "listOfClassesFileName", required = true)
    private String listOfClassesFileName;

    @Override
    public void execute() throws MojoExecutionException {
        File outputFolder = new File(outputFolderFileName);

        File listOfClassesFile = new File(outputFolder, listOfClassesFileName);

        try (BufferedWriter writer = new BufferedWriter(new FileWriter(listOfClassesFile))) {
            getLog().info("Output file for list of classes is - " + listOfClassesFileName);

            String sourceFolderFileName = project.getBuild().getSourceDirectory();

            Files.walk(Paths.get(sourceFolderFileName))
                    .filter(Files::isRegularFile)
                    .forEach(file -> {
                        try {
                            writer.write(file.getFileName().toString());
                            writer.newLine();
                        } catch (IOException e) {
                            throw new IllegalArgumentException(e);
                        }
                    });

            getLog().info("Finish work");
        } catch (IOException e) {
            throw new MojoExecutionException(e);
        }
    }
}
