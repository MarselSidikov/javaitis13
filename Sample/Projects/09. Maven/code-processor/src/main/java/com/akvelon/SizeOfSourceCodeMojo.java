package com.akvelon;

import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.project.MavenProject;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@Mojo(name = "size-of-source-code", defaultPhase = LifecyclePhase.COMPILE)
public class SizeOfSourceCodeMojo extends AbstractMojo {

    @Parameter(defaultValue = "${project}", required = true, readonly = true)
    private MavenProject project;

    @Parameter(defaultValue = "${project.build.outputDirectory}", required = true)
    private String outputFolderFileName;

    @Parameter(name = "fileForSizeValueFileName", required = true)
    private String fileForSizeValueFileName;

    @Override
    public void execute() throws MojoExecutionException, MojoFailureException {
        File outputFolder = new File(outputFolderFileName);

        File fileForSizeValue = new File(outputFolder, fileForSizeValueFileName);

        try (BufferedWriter writer = new BufferedWriter(new FileWriter(fileForSizeValue))) {
            getLog().info("Output file for size value is - " + fileForSizeValueFileName);

            String sourceFolderFileName = project.getBuild().getSourceDirectory();

            long size = Files.walk(Paths.get(sourceFolderFileName))
                    .filter(Files::isRegularFile)
                    .map(SizeOfSourceCodeMojo::getSizeOfFile)
                    .mapToLong(Long::longValue)
                    .sum();

            writer.write(String.valueOf(size));
            getLog().info("Finish work");
        } catch (IOException e) {
            throw new MojoExecutionException(e);
        }
    }

    private static long getSizeOfFile(Path path) {
        try {
            return Files.size(path);
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
