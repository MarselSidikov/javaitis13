package com.akvelon.text.app;

import com.akvelon.text.TextUtil;
import org.apache.commons.io.IOUtils;

import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;

/**
 * 20.04.2022
 * 09. Maven
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public class Main {
    public static void main(String[] args) {
        TextUtil textUtil = new TextUtil();
        try {
            String text = IOUtils.toString(new FileReader(args[0]));
            System.out.println(Arrays.toString(textUtil.tokenize(text)));
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }
}
