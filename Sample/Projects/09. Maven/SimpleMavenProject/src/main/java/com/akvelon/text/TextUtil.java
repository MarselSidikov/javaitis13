package com.akvelon.text;

import java.io.*;
import java.util.Properties;

/**
 * 20.04.2022
 * 09. Maven
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public class TextUtil {

    private final String separator;

    public TextUtil() {
        Properties properties = new Properties();
        try {
            InputStream inputStream = getClass().getResourceAsStream("/application.properties");
            Reader reader = new InputStreamReader(inputStream);
            properties.load(reader);
            this.separator = properties.getProperty("tokenizer.separator");
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    public String[] tokenize(String text) {
        return text.split(separator);
    }
}
