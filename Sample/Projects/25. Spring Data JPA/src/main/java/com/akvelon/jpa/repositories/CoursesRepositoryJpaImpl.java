package com.akvelon.jpa.repositories;

import com.akvelon.jpa.models.Course;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;

/**
 * 06.06.2022
 * 24. JPA
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
@Repository
public class CoursesRepositoryJpaImpl implements CoursesRepository {

    @PersistenceContext
    private EntityManager entityManager;

    @Transactional
    @Override
    public void save(Course course) {
        entityManager.persist(course);
    }

    @Override
    public List<Course> findAllByLesson_Name(String lessonName) {
        TypedQuery<Course> query = entityManager
                .createQuery("select course from Course course " +
                        "left join course.lessons lesson " +
                        "where lesson.name = :name", Course.class);

        query.setParameter("name", lessonName);
        return query.getResultList();
    }
}
