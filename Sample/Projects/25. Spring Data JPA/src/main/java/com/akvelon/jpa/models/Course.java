package com.akvelon.jpa.models;

import lombok.*;

import javax.persistence.*;
import java.util.List;
import java.util.Set;

/**
 * 02.06.2022
 * 23. Hibernate
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString(exclude = {"lessons", "students"})
@EqualsAndHashCode(exclude = {"lessons", "students"})
@Entity
public class Course {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String title;

    @ManyToMany(mappedBy = "courses")
    private Set<Student> students;

    @OneToMany(mappedBy = "course")
    private List<Lesson> lessons;
}
