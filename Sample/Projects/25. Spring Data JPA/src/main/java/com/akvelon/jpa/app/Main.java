package com.akvelon.jpa.app;

import com.akvelon.jpa.config.ApplicationConfig;
import com.akvelon.jpa.models.Course;
import com.akvelon.jpa.repositories.CoursesRepository;
import com.akvelon.jpa.repositories.LessonsRepository;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * 06.06.2022
 * 24. JPA
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public class Main {
    public static void main(String[] args) {
        ApplicationContext context = new AnnotationConfigApplicationContext(ApplicationConfig.class);
        LessonsRepository lessonsRepository = context.getBean(LessonsRepository.class);

        System.out.println(lessonsRepository.findAll());

    }
}
