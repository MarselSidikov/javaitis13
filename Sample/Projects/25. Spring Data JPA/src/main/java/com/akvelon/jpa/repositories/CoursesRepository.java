package com.akvelon.jpa.repositories;

import com.akvelon.jpa.models.Course;

import java.util.List;

/**
 * 06.06.2022
 * 25. Spring Data JPA
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public interface CoursesRepository {
    void save(Course course);

    List<Course> findAllByLesson_Name(String lessonName);
}
