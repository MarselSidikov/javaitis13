package com.akvelon.jpa.repositories;

import com.akvelon.jpa.models.Lesson;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * 06.06.2022
 * 25. Spring Data JPA
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public interface LessonsRepository extends JpaRepository<Lesson, Long> {
}
