import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.time.LocalDateTime;

/**
 * 14.06.2022
 * aspects-demo
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public class FileAccessorImpl {

    private LocalDateTime lastFileOpenTime;

    public InputStream openFile(String fileName) {
        lastFileOpenTime = LocalDateTime.now();
        try {
            return new FileInputStream((fileName));
        } catch (FileNotFoundException e) {
            throw new IllegalArgumentException(e);
        }
    }

    public LocalDateTime getLastFileOpenTime() {
        return lastFileOpenTime;
    }
}
