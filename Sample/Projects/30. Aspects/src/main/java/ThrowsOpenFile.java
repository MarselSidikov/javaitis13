import org.springframework.aop.ThrowsAdvice;

/**
 * 14.06.2022
 * aspects-demo
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public class ThrowsOpenFile implements ThrowsAdvice {
    public void afterThrowing(Exception ex) {
        System.out.println("Было исключение " + ex.getMessage());
    }
}
