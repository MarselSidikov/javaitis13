import org.springframework.aop.framework.ProxyFactory;

/**
 * 14.06.2022
 * aspects-demo
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public class Main {
    public static void main(String[] args) {
        FileAccessorImpl target = new FileAccessorImpl();

        ProxyFactory proxyFactory = new ProxyFactory(target);
        proxyFactory.addAdvice(new AfterOpenFile());
        proxyFactory.addAdvice(new BeforeOpenFile());
        proxyFactory.addAdvice(new ThrowsOpenFile());

        FileAccessorImpl proxied = (FileAccessorImpl) proxyFactory.getProxy();

        System.out.println(proxied.getClass().getName());
        proxied.openFile("input.txt");
    }
}
