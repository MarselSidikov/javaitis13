import org.springframework.aop.AfterReturningAdvice;

import java.lang.reflect.Method;

/**
 * 14.06.2022
 * aspects-demo
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public class AfterOpenFile implements AfterReturningAdvice {

    @Override
    public void afterReturning(Object returnValue, Method method, Object[] args, Object target) throws Throwable {
        System.out.println("Возвращаемое значение " + returnValue);
        System.out.println("Метод " + method);
        System.out.println("Был открыт файл " + args[0]);
        System.out.println("Класс исходного объекта " + target);
    }
}
