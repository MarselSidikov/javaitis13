import org.springframework.aop.MethodBeforeAdvice;

import java.lang.reflect.Method;

/**
 * 14.06.2022
 * aspects-demo
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public class BeforeOpenFile implements MethodBeforeAdvice {
    @Override
    public void before(Method method, Object[] args, Object target) throws Throwable {
        System.out.println("Открывается файл - " + args[0]);
    }
}
