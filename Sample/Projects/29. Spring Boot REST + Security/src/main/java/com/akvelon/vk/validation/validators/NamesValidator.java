package com.akvelon.vk.validation.validators;

import com.akvelon.vk.validation.annotations.NotSameNames;
import org.springframework.data.util.ReflectionUtils;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

/**
 * 30.06.2022
 * 29. Spring Boot REST + Security
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public class NamesValidator implements ConstraintValidator<NotSameNames, Object> {

    private String[] fieldsNames;

    @Override
    public void initialize(NotSameNames notSameNames) {
        this.fieldsNames = notSameNames.fields();
    }

    @Override
    public boolean isValid(Object object, ConstraintValidatorContext context) {
        List<String> fieldsValues = new ArrayList<>();

        for (String fieldName : fieldsNames) {
            try {
                Field field = ReflectionUtils.findRequiredField(object.getClass(), fieldName);
                field.setAccessible(true);
                fieldsValues.add((String) field.get(object));
            } catch (IllegalAccessException e) {
                throw new IllegalArgumentException(e);
            }
        }
        return fieldsValues.size() == fieldsValues.stream().distinct().count();
    }
}
