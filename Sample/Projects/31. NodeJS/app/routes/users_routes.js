bodyParser = require('body-parser').json();

module.exports = function (app, fs) {
    app.post('/users', bodyParser, function (request, response) {
        let body = request.body;
        fs.appendFile('storage/data.txt', body.name + ' ' + body.surname + '\n',
            function (err) {
                if (err) {
                    throw err;
                }

                console.log('Saved');
                response.redirect("/");

            })
    });

    app.get('/users', function (request, response) {
        fs.readFile('storage/data.txt', 'utf8', function (err, data) {
            let lines = data.split('\n');
            let result = [];

            for (let i = 0; i < lines.length; i++) {
                result.push({
                    'name' : lines[i].split(' ')[0],
                    'surname': lines[i].split(' ')[1]
                });
            }

            response.setHeader('Content-Type', 'application/json');
            response.send(JSON.stringify(result));
        });
    });
}