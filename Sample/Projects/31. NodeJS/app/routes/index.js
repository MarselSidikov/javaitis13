const usersRoutes = require('./users_routes');

module.exports = function (app, fs) {
    usersRoutes(app, fs);
}