INSERT INTO course (title) VALUES ('Java');
INSERT INTO course (title) VALUES ('SQL');
INSERT INTO course (title) VALUES ('HTML');
INSERT INTO course (title) VALUES ('JavaScript');
INSERT INTO course (title) VALUES ('Kafka');

INSERT INTO lesson (name, course_id) VALUES ('spring', 1);
INSERT INTO lesson (name, course_id) VALUES ('threads', 1);
INSERT INTO lesson (name, course_id) VALUES ('indexes', 2);
INSERT INTO lesson (name, course_id) VALUES ('queries', 2);
INSERT INTO lesson (name, course_id) VALUES ('tables', 3);
INSERT INTO lesson (name, course_id) VALUES ('divs', 3);
INSERT INTO lesson (name, course_id) VALUES ('tables', 2);

INSERT INTO student (first_name, last_name) VALUES ('Марсель', 'Сидиков');
INSERT INTO student (first_name, last_name) VALUES ('Айрат', 'Мухутдинов');
INSERT INTO student (first_name, last_name) VALUES ('Алия', 'Сидикова');

INSERT INTO student_course (student_id, course_id) VALUES (1, 1);
INSERT INTO student_course (student_id, course_id) VALUES (2, 1);
INSERT INTO student_course (student_id, course_id) VALUES (3, 2);
INSERT INTO student_course (student_id, course_id) VALUES (3, 3);