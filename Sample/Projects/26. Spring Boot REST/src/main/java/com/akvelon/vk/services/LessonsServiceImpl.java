package com.akvelon.vk.services;

import com.akvelon.vk.dto.LessonDto;
import com.akvelon.vk.dto.LessonsPage;
import com.akvelon.vk.dto.NewLesson;
import com.akvelon.vk.exceptions.NotFoundException;
import com.akvelon.vk.models.Lesson;
import com.akvelon.vk.repositories.LessonsRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import static com.akvelon.vk.dto.LessonDto.from;

/**
 * 06.06.2022
 * 22. Spring REST
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
@RequiredArgsConstructor
@Service
public class LessonsServiceImpl implements LessonsService {

    private final LessonsRepository lessonsRepository;

    @Value("${default.page.size}")
    private int defaultPageSize;

    @Override
    public LessonsPage getLessons(int page) {
        PageRequest pageRequest = PageRequest.of(page, defaultPageSize);
        Page<Lesson> lessonsPage = lessonsRepository.findAll(pageRequest);
        return LessonsPage.builder()
                .lessons(from(lessonsPage.getContent()))
                .totalPages(lessonsPage.getTotalPages())
                .build();
    }

    @Transactional
    @Override
    public LessonDto addLesson(NewLesson newLesson) {
        Lesson lesson = Lesson.builder()
                .name(newLesson.getName())
                .build();

        lessonsRepository.save(lesson);
        return from(lesson);
    }

    @Override
    public LessonDto getLesson(Long lessonId) {
        return from(lessonsRepository.findById(lessonId).orElseThrow(
                () -> new NotFoundException("Урок с идентификатором <" + lessonId + "> не найден")));
    }
}
