package com.akvelon.vk.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * 09.06.2022
 * 26. Spring Boot REST
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Schema(description = "Страница с уроками и общее количество страниц")
public class LessonsPage {
    @Schema(description = "Список уроков")
    private List<LessonDto> lessons;
    @Schema(description = "Количество доступных страниц", example = "5")
    private Integer totalPages;
}
