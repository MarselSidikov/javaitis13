package com.akvelon.vk.controllers.api;

import com.akvelon.vk.dto.ExceptionDto;
import com.akvelon.vk.dto.LessonDto;
import com.akvelon.vk.dto.LessonsPage;
import com.akvelon.vk.dto.NewLesson;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * 09.06.2022
 * 26. Spring Boot REST
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
@RequestMapping("/api/lessons")
public interface LessonsApi {

    @Operation(summary = "Получение списка уроков")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Страница с уроками",
                    content = {
                            @Content(mediaType = "application/json",
                                    schema = @Schema(implementation = LessonsPage.class))
                    }
            )
    })
    @GetMapping
    ResponseEntity<LessonsPage> getLessons(@Parameter(description = "Номер страницы") @RequestParam("page") int page);

    @Operation(summary = "Добавление урока")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Добавленный урок",
                    content = {
                            @Content(mediaType = "application/json",
                                    schema = @Schema(implementation = LessonDto.class))
                    }
            )
    })
    @PostMapping
    ResponseEntity<LessonDto> addLesson(@RequestBody NewLesson lesson);

    @Operation(summary = "Получение конкретного урока")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Описание урока",
                    content = {
                            @Content(mediaType = "application/json",
                                    schema = @Schema(implementation = LessonDto.class))
                    }
            ),
            @ApiResponse(responseCode = "404", description = "Сведения об ошибке",
                    content = {
                            @Content(mediaType = "application/json",
                                    schema = @Schema(implementation = ExceptionDto.class))
                    }
            )

    })
    @GetMapping("/{lesson-id}")
    ResponseEntity<LessonDto> getLesson(@Parameter(description = "Идентификатор урока", example = "1")
                                        @PathVariable("lesson-id") Long lessonId);
}
