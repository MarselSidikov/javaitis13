package com.akvelon.vk.repositories;

import com.akvelon.vk.models.Lesson;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * 06.06.2022
 * 22. Spring REST
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public interface LessonsRepository extends JpaRepository<Lesson, Long> {
}
