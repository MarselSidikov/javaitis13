package com.akvelon.vk.services;

import com.akvelon.vk.dto.LessonDto;
import com.akvelon.vk.dto.LessonsPage;
import com.akvelon.vk.dto.NewLesson;

import java.util.List;

/**
 * 06.06.2022
 * 22. Spring REST
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public interface LessonsService {
    LessonsPage getLessons(int page);

    LessonDto addLesson(NewLesson lesson);

    LessonDto getLesson(Long lessonId);
}
