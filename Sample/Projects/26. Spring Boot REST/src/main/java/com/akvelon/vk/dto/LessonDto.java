package com.akvelon.vk.dto;

import com.akvelon.vk.models.Lesson;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.stream.Collectors;

/**
 * 06.06.2022
 * 22. Spring REST
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
@Schema(description = "Урок")
public class LessonDto {
    @Schema(description = "идентификатор урока", example = "1")
    private Long id;
    @Schema(description = "Название урока", example = "Lesson")
    private String name;

    public static LessonDto from(Lesson lesson) {
        return LessonDto
                .builder()
                    .id(lesson.getId())
                    .name(lesson.getName())
                .build();
    }

    public static List<LessonDto> from(List<Lesson> lessons) {
        return lessons.stream()
                .map(LessonDto::from)
                .collect(Collectors.toList());
    }
}
