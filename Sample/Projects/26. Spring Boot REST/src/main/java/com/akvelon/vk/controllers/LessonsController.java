package com.akvelon.vk.controllers;

import com.akvelon.vk.controllers.api.LessonsApi;
import com.akvelon.vk.dto.LessonDto;
import com.akvelon.vk.dto.LessonsPage;
import com.akvelon.vk.dto.NewLesson;
import com.akvelon.vk.services.LessonsService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 06.06.2022
 * 22. Spring REST
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
@RequiredArgsConstructor
@RestController
public class LessonsController implements LessonsApi {

    private final LessonsService lessonsService;

    @Override
    public ResponseEntity<LessonsPage> getLessons(int page) {
        return ResponseEntity.ok(lessonsService.getLessons(page));
    }

    @Override
    public ResponseEntity<LessonDto> addLesson(NewLesson lesson) {
        return ResponseEntity.status(HttpStatus.CREATED)
                .body(lessonsService.addLesson(lesson));
    }

    @Override
    public ResponseEntity<LessonDto> getLesson(Long lessonId) {
        return ResponseEntity.ok(lessonsService.getLesson(lessonId));
    }
}
