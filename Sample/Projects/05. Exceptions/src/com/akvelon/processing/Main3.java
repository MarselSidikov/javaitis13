package com.akvelon.processing;

import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * 12.04.2022
 * 05. Exceptions
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public class Main3 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int[] results = new int[3];
        int i = 0;

        while (true) {
            try {
                int a = scanner.nextInt();
                int b = scanner.nextInt();
                int c = a / b;
                results[i] = c;
                i++;
                System.out.println(c);
            } catch (ArithmeticException | InputMismatchException e) {
                scanner.nextLine();
                System.out.println("Повторите ввод");
            } catch (RuntimeException e) {
                System.out.println("Ошибка!!");
            }
        }
    }
}
