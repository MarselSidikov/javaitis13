package com.akvelon.checked.example;

import java.io.FileNotFoundException;
import java.util.Random;

/**
 * 12.04.2022
 * 05. Exceptions
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public class A {

    public void a() {
        throw new IllegalArgumentException();
    }

    public void b() throws FileNotFoundException {
        Random random = new Random();
        if (random.nextInt(100) / 2 == 0) {
            throw new FileNotFoundException();
        }
    }
}
