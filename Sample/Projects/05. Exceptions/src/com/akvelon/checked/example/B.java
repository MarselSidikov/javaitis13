package com.akvelon.checked.example;

import java.io.FileNotFoundException;

/**
 * 12.04.2022
 * 05. Exceptions
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public class B {
    private A a;

    public B(A a) {
        this.a = a;
    }

    public void c() {
        a.a();
    }

//    public void d() {
//        try {
//            a.b();
//        } catch (FileNotFoundException e) {
//            System.out.println("Проблемс");
//        }
//    }

    public void d() throws FileNotFoundException {
        a.b();
    }
}
