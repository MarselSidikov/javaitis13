package com.akvelon.checked.application.validators;

import com.akvelon.checked.application.validators.exceptions.EmailNotCorrectException;

/**
 * 12.04.2022
 * 05. Exceptions
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public interface EmailValidator {
    /**
     * Выполняет проверку email
     * @param email email, который необходимо проверить
     * @throws com.akvelon.checked.application.validators.exceptions.EmailNotCorrectException, если Email не валидный
     */
    void validate(String email) throws EmailNotCorrectException;
}
