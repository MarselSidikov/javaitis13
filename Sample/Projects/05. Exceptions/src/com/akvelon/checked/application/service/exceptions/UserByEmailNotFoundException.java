package com.akvelon.checked.application.service.exceptions;

/**
 * 12.04.2022
 * 05. Exceptions
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public class UserByEmailNotFoundException extends RuntimeException {
}
