package com.akvelon.checked.application.repository;

import java.io.*;
import java.util.Optional;
import java.util.function.Function;

/**
 * 12.04.2022
 * 05. Exceptions
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public class UsersRepositoryFileBasedImpl implements UsersRepository {

    private final String fileName;

    public UsersRepositoryFileBasedImpl(String fileName) {
        this.fileName = fileName;
    }

    private static final Function<String[], User> USER_MAP_FUNCTION = elements -> new User(elements[0], elements[1]);

//    @Override
//    public void save(User user) {
//        Writer writer = null;
//        BufferedWriter bufferedWriter = null;
//        try {
//            writer = new FileWriter(fileName, true);
//            bufferedWriter = new BufferedWriter(writer);
//            bufferedWriter.write(user.getEmail() + "|" + user.getPassword());
//            bufferedWriter.newLine();
//        } catch (IOException e) {
//            throw new IllegalStateException(e);
//        } finally {
//            if (writer != null) {
//                try {
//                    writer.close();
//                } catch (IOException ignore) {}
//            }
//            if (bufferedWriter != null) {
//                try {
//                    bufferedWriter.close();
//                } catch (IOException ignore) {}
//            }
//        }
//    }

    @Override
    public void save(User user) {
        try (Writer writer = new FileWriter(fileName, true);
             BufferedWriter bufferedWriter = new BufferedWriter(writer)) {
            bufferedWriter.write(user.getEmail() + "|" + user.getPassword());
            bufferedWriter.newLine();
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    public Optional<User> findByEmail(String email) {
        try (Reader reader = new FileReader(fileName)) {
            BufferedReader bufferedReader = new BufferedReader(reader);
            return bufferedReader.lines()
                    .map(line -> line.split("\\|"))
                    .map(USER_MAP_FUNCTION)
                    .filter(user -> user.getEmail().equals(email))
                    .findFirst();
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }
}
