package com.akvelon.checked.application.validators;

import com.akvelon.checked.application.validators.exceptions.EmailNotCorrectException;

/**
 * 12.04.2022
 * 05. Exceptions
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public class SimpleEmailValidatorImpl implements EmailValidator {

    @Override
    public void validate(String email) {
        if (!email.contains("@")) {
            throw new EmailNotCorrectException();
        }
    }
}
