package com.akvelon.examples;

public class MainStackOverflow {
    // n! = 1 * 2 * 3 * ... * n
    // 4! = 1 * 2 * 3 * 4 = 24
    // 5! = 4! * 5 = 120
    // n! = (n-1)! * n
    // 0! = 1
    public static int f(int n) {
//        if (n == 0) {
//            return 1;
//        }

        return f(n - 1) * n;
    }

    public static void main(String[] args) {
        f(5);
    }
}
