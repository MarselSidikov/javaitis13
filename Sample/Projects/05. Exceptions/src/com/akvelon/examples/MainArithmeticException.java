package com.akvelon.examples;

/**
 * 12.04.2022
 * 05. Exceptions
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public class MainArithmeticException {
    public static void main(String[] args) {
        int a = 10;
        int b = 0;

        int c = a / b;
    }
}
