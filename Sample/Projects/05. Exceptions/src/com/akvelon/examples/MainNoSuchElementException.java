package com.akvelon.examples;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

/**
 * 12.04.2022
 * 05. Exceptions
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public class MainNoSuchElementException {
    public static void main(String[] args) {
        List<Integer> integerList = Arrays.asList(4, 5, 6);
        Iterator<Integer> integerIterator = integerList.iterator();
        integerIterator.next();
        integerIterator.next();
        integerIterator.next();

        integerIterator.next();
    }
}
