package com.akvelon.examples;

/**
 * 12.04.2022
 * 05. Exceptions
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public class MainNullPointerException {
    public static void main(String[] args) {
        int[] array = null;
        array[1] = 10;
    }
}
