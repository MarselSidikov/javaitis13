package com.akvelon.object;

/**
 * 14.04.2022
 * 06. Objects, toString, Collections
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public class Human {
    private String firstName;
    private String lastName;
    private int age;

    public Human(String firstName, String lastName, int age) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public int getAge() {
        return age;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj instanceof Human) {
            // явное нисходящее преобразование
            Human that = (Human)obj;

            return this.age == that.age
                    && this.firstName.equals(that.firstName)
                    && this.lastName.equals(that.lastName);

        }
        return false;
    }

//    public boolean equals(Human that) {
//        if (that == null) {
//            return false;
//        }
//
//        if (this == that) {
//            return true;
//        }
//
//        return this.age == that.age
//                && this.firstName.equals(that.firstName)
//                && this.lastName.equals(that.lastName);
//    }
}
