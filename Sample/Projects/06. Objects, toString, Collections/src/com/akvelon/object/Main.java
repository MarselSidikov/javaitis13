package com.akvelon.object;

/**
 * 14.04.2022
 * 06. Objects, toString, Collections
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public class Main {

    public static boolean allEquals(Object... objects) {
        for (int i = 0; i < objects.length - 1; i++) {
            if (!objects[i].equals(objects[i + 1])) {
                return false;
            }
        }
        return true;
    }

    public static void main(String[] args) {
        Human a = new Human("Марсель", "Сидиков", 28);
        Human b = new Human("Марсель", "Сидиков", 28);
        Human c = new Human("Марсель", "Сидиков", 28);

        System.out.println(allEquals(a, b, c));
    }
}
