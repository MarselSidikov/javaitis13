package com.akvelon.string;

/**
 * 14.04.2022
 * 06. Objects, toString, Collections
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public class Main {
    public static void main(String[] args) {
        String s1 = "Hello";
        String s2 = "Hello";

        String s3 = new String("Hello");
        String s4 = new String("Hello");

        System.out.println(s1 == s2);
        System.out.println(s3 == s4);

        String internedS3 = s3.intern();
        String internedS4 = s4.intern();
        System.out.println("----");
        System.out.println(s1 == internedS3);
        System.out.println(s2 == internedS3);
        System.out.println(internedS3 == internedS4);
    }
}
