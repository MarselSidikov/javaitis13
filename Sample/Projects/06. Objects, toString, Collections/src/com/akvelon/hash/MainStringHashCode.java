package com.akvelon.hash;

/**
 * 14.04.2022
 * 06. Objects, toString, Collections
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public class MainStringHashCode {

    public static int stringHashCode(String string) {
        int hash = 0;

        char[] value = string.toCharArray();

        for (int i = 0; i < value.length; i++) {
            hash = 31 * hash + value[i];
        }

        return hash;
    }

    // 72 101 108 108 111

    // hash = 31 * 0 + 72
    // hash = 31 * (31 * 0 + 72) + 101
    // hash = 31 * (31 * (31 * 0 + 72) + 101) + 108
    // hash = 31 * (31 * (31 * (31 * 0 + 72) + 101) + 108) + 108
    // hash = 31 * (31 * (31 * (31 * (31 * 0 + 72) + 101) + 108) + 108) + 111

    // 31 * 31 * 31 * 31 * 72 + 31 * 31 * 31 * 101 + 31 * 31 * 108 + 31 * 108 + 111
    // 31^4 * 72 + 31^3 * 101 + 31^2 * 108 + 31^1 * 108 + 31^0 * 111
    // 31^[N-1] * V[0] + 31^[N-2] * V[1] + 31^[N-3] * V[2] + 31[N-4] * V[3] + 31[N-5] * V[4]
    // SUM 31^(N - i - 1) * V[i]
    // Что интересного?
    // участвует и код символа и позиция символа Hello != Helol

    public static void main(String[] args) {
        int code = "Hello".hashCode();
        System.out.println("Codes " + " " + (int)'H' + " " +  (int)'e' + " " + (int)'l' + " " +  (int)'l' + " " + (int)'o');


    }
}
