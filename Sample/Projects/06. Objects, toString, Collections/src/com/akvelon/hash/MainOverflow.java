package com.akvelon.hash;

/**
 * 14.04.2022
 * 06. Objects, toString, Collections
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public class MainOverflow {
    public static void main(String[] args) {
        int i = 2_147_483_647 + 1;
        System.out.println(i);
    }
}
