package com.akvelon.homework;

/**
 * 27.04.2022
 * 12. Classloaders and Proxy
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public class ProxyClassLoader extends ClassLoader {

    private final String folderName;

    private After after;
    private Before before;
    private Instead instead;

    public void setAfter(After after) {
        this.after = after;
    }

    public void setBefore(Before before) {
        this.before = before;
    }

    public void setInstead(Instead instead) {
        this.instead = instead;
    }

    public ProxyClassLoader(String folderName) {
        this.folderName = folderName;
    }

    @Override
    protected Class<?> findClass(String name) throws ClassNotFoundException {
        return null;
    }
}
