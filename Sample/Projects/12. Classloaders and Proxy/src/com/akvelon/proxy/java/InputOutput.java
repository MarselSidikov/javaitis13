package com.akvelon.proxy.java;

/**
 * 27.04.2022
 * 12. Classloaders and Proxy
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public interface InputOutput {
    String input();

    void output(String text);
}
