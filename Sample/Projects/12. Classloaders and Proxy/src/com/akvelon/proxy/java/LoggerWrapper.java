package com.akvelon.proxy.java;

import jdk.internal.util.xml.impl.Input;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.time.LocalDateTime;

/**
 * 27.04.2022
 * 12. Classloaders and Proxy
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public class LoggerWrapper {
    private static class InputOutputInvocationHandler implements InvocationHandler {

        private final InputOutput original;

        public InputOutputInvocationHandler(InputOutput original) {
            this.original = original;
        }

        @Override
        public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
            System.out.println(LocalDateTime.now() + " invoke - " + method.getName() + " "
                    + (args != null ? args[0] : ""));
            return method.invoke(original, args);
        }
    }

    public static InputOutput withLogger(InputOutput original) {
        ClassLoader classLoader = original.getClass().getClassLoader();
        Class<?>[] interfaces = original.getClass().getInterfaces();
        return (InputOutput) Proxy.newProxyInstance(classLoader, interfaces, new InputOutputInvocationHandler(original));
    }
}
