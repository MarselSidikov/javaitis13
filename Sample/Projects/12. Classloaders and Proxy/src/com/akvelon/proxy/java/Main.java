package com.akvelon.proxy.java;

/**
 * 27.04.2022
 * 12. Classloaders and Proxy
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public class Main {
    public static void main(String[] args) {
        InputOutput inputOutput = new FileIO("File.txt");
        InputOutput proxy = LoggerWrapper.withLogger(inputOutput);
        System.out.println(proxy.input());
        proxy.output("Bye bye!");
        System.out.println(proxy.input());
    }
}
