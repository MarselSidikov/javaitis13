package com.akvelon.proxy.plain;

/**
 * 27.04.2022
 * 12. Classloaders and Proxy
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public class HumanProxy extends Human {

    private After after;
    private Before before;

    private final Human original;

    public HumanProxy(Human original) {
        super(original.name);
        this.original = original;
    }

    public void setAfter(After after) {
        this.after = after;
    }

    public void setBefore(Before before) {
        this.before = before;
    }

    @Override
    public void hi() {
        if (before != null) {
            before.execute();
        }

        original.hi();

        if (after != null) {
            after.execute();
        }
    }
}
