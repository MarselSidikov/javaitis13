package com.akvelon.proxy.plain;

/**
 * 27.04.2022
 * 12. Classloaders and Proxy
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public class Human {
    protected String name;

    public Human(String name) {
        this.name = name;
    }

    public void hi() {
        System.out.println("Привет, я - " + name);
    }
}
