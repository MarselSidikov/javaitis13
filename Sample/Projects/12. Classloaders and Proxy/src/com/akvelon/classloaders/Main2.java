package com.akvelon.classloaders;

/**
 * 27.04.2022
 * 12. Classloaders and Proxy
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public class Main2 {
    public static void main(String[] args) {
        try {
            Class.forName("com.akvelon.Users");
        } catch (ClassNotFoundException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
