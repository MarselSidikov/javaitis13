package com.akvelon.classloaders;

import java.io.FileInputStream;
import java.io.InputStream;

/**
 * 27.04.2022
 * 12. Classloaders and Proxy
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public class FolderClassLoader extends ClassLoader {
    private final String folderName;

    public FolderClassLoader(String folderName) {
        this.folderName = folderName;
    }

    @Override
    protected Class<?> findClass(String name) throws ClassNotFoundException {
        try {
            String pathToClass = folderName + "/" + name.replace('.', '/') + ".class";

            try (InputStream classStream = new FileInputStream(pathToClass)){
                byte[] bytes = new byte[classStream.available()];
                int bytesCount = classStream.read(bytes);
                return defineClass(name, bytes, 0, bytesCount);
            }
        } catch (Exception e) {
            throw new IllegalArgumentException(e);
        }
    }
}
