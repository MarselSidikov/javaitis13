package com.akvelon.vk.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 20.06.2022
 * spring-boot-mvc
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
@Controller
@RequestMapping("/users")
public class UsersController {

    @GetMapping
    public String getUsersPage() {
        return "users";
    }
}
