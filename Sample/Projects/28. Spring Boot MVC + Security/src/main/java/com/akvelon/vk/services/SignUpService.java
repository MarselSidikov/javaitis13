package com.akvelon.vk.services;

import com.akvelon.vk.dto.NewUserForm;

/**
 * 16.06.2022
 * spring-boot-mvc
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public interface SignUpService {
    void signUp(NewUserForm user);
}
