package com.akvelon.vk.dto;

import lombok.Data;

/**
 * 16.06.2022
 * spring-boot-mvc
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
@Data
public class NewUserForm {
    private String firstName;
    private String lastName;

    private String email;
    private String password;
}
