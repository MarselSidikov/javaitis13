package com.akvelon.vk.controllers;

import com.akvelon.vk.security.details.UserDetailsImpl;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 16.06.2022
 * spring-boot-mvc
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
@Controller
@RequestMapping("/profile")
public class ProfileController {

    @GetMapping
    public String getProfilePage(@AuthenticationPrincipal UserDetailsImpl userDetails, Model model) {
        model.addAttribute("firstName", userDetails.getUser().getFirstName());
        return "profile";
    }
}
