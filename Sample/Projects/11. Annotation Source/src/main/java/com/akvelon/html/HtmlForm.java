package com.akvelon.html;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 26.04.2022
 * 11. Annotation Source
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
@Retention(RetentionPolicy.SOURCE)
@Target(ElementType.TYPE)
public @interface HtmlForm {
    String method() default "get";
    String action() default "/";
}
