package com.akvelon.forms;

import com.akvelon.html.HtmlForm;

/**
 * 26.04.2022
 * 11. Annotation Source
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
@HtmlForm(action = "https://google.com/search")
public class SearchForm {
}
