package com.akvelon.html;

import com.google.auto.service.AutoService;

import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.Processor;
import javax.annotation.processing.RoundEnvironment;
import javax.annotation.processing.SupportedAnnotationTypes;
import javax.lang.model.element.Element;
import javax.lang.model.element.TypeElement;
import javax.tools.Diagnostic;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Set;

/**
 * 26.04.2022
 * 11. Annotation Source
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
@AutoService(Processor.class)
@SupportedAnnotationTypes(value = "com.akvelon.html.HtmlForm")
public class HtmlFormProcessor extends AbstractProcessor {

    @Override
    public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment environment) {
        Set<? extends Element> annotatedElements = environment.getElementsAnnotatedWith(HtmlForm.class);
        String path = HtmlFormProcessor.class.getProtectionDomain().getCodeSource().getLocation().getPath();
        path = path.replace("%20", " ");

        for (Element element : annotatedElements) {
            String resultFileName = element.getSimpleName().toString() + ".html";

            try (BufferedWriter writer = new BufferedWriter(new FileWriter(path + "\\" + resultFileName))) {
                HtmlForm annotation = element.getAnnotation(HtmlForm.class);
                writer.write("<form action='" + annotation.action() + "' method='" + annotation.method() + "'/>");
            } catch (IOException e) {
                throw new IllegalArgumentException(e);
            }
        }


        return true;
    }
}
