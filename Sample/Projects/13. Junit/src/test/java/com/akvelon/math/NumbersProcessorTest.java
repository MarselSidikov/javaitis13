package com.akvelon.math;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

/**
 * 12.05.2022
 * 13. Junit
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
@DisplayName("NumbersProcessor is working")
@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
@ExtendWith(MockitoExtension.class)
class NumbersProcessorTest {

    private static final List<Boolean> EXPECTED = Arrays
            .asList(false, false, true, false, true);

    private NumbersProcessor processor;

    @Mock
    private NumberToBooleanMapper mapper;

    @BeforeEach
    void setUp() {
        //stubbing
        when(mapper.map(0)).thenThrow(IncorrectNumberException.class);
        when(mapper.map(7)).thenReturn(true);
        when(mapper.map(3)).thenReturn(true);
        when(mapper.map(4)).thenReturn(false);
        when(mapper.map(2)).thenReturn(false);

        processor = new NumbersProcessor(mapper);
    }

    @ParameterizedTest(name = "return correct vector for {0}")
    @MethodSource(value = "numbers")
    public void map_on_correct_numbers(List<Integer> numbers) {
        List<Boolean> actual = processor.toBooleanVector(numbers);
        verify(mapper, times(numbers.size())).checkNegative(anyInt());
        assertEquals(EXPECTED, actual);
    }

    public static Stream<Arguments> numbers() {
        return Stream.of(Arguments.of(Arrays.asList(0, 2, 3, 4, 7)));
    }
}