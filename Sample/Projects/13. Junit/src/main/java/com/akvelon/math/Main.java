package com.akvelon.math;

import java.util.Arrays;
import java.util.List;

/**
 * 28.04.2022
 * 13. Junit
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public class Main {
    public static void main(String[] args) {
        MathOperations operations = new MathOperations();

        NumbersProcessor processor = new NumbersProcessor(operations);

        List<Integer> numbers = Arrays.asList(1, 2, 7, 19, 123, 121, 168);

        System.out.println(processor.toBooleanVector(numbers));
    }
}
