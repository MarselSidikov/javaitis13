package com.akvelon.math;

/**
 * 28.04.2022
 * 13. Junit
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public class IncorrectNumberException extends ArithmeticException {
}
