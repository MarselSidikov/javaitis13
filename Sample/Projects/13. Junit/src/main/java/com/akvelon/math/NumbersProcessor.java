package com.akvelon.math;

import java.util.ArrayList;
import java.util.List;

/**
 * 12.05.2022
 * 13. Junit
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public class NumbersProcessor {

    private final NumberToBooleanMapper mapper;

    public NumbersProcessor(NumberToBooleanMapper mapper) {
        this.mapper = mapper;
    }

    public List<Boolean> toBooleanVector(List<Integer> numbers) {
        List<Boolean> result = new ArrayList<>();

        for (Integer number : numbers) {
            mapper.checkNegative(number);
            try {
                result.add(mapper.map(number));
            } catch (IncorrectNumberException e) {
                result.add(false);
            }
        }

        return result;
    }
}
