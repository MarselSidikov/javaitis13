package com.akvelon.math;

/**
 * 12.05.2022
 * 13. Junit
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public interface NumberToBooleanMapper {
    boolean map(int number);

    void checkNegative(int number) throws IncorrectNumberException;
}
