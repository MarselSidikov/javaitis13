package com.akvelon.problem;

import java.math.BigInteger;

/**
 * 05.04.2022
 * 01. Classes
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public class Main {
    public static void main(String[] args) {
        MarselService marselService = new MarselService();
        marselService.getCourse();
        marselService.getFood();
        marselService.getTaxi();
        marselService.getMoney();
    }
}
