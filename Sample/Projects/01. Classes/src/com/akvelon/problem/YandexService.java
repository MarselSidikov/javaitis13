package com.akvelon.problem;

/**
 * 05.04.2022
 * 01. Classes
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public class YandexService {
    public void getTaxi() {
        System.out.println("Такси");
    }

    public void getFood() {
        System.out.println("Еда");
    }

    public void getMoney() {
        System.out.println("Отдайте деньги!");
    }
}
