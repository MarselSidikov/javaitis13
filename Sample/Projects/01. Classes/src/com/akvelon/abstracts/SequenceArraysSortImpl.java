package com.akvelon.abstracts;

import java.util.Arrays;

/**
 * 05.04.2022
 * 01. Classes
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public class SequenceArraysSortImpl extends Sequence {
    public SequenceArraysSortImpl(int[] elements) {
        super(elements);
    }

    @Override
    protected void sort() {
        Arrays.sort(elements);
    }
}
