package com.akvelon.abstracts;

import java.util.Arrays;

/**
 * 05.04.2022
 * 01. Classes
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public abstract class Sequence {
    protected int[] elements;

    public Sequence(int[] elements) {
        this.elements = Arrays.copyOf(elements, elements.length);
    }

    public boolean exists(int element) {
        sort();
        return binarySearch(elements, 0, elements.length - 1, element) != -1;
    }

    private int binarySearch(int[] array, int left, int right, int element) {
        while (left <= right) {
            int middle = left + (right - left) / 2;
            if (array[middle] == element) {
                return middle;
            }
            if (array[middle] < element) {
                left = middle + 1;
            } else {
                right = middle - 1;
            }
        }

        return -1;
    }

    protected abstract void sort();
}
