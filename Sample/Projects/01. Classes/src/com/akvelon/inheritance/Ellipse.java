package com.akvelon.inheritance;

/**
 * 05.04.2022
 * 01. Classes
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public class Ellipse extends Figure {
    protected int firstRadius;
    protected int secondRadius;

    public Ellipse(int x, int y, int firstRadius, int secondRadius) {
        super(x, y);
        this.firstRadius = firstRadius;
        this.secondRadius = secondRadius;
    }
}
