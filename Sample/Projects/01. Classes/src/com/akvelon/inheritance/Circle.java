package com.akvelon.inheritance;

/**
 * 05.04.2022
 * 01. Classes
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public class Circle extends Ellipse implements CorrectFigure {
    public Circle(int x, int y, int radius) {
        super(x, y, radius, radius);
    }

    @Override
    public int getSide() {
        return firstRadius;
    }
}
