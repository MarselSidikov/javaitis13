package com.akvelon.inheritance;

/**
 * 05.04.2022
 * 01. Classes
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public class Square extends Rectangle implements CorrectFigure {
    public Square(int x, int y, int width) {
        super(x, y, width, width);
    }

    @Override
    public int getSide() {
        return width;
    }
}
