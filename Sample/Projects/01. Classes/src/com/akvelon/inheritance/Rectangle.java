package com.akvelon.inheritance;

/**
 * 05.04.2022
 * 01. Classes
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public class Rectangle extends Figure {

    protected int width;
    protected int length;

    public Rectangle(int x, int y, int width, int length) {
        super(x, y);
        this.width = width;
        this.length = length;
    }
}
