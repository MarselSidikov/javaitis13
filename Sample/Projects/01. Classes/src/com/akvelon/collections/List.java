package com.akvelon.collections;

/**
 * 05.04.2022
 * 01. Classes
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public interface List {
    void add(int element);

    Iterator iterator();
}
