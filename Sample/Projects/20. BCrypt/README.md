# Хеширование паролей

* Если хранить пароли в открытом виде, то при взломе бд злоумышленник получает пары `email-пароль`.
* Поэтому, мы не храним пароли в чистом виде. НИКОГДА.

## Алгоритмы хеширования паролей

* Задача - получить хеш от пароля, сохранить его, а затем проверять, может ли быть получен этот хеш из того, что ввел пользователь.

### BCrypt

* Особенность - если один и тот же пароль хешировать, мы все время будем получать разные хеши.
* Следовательно, зная хеш, невозможно понять, какой был исходный пароль.

#### Структура и принцип работы

`$[2a]$[10]$[zcPYne99T3F4t3XyOcaeN.][fGbINW4Ps/R1fRu59l/tJyZhe.VBuNe]`

* `zcPYne99T3F4t3XyOcaeN.` - salt
* `fGbINW4Ps/R1fRu59l/tJyZhe.VBuNe` - hash

##### Алгоритм формирования хеша

```
1) подаем алгоритму пароль - qwerty007
2) генерируется случайна последовательность символов (salt) - zcPYne99T3F4t3XyOcaeN
3) создается хеш на основе исходного пароля и соли - hash(salt, qwerty007) -> fGbINW4Ps/R1fRu59l/tJyZhe.VBuNe
4) формируется результирующая строка $2a$10$[salt][hash]
5) Ее и нужно сохранить в бд
```

#### Алгоритм проверки пароля

```
1) Имеем хеш - $2a$10$WLwNELBTMVQA32SCsGHRHehDKsrlHcEWyvwM8XjgiVbmruBzWELuO
2) На вход подали пароль - qwerty008
3) Из хеша выделяется salt - WLwNELBTMVQA32SCsGHRHeh
4) Хешируем новый пароль со старой солью - hash(salt, qwerty008) - получили хеш
5) Сравниваем полученный с тем, что было в базе - hDKsrlHcEWyvwM8XjgiVbmruBzWELuO, не совпало, пароль плохой.
```