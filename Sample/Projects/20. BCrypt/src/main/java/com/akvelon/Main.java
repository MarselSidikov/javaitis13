package com.akvelon;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

/**
 * 25.05.2022
 * 20. BCrypt
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public class Main {
    public static void main(String[] args) {
        PasswordEncoder encoder = new BCryptPasswordEncoder();
        String hashPassword = encoder.encode("qwerty007");
        String hashPassword1 = encoder.encode("qwerty007");
        String hashPassword2 = encoder.encode("qwerty007");
        String hashPassword3 = encoder.encode("qwerty007");
        System.out.println(hashPassword);
        System.out.println(hashPassword1);
        System.out.println(hashPassword2);
        System.out.println(hashPassword3);

        System.out.println(encoder.matches("qwerty008", "$2a$10$Kv5t3OcEMYtiHE5biQYz0.ksLyx9JIgGsiAIJiXP0j02.kPnjKtsC"));
    }
}
