# Серверное приложение на Java

![image](img/1.png)

* Java Servlet API - стандарт обработки HTTP-запросов. Зависимость `javax.servlet-api`.
* Сервлет - компонент приложения, который непосредственно обрабатывает HTTP-запросы.

* Жизненный цикл сервлета:
  - `public void init() throws ServletException`
  - `protected void service(HttpServletRequest request, HttpServletResponse response)`
    - `doGet`
    - `doPost
    - `doPut`
  - `public void destroy()`

* `web.xml` - дескриптор развертывания (какие сервлеты и как будут запускаться)

* Контейнер сервлетов (не путать с веб-сервером) - `Tomcat`, серверное приложение в котором можно развернуть (задеплоить) сервлеты и они будут готовы обрабатывать запросы.

* Сессия - это некоторый объект, который хранится на сервере (в нашем случае - в оперативной памяти Tomcat). У сессии есть атрибуты, сама сессия - Java-объект.
* Сессия может объединять набор HTTP-запросов. Вы можете сказать, что определенный запрос принадлежит сессии, или нет.
* Есть информация, которую мы не хотим хранить в куках - ее либо слишком много, либо это небезопасно.
* Такую информацию нужно хранить в сессиях. Потому что у клиента нет доступа к самой сессии.
* Сама сессия хранится на сервере, но ее идентификатор хранится в браузере в куке JSESSIONID.

## Доизучить

* Сессии
* Редиректы и перенаправления
* Логирование