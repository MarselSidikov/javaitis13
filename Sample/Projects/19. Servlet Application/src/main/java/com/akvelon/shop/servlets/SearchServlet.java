package com.akvelon.shop.servlets;

import com.akvelon.shop.dto.UserDto;
import com.akvelon.shop.services.SearchService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.context.ApplicationContext;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

/**
 * 23.05.2022
 * 19. Servlet Application
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
@WebServlet(loadOnStartup = 1, value = "/search")
public class SearchServlet extends HttpServlet {

    private SearchService searchService;

    private ObjectMapper objectMapper;

    @Override
    public void init(ServletConfig config) throws ServletException {
       ServletContext servletContext = config.getServletContext();
       this.searchService = ((ApplicationContext)(servletContext.getAttribute("springContext")))
               .getBean(SearchService.class);
       this.objectMapper = ((ApplicationContext)(servletContext.getAttribute("springContext")))
               .getBean(ObjectMapper.class);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String email = request.getParameter("email");
        List<UserDto> users = searchService.searchByEmail(email);

        response.setContentType("application/json");

        PrintWriter writer = response.getWriter();

        writer.println(objectMapper.writeValueAsString(users));
    }

}
