package com.akvelon.shop.servlets;

import com.akvelon.shop.dto.UserDto;
import com.akvelon.shop.services.SearchService;
import com.akvelon.shop.services.UsersService;
import org.springframework.context.ApplicationContext;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

/**
 * 23.05.2022
 * 19. Servlet Application
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
@WebServlet(loadOnStartup = 1, value = "/users")
public class UsersServlet extends HttpServlet {

    private UsersService usersService;

    @Override
    public void init(ServletConfig config) throws ServletException {
       ServletContext servletContext = config.getServletContext();
       this.usersService = ((ApplicationContext)(servletContext.getAttribute("springContext")))
               .getBean(UsersService.class);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {

        String color = "black";

        if (request.getAttribute("color") != null) {
            color = (String) request.getAttribute("color");
        }

        Integer pageNumber = Integer.parseInt(request.getParameter("page"));

        List<UserDto> users = usersService.getAll(pageNumber);

        response.setContentType("text/html");

        PrintWriter writer = response.getWriter();

        writer.println("<hr style='height:4px;background-color:" + color + "'>");

        writer.println("<table>");
        writer.println("    <tr>");
        writer.println("        <th>" + "ID" + " </th>");
        writer.println("        <th>" + "Email" + " </th>");
        writer.println("        <th>" + "Password" + " </th>");
        writer.println("    </tr>");

        for (UserDto user : users) {
            writer.println("    <tr>");
            writer.println("        <td>" + user.getId() + " </td>");
            writer.println("        <td>" + user.getEmail() + " </td>");
            writer.println("    </tr>");
        }
        writer.println("</table>");
    }

}
