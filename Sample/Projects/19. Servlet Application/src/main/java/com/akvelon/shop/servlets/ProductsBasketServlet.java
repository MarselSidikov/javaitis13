package com.akvelon.shop.servlets;

import com.akvelon.shop.dto.BasketDto;
import com.akvelon.shop.services.BasketService;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.UUID;

/**
 * 25.05.2022
 * 19. Servlet Application
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
@WebServlet("/basket")
@MultipartConfig
public class ProductsBasketServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();

        request.setAttribute("products", Arrays.asList("Milk", "Bread", "Water", "Egg", "Hen"));

        if (session.isNew()) {
            session.setAttribute("basket",
                    BasketDto
                            .builder()
                            .basketId(UUID.randomUUID().toString())
                            .products(new ArrayList<>())
                            .build());
        }

        BasketDto basketDto = (BasketDto) session.getAttribute("basket");

        request.setAttribute("basket", basketDto.getProducts());

        request.getRequestDispatcher("/jsp/basket.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String product = new BufferedReader(new InputStreamReader(request.getPart("title")
                .getInputStream()))
                .readLine();

        HttpSession session = request.getSession(false);

        BasketDto basketDto = (BasketDto) session.getAttribute("basket");

        basketDto.getProducts().add(product);
    }
}
