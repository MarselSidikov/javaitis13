package com.akvelon.shop.dto;

import lombok.*;

import java.io.InputStream;

/**
 * 24.05.2022
 * 19. Servlet Application
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class FileDto {
    private String fileName;
    private Integer size;
    private String mimeType;
    private String description;
    private InputStream fileStream;
}
