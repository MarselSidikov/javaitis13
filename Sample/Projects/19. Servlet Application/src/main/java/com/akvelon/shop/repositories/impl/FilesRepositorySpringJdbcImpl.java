package com.akvelon.shop.repositories.impl;

import com.akvelon.shop.models.FileInfo;
import com.akvelon.shop.repositories.FilesRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

/**
 * 24.05.2022
 * 19. Servlet Application
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
@RequiredArgsConstructor
@Repository
public class FilesRepositorySpringJdbcImpl implements FilesRepository {

    public static final String FILES_TABLE_NAME = "file";

    //language=SQL
    private static final String SQL_SELECT_BY_STORAGE_FILENAME = "select * from " + FILES_TABLE_NAME + " where storage_file_name = :storageFileName";

    private final NamedParameterJdbcTemplate jdbcTemplate;

    private final SimpleJdbcInsert jdbcInsert;

    private static final RowMapper<FileInfo> fileRowMapper = (row, rowNumber) -> FileInfo.builder()
            .size(row.getLong("size"))
            .originalFileName(row.getString("original_file_name"))
            .storageFileName(row.getString("storage_file_name"))
            .description(row.getString("description"))
            .mimeType(row.getString("mime_type"))
            .build();

    @Override
    public void save(FileInfo entity) {
        Map<String, Object> params = new HashMap<>();

        params.put("original_file_name", entity.getOriginalFileName());
        params.put("storage_file_name", entity.getStorageFileName());
        params.put("size", entity.getSize());
        params.put("mime_type", entity.getMimeType());
        params.put("description", entity.getDescription());


        entity.setId(jdbcInsert
                .withTableName(FILES_TABLE_NAME)
                .usingGeneratedKeyColumns("id")
                .executeAndReturnKey(params).longValue());
    }

    @Override
    public Optional<FileInfo> findByStorageFileName(String storageFileName) {
        SqlParameterSource sqlParameterSource = new MapSqlParameterSource("storageFileName", storageFileName);
        try {
            return Optional.ofNullable(jdbcTemplate.queryForObject(
                    SQL_SELECT_BY_STORAGE_FILENAME, sqlParameterSource, fileRowMapper));
        } catch (EmptyResultDataAccessException e) {
            return Optional.empty();
        }
    }
}
