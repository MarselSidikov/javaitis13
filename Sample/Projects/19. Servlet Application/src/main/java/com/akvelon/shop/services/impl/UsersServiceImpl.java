package com.akvelon.shop.services.impl;

import com.akvelon.shop.dto.UserDto;
import com.akvelon.shop.repositories.UsersRepository;
import com.akvelon.shop.services.UsersService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.akvelon.shop.dto.UserDto.from;

/**
 * 23.05.2022
 * 19. Servlet Application
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
@Service
public class UsersServiceImpl implements UsersService {

    private final UsersRepository usersRepository;

    private final int pageSize;

    public UsersServiceImpl(UsersRepository usersRepository, @Value("${web.pages.size}") int pageSize) {
        this.usersRepository = usersRepository;
        this.pageSize = pageSize;
    }

    @Override
    public List<UserDto> getAll(Integer pageNumber) {
        return from(usersRepository.findAll(pageNumber, pageSize));
    }
}
