package com.akvelon.shop.servlets;

import com.akvelon.shop.dto.ContractDto;
import com.fasterxml.jackson.databind.ObjectMapper;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * 24.05.2022
 * 19. Servlet Application
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
@WebServlet("/addContract")
public class ContractsServlet extends HttpServlet {

    // TODO: просто для примера!
    private static List<ContractDto> contracts = new ArrayList<>();
    // TODO: просто для примера!
    private ObjectMapper objectMapper = new ObjectMapper();

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String body = new BufferedReader(new InputStreamReader(request.getInputStream())).readLine();
        ContractDto contract = objectMapper.readValue(body, ContractDto.class);
        contracts.add(contract);

        response.getWriter().write(objectMapper.writeValueAsString(contracts));
    }
}
