package com.akvelon.shop.services;

import com.akvelon.shop.dto.FileDto;

import java.io.OutputStream;

/**
 * 24.05.2022
 * 19. Servlet Application
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public interface FilesService {
    void upload(FileDto file);

    void writeFileToStream(String storageFileName, OutputStream outputStream);

    FileDto getFileInformation(String storageFileName);
}
