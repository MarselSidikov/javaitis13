package com.akvelon.shop.servlets;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 24.05.2022
 * 19. Servlet Application
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
@WebServlet("/changeColor")
public class ChangeColorServlet extends HttpServlet {

    public static final String COLOR_COOKIE_NAME = "color";

    public static final String COLOR_PARAMETER_NAME = "color";

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String color = request.getParameter(COLOR_PARAMETER_NAME);

        Cookie colorCooke = new Cookie(COLOR_COOKIE_NAME, color);
        response.addCookie(colorCooke);
    }
}
