package com.akvelon.shop.config.servlets.strategies;

import com.akvelon.shop.config.ApplicationConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import javax.servlet.ServletContext;

/**
 * 23.05.2022
 * 19. Servlet Application
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public class SpringAndDataSourceInitializeStrategyImpl extends AbstractServletContextListenerStrategy implements ServletContextListenerStrategy {


    public SpringAndDataSourceInitializeStrategyImpl(ServletContext servletContext) {
        super(servletContext);
    }

    @Override
    public void execute() {
        try {
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException e) {
            throw new IllegalStateException(e);
        }

        ApplicationContext springContext = new AnnotationConfigApplicationContext(ApplicationConfig.class);
        servletContext.setAttribute("springContext", springContext);
        servletContext.setAttribute("dataSource", springContext.getBean(HikariDataSource.class));
    }
}
