package com.akvelon.shop.servlets;

import com.akvelon.shop.dto.AddProductDto;
import com.akvelon.shop.dto.UserDto;
import com.akvelon.shop.services.ProductsService;
import com.akvelon.shop.services.SearchService;
import org.springframework.context.ApplicationContext;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

/**
 * 23.05.2022
 * 19. Servlet Application
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
@WebServlet(loadOnStartup = 1, value = "/addProduct")
public class ProductsServlet extends HttpServlet {

    private ProductsService productsService;

    @Override
    public void init(ServletConfig config) throws ServletException {
       ServletContext servletContext = config.getServletContext();
       this.productsService = ((ApplicationContext)(servletContext.getAttribute("springContext")))
               .getBean(ProductsService.class);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        AddProductDto product = AddProductDto.builder()
                .title(request.getParameter("title"))
                .price(Double.parseDouble(request.getParameter("price")))
                .build();

        productsService.addProduct(product);

        response.sendRedirect("/html/addProduct.html");

    }

}
