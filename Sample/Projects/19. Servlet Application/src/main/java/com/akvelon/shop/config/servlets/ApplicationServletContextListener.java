package com.akvelon.shop.config.servlets;

import com.akvelon.shop.config.servlets.strategies.HikariDataSourceCloseConnectionsStrategy;
import com.akvelon.shop.config.servlets.strategies.ServletContextListenerStrategy;
import com.akvelon.shop.config.servlets.strategies.SpringAndDataSourceInitializeStrategyImpl;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

/**
 * 23.05.2022
 * 19. Servlet Application
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
@WebListener
public class ApplicationServletContextListener implements ServletContextListener {

    private ServletContextListenerStrategy initializeStrategy;

    private ServletContextListenerStrategy destroyStrategy;


    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {

        ServletContext servletContext = servletContextEvent.getServletContext();

        initializeStrategy = new SpringAndDataSourceInitializeStrategyImpl(servletContext);

        initializeStrategy.execute();
    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {
        ServletContext servletContext = servletContextEvent.getServletContext();

        destroyStrategy = new HikariDataSourceCloseConnectionsStrategy(servletContext);

        destroyStrategy.execute();
    }
}
