package com.akvelon.shop.config.servlets.strategies;

/**
 * 23.05.2022
 * 19. Servlet Application
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public interface ServletContextListenerStrategy {
    void execute();
}
