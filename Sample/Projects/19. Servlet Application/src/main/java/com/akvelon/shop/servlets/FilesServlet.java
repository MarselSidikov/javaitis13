package com.akvelon.shop.servlets;

import com.akvelon.shop.dto.FileDto;
import com.akvelon.shop.services.FilesService;
import com.akvelon.shop.services.ProductsService;
import org.springframework.context.ApplicationContext;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.*;
import java.util.Collection;

/**
 * 24.05.2022
 * 19. Servlet Application
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
// files?fileName=b9906363-351c-47ac-8d2d-9d10e2b04f7e.pdf
@WebServlet("/files/*")
@MultipartConfig
public class FilesServlet extends HttpServlet {

    private static final String UPLOAD_URL = "files/upload";

    private FilesService filesService;

    @Override
    public void init(ServletConfig config) throws ServletException {
        ServletContext servletContext = config.getServletContext();
        this.filesService = ((ApplicationContext)(servletContext.getAttribute("springContext")))
                .getBean(FilesService.class);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if (request.getRequestURI().endsWith(UPLOAD_URL)) {
            Part descriptionPart = request.getPart("description");
            String description = (new BufferedReader(new InputStreamReader(descriptionPart.getInputStream()))).readLine();

            Part filePart = request.getPart("file");

            FileDto file = FileDto.builder()
                    .fileName(filePart.getSubmittedFileName())
                    .description(description)
                    .mimeType(filePart.getContentType())
                    .size((int)filePart.getSize())
                    .fileStream(filePart.getInputStream())
                    .build();

            filesService.upload(file);
        } else {
            response.sendError(HttpServletResponse.SC_BAD_REQUEST);
        }
    }

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException     {
        String storageFileName = request.getParameter("fileName");

        FileDto file = filesService.getFileInformation(storageFileName);
        response.setContentType(file.getMimeType());
        response.setContentLength(file.getSize());
        response.setHeader("Content-Disposition", "filename=\"" + file.getFileName() + "\"");

        filesService.writeFileToStream(storageFileName, response.getOutputStream());
        response.flushBuffer();

    }
}
