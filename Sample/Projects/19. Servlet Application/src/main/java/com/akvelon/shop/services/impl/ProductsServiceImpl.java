package com.akvelon.shop.services.impl;

import com.akvelon.shop.dto.AddProductDto;
import com.akvelon.shop.services.ProductsService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

/**
 * 23.05.2022
 * 19. Servlet Application
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
@RequiredArgsConstructor
@Service
public class ProductsServiceImpl implements ProductsService {

    @Override
    public void addProduct(AddProductDto product) {
        System.out.println(product);    
    }
}
