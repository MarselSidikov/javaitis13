package com.akvelon.shop.config.servlets.strategies;

import com.zaxxer.hikari.HikariDataSource;

import javax.servlet.ServletContext;

/**
 * 23.05.2022
 * 19. Servlet Application
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public class HikariDataSourceCloseConnectionsStrategy extends AbstractServletContextListenerStrategy implements ServletContextListenerStrategy {

    public HikariDataSourceCloseConnectionsStrategy(ServletContext servletContext) {
        super(servletContext);
    }

    @Override
    public void execute() {
        HikariDataSource dataSource = (HikariDataSource) servletContext.getAttribute("dataSource");
        dataSource.close();
    }
}
