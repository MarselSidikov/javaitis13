package com.akvelon.shop.services;

import com.akvelon.shop.dto.AddProductDto;

/**
 * 23.05.2022
 * 19. Servlet Application
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public interface ProductsService {
    void addProduct(AddProductDto product);
}
