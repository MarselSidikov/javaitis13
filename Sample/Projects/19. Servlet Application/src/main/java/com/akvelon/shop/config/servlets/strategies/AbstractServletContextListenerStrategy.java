package com.akvelon.shop.config.servlets.strategies;

import javax.servlet.ServletContext;

/**
 * 23.05.2022
 * 19. Servlet Application
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public abstract class AbstractServletContextListenerStrategy implements ServletContextListenerStrategy {

    protected ServletContext servletContext;

    public AbstractServletContextListenerStrategy(ServletContext servletContext) {
        this.servletContext = servletContext;
    }
}
