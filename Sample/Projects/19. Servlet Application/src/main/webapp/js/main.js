function searchUsers(email) {
    fetch('/search?email=' + email)
        .then((response) => {
            return response.json();
        })
        .then((users) => {
            let table = document.getElementById('table_with_users');

            table.innerHTML = '<tr> ' +
                '            <th>ID</th> ' +
                '            <th>Email</th> ' +
                '        </tr>';

            for (let i = 0; i < users.length; i++) {
                let row = table.insertRow(-1);
                let idCell = row.insertCell(0);
                let emailCell = row.insertCell(1);

                idCell.innerHTML = users[i].id;
                emailCell.innerHTML = users[i].email;
            }
        });
}

function sendContract(title, price) {
    let json = {
        "title": title,
        "price": price
    };

    fetch('/addContract', {
        method: 'POST',
        body: JSON.stringify(json),
        headers: {
            'Content-Type': 'application/json'
        }
    }).then((response) => {
        return response.json();
    }).then((contracts) => {
            let table = document.getElementById('table_with_contracts');

            table.innerHTML = '<tr> ' +
                '            <th>Title</th> ' +
                '            <th>Price</th> ' +
                '        </tr>';

            for (let i = 0; i < contracts.length; i++) {
                let row = table.insertRow(-1);
                let titleCell = row.insertCell(0);
                let priceCell = row.insertCell(1);

                titleCell.innerHTML = contracts[i].title;
                priceCell.innerHTML = contracts[i].price;
            }
        });
}