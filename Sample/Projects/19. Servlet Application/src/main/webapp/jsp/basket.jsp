<%@ page import="java.util.List" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Products</title>
</head>
<script>
    function sendProduct(product) {
        let formData = new FormData();
        formData.append('title', product);
        fetch('/basket', {
            method: 'POST',
            body: formData
        }).then(() => {
            location.reload();
        });
    }
</script>
<body>
<%
    List<String> products = (List<String>) request.getAttribute("products");
    for (String product : products) {
%>
<button onclick="sendProduct('<%=product%>')"><%=product%>
</button>
<br>
<%
    }
%>
<hr>
<%
    List<String> basket = (List<String>) request.getAttribute("basket");
    for (String product : basket) {
%>
<%=product%><br>
<%}%>
</body>
</html>
