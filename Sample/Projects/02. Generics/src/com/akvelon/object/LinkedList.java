package com.akvelon.object;

/**
 * 05.04.2022
 * 01. Classes
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public class LinkedList implements List {

    private Node first;
    private Node last;

    private int size;

    LinkedList() {
        this.first = null;
        this.size = 0;
    }

    @Override
    public void add(Object element) {
        Node newNode = new Node(element);

        if (size == 0) {
            this.first = newNode;
        } else {
            this.last.next = newNode;
        }
        this.last = newNode;
        size++;

    }

    @Override
    public Object get(int index) {
        if (index >= 0 && index < size) {
            Node current = first;
            for (int i = 0; i < index; i++) {
                current = current.next;
            }

            return current.value;
        } throw new IndexOutOfBoundsException();
    }

    @Override
    public Iterator iterator() {
        return new LinkedListIterator();
    }

    private class LinkedListIterator implements Iterator {
        private Node current;

        public LinkedListIterator() {
            this.current = first;
        }

        @Override
        public boolean hasNext() {
            return current != null;
        }

        @Override
        public Object next() {
            Object forReturnValue = current.value;
            current = current.next;
            return forReturnValue;
        }
    }

    private static class Node {
        private final Object value;

        private Node next;

        Node(Object value) {
            this.value = value;
        }
    }
}
