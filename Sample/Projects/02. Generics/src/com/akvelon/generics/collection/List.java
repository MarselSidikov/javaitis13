package com.akvelon.generics.collection;

/**
 * 05.04.2022
 * 01. Classes
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public interface List<E> {
    void add(E element);

    Object get(int index);

    Iterator<E> iterator();
}
