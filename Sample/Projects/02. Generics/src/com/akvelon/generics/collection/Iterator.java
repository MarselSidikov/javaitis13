package com.akvelon.generics.collection;

/**
 * 05.04.2022
 * 01. Classes
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public interface Iterator<T> {

    boolean hasNext();

    T next();
}
