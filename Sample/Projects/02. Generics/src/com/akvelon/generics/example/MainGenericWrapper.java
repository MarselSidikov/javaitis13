package com.akvelon.generics.example;

/**
 * 06.04.2022
 * 02. Generics
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public class MainGenericWrapper {

    public static <T> void swap(GenericWrapper<T> a, GenericWrapper<T> b) {
        T temp = a.getValue();
        a.setValue(b.getValue());
        b.setValue(temp);
    }

    public static void main(String[] args) {
        GenericWrapper<Integer> x = new GenericWrapper<>(10);
        GenericWrapper<String> d = new GenericWrapper<>("Hello");
        GenericWrapper<Integer> y = new GenericWrapper<>(15);

        swap(x, y);

        System.out.println(x.getValue());
        System.out.println(y.getValue());
    }
}
