# JPA

* Java Persistence API - стандарт ORM в Java. 
* Представлено в виде набора аннотаций и интерфейсов.
* Hibernate - одна из реализаций JPA.
* Persistence Context - коллекция MANAGED-сущностей

## Осталось

* Модель памяти Hibernate
* Разница между методами
* `subselect`
* Состояния сущностей