package com.akvelon.jpa.app;

import com.akvelon.jpa.models.Course;
import com.akvelon.jpa.repositories.CoursesRepositoryJpaImpl;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import javax.persistence.EntityManager;

/**
 * 06.06.2022
 * 24. JPA
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public class Main {
    public static void main(String[] args) {
        Configuration configuration = new Configuration();
        configuration.configure("hibernate.cfg.xml");
        SessionFactory sessionFactory = configuration.buildSessionFactory();

        EntityManager entityManager = sessionFactory.createEntityManager();
        CoursesRepositoryJpaImpl coursesRepository = new CoursesRepositoryJpaImpl(entityManager);

        System.out.println(coursesRepository.findAllByLesson_Name("tables"));

    }
}
