package com.akvelon.jpa.repositories;

import com.akvelon.jpa.models.Course;
import lombok.RequiredArgsConstructor;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.TypedQuery;
import java.util.List;

/**
 * 06.06.2022
 * 24. JPA
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
@RequiredArgsConstructor
public class CoursesRepositoryJpaImpl {

    private final EntityManager entityManager;

    public void save(Course course) {
        EntityTransaction transaction = entityManager.getTransaction();

        transaction.begin();
        entityManager.persist(course);
        transaction.commit();
    }

    public List<Course> findAllByLesson_Name(String lessonName) {
        TypedQuery<Course> query = entityManager
                .createQuery("select course from Course course " +
                        "left join course.lessons lesson " +
                        "where lesson.name = :name", Course.class);

        query.setParameter("name", lessonName);
        return query.getResultList();
    }
}
