package com.akvelon.console.config;

import java.util.Properties;
import java.io.*;

public class Config {

	private static final String PROPERTIES_FILE = "resources\\application.properties";

	private Properties properties;


	public Config() {
		properties = new Properties();

		try {
			FileReader reader = new FileReader(PROPERTIES_FILE);
			properties.load(reader);		
		} catch (IOException e) {
			throw new IllegalArgumentException(e);
		}

	}

	public String getProgramTitle() {
		return properties.getProperty("program.title");
	}
}