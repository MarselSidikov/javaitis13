package com.akvelon.console.app;

import com.beust.jcommander.JCommander;
import com.akvelon.console.config.Config;

public class Program {
	public static void main(String[] argv) {
		Config config = new Config();

		Args args = new Args();

		JCommander.newBuilder()
  			.addObject(args)
  			.build()
  			.parse(argv);

		System.out.println(config.getProgramTitle() + args.getNumbers() + " " + args.getIsLowerCase());
	}
}