package com.akvelon.console.app;

import com.beust.jcommander.Parameter;
import java.util.List;
import java.util.ArrayList;

class Args {
	@Parameter(names = "--numbers")
	private List<Integer> numbers = new ArrayList<>();

	@Parameter(names = "--lower-case")
	private Boolean isLowerCase = false;

	public List<Integer> getNumbers() {
		return numbers;
	}

	public Boolean getIsLowerCase() {
		return isLowerCase;
	}

}