package com.akvelon.shop;

import com.akvelon.shop.services.UsersService;
import com.akvelon.shop.services.UsersServiceImpl;
import com.akvelon.shop.validators.PasswordValidator;
import com.akvelon.shop.validators.PasswordValidatorByLengthImpl;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * 18.05.2022
 * 15. Spring
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public class Main {
    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("C:\\Users\\Marsel\\Desktop\\Education\\akvelon_java_internship_1\\Projects\\15. Spring\\context.xml");

        UsersService usersService = context.getBean(UsersService.class);

        usersService.signUp("sidikov.marsel@gmail.com", "qwer!");
    }
}
