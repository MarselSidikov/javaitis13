package com.akvelon.shop.validators;

import com.akvelon.shop.exceptions.PasswordValidationException;

/**
 * 18.05.2022
 * 15. Spring
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public class PasswordValidatorByLengthImpl implements PasswordValidator {

    private final int minLength;

    public PasswordValidatorByLengthImpl(int minLength) {
        this.minLength = minLength;
    }

    @Override
    public void validate(String password) throws PasswordValidationException {
        if (password.length() < minLength) {
            throw new PasswordValidationException("Short password");
        }
    }
}
