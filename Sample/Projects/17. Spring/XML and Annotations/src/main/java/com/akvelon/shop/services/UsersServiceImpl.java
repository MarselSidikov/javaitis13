package com.akvelon.shop.services;

import com.akvelon.shop.validators.PasswordValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

/**
 * 18.05.2022
 * 15. Spring
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
@Component(value = "userService")
public class UsersServiceImpl implements UsersService {

    private final PasswordValidator passwordValidator;

    public UsersServiceImpl(@Qualifier("passwordValidatorByCharacters") PasswordValidator passwordValidator) {
        this.passwordValidator = passwordValidator;
    }

    @Override
    public void signUp(String email, String password) {
        passwordValidator.validate(password);
        System.out.println("Все ок, пользователь сохранен");
    }
}
