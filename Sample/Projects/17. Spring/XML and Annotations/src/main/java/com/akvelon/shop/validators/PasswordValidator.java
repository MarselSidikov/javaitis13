package com.akvelon.shop.validators;

import com.akvelon.shop.exceptions.PasswordValidationException;

/**
 * 18.05.2022
 * 15. Spring
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public interface PasswordValidator {
    void validate(String password) throws PasswordValidationException;
}
