package com.akvelon.shop.validators;

import com.akvelon.shop.exceptions.PasswordValidationException;

import java.util.ArrayList;
import java.util.List;

/**
 * 18.05.2022
 * 15. Spring
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public class PasswordValidatorCharactersValidationImpl implements PasswordValidator {

    private String specialCharacters;

    @Override
    public void validate(String password) throws PasswordValidationException {
        int count = 0;
        for (char character : specialCharacters.toCharArray()) {
            if (password.indexOf(character) != -1) {
                count++;
            }
        }

        if (count == 0) {
            throw new PasswordValidationException("Missing special characters");
        }
    }

    public void setSpecialCharacters(String specialCharacters) {
        this.specialCharacters = specialCharacters;
    }
}
