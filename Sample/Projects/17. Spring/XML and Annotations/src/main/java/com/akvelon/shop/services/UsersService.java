package com.akvelon.shop.services;

/**
 * 18.05.2022
 * 15. Spring
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public interface UsersService {
    void signUp(String email, String password);
}
