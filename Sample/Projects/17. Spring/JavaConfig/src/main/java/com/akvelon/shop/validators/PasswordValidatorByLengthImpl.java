package com.akvelon.shop.validators;

import com.akvelon.shop.exceptions.PasswordValidationException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

/**
 * 18.05.2022
 * 15. Spring
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
@Profile("develop")
@Component(value = "passwordValidatorByLength")
public class PasswordValidatorByLengthImpl implements PasswordValidator {

    private final int minLength;

    public PasswordValidatorByLengthImpl(@Value("${password.validator.byLength.minLength}") int minLength) {
        this.minLength = minLength;
    }

    @Override
    public void validate(String password) throws PasswordValidationException {
        if (password.length() < minLength) {
            throw new PasswordValidationException("Short password");
        }
    }
}
