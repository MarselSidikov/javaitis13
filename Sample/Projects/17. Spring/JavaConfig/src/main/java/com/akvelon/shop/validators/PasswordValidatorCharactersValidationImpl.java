package com.akvelon.shop.validators;

import com.akvelon.shop.exceptions.PasswordValidationException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * 18.05.2022
 * 15. Spring
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
@Profile("production")
@Component(value = "passwordValidatorByCharacters")
public class PasswordValidatorCharactersValidationImpl implements PasswordValidator {

    @Value("${password.validator.byCharacters.specialCharacters}")
    private String specialCharacters;

    @Override
    public void validate(String password) throws PasswordValidationException {
        int count = 0;
        for (char character : specialCharacters.toCharArray()) {
            if (password.indexOf(character) != -1) {
                count++;
            }
        }

        if (count == 0) {
            throw new PasswordValidationException("Missing special characters");
        }
    }

}
