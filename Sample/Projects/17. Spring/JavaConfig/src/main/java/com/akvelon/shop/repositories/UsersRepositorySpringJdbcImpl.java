package com.akvelon.shop.repositories;

import com.akvelon.shop.models.User;
import lombok.RequiredArgsConstructor;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.Map;

/**
 * 19.05.2022
 * JavaConfig
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
@RequiredArgsConstructor
@Repository
public class UsersRepositorySpringJdbcImpl implements UsersRepository {

    public static final String USERS_TABLE_NAME = "account";

    private final NamedParameterJdbcTemplate jdbcTemplate;

    private final SimpleJdbcInsert jdbcInsert;

    @Override
    public void save(User entity) {
        Map<String, Object> params = new HashMap<>();

        params.put("email", entity.getEmail());
        params.put("password", entity.getPassword());

        entity.setId(jdbcInsert
                .withTableName(USERS_TABLE_NAME)
                        .usingGeneratedKeyColumns("id")
                .executeAndReturnKey(params).longValue());
    }
}
