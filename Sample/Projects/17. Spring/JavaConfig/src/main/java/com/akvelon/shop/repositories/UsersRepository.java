package com.akvelon.shop.repositories;

import com.akvelon.shop.models.User;

/**
 * 19.05.2022
 * JavaConfig
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public interface UsersRepository extends CrudRepository<User, Long> {
}
