package com.akvelon.shop;

import com.akvelon.shop.config.ApplicationConfig;
import com.akvelon.shop.services.UsersService;
import com.akvelon.shop.services.UsersServiceImpl;
import com.akvelon.shop.validators.PasswordValidator;
import com.akvelon.shop.validators.PasswordValidatorByLengthImpl;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * 18.05.2022
 * 15. Spring
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public class Main {
    public static void main(String[] args) {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext();

        context.getEnvironment().setActiveProfiles(args[0]);
        context.register(ApplicationConfig.class);
        context.refresh();

        UsersService usersService = context.getBean(UsersService.class);

        usersService.signUp("sidikov.marsel@gmail.com", "qwe!");
    }
}
