package com.akvelon.shop.services;

import com.akvelon.shop.models.User;
import com.akvelon.shop.repositories.UsersRepository;
import com.akvelon.shop.validators.PasswordValidator;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

/**
 * 18.05.2022
 * 15. Spring
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
@Service(value = "userService")
@RequiredArgsConstructor
public class UsersServiceImpl implements UsersService {

    private final PasswordValidator passwordValidator;

    private final UsersRepository usersRepository;

    @Override
    public void signUp(String email, String password) {
        passwordValidator.validate(password);

        User user = User.builder()
                .email(email)
                .password(password)
                .build();

        usersRepository.save(user);
        System.out.println(user);
    }
}
