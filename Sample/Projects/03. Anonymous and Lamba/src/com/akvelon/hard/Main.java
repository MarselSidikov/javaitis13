package com.akvelon.hard;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Objects;
import java.util.function.ToIntFunction;

/**
 * 08.04.2022
 * 03. Anonymous and Lamba
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public class Main {
    public static <T> void sort(T[] array, Comparator<T> comparator) {
        for (int i = array.length - 1; i >= 0; i--) {
            for (int j = 0; j < i; j++) {
                int compareResult = comparator.compare(array[j], array[j + 1]);
                if (compareResult > 0) {
                    T temp = array[j];
                    array[j] = array[j + 1];
                    array[j + 1] = temp;
                }
            }
        }
    }

    public static void main(String[] args) {
        User[] users = {
                new User(10, "Марсель", "Сидиков", 28),
                new User(15, "Айрат", "Мухутдинов", 23),
                new User(2, "Виктор", "Евлампьев", 24),
                new User(1, "Ринат", "Сафин", 24),
                new User(6, "Алия", "Сидиков", 21),
                new User(12, "Зульфат", "Мухутдинов", 29)
        };

        ToIntFunction<User> extractAge = value -> value.getAge();

        Comparator<User> userComparator = Comparator.comparingInt(user -> user.getAge());

        sort(users, userComparator);

        System.out.println(Arrays.toString(users));
    }

    public static <T> Comparator<T> comparingInt(ToIntFunction<? super T> keyExtractor) {
        return (Comparator<T>)
                (c1, c2) -> Integer.compare(keyExtractor.applyAsInt(c1), keyExtractor.applyAsInt(c2));
    }
}
