package com.akvelon.anonymous;

/**
 * 08.04.2022
 * 03. Anonymous and Lambda
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public interface Button {
    void click();

    void setText(String text);
}
