package com.akvelon.lambda;

/**
 * 08.04.2022
 * 03. Anonymous and Lambda
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public interface LineProcessFunction {
    String apply(String line);
}
