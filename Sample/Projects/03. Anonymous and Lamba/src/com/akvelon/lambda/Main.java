package com.akvelon.lambda;

import java.util.Arrays;
import java.util.List;

/**
 * 08.04.2022
 * 03. Anonymous and Lambda
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public class Main {
    public static void main(String[] args) {
        List<String> lines = Arrays.asList("hello, how", "marsel & no marsel", "how are you", " go go!");

//        LineProcessFunction functionForReplaceSpaces = new LineProcessFunction() {
//            @Override
//            public String apply(String line) {
//                return line.replace(" ", "#");
//            }
//        };
//
//        LineProcessFunction functionForRemoveSpace = new LineProcessFunction() {
//
//            private String someField;
//
//            @Override
//            public String apply(String line) {
//                this.someField = "hello";
//                return line.replaceAll(" ", "");
//            }
//        };


        System.out.println(LinesProcessor
                .process(lines, (x, y) -> x + y));

        System.out.println(LinesProcessor
                .process(lines, line -> line.replaceAll(" ", "")));
    }
}
