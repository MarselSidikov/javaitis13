package com.akvelon.lambda;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * 08.04.2022
 * 03. Anonymous and Lambda
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public class LinesProcessor {
    public static List<String> process(List<String> lines, LineProcessFunction function) {
        List<String> result = new ArrayList<>();
        for (String line : lines) {
            result.add(function.apply(line));
        }

        return result;
    };

    public static List<String> process(List<String> lines, TwoLinesProcessFunction function) {
        List<String> result = new ArrayList<>();
        for (int i = 0; i < lines.size(); i += 2) {
            result.add(function.apply(lines.get(i), lines.get(i + 1)));
        }
        return result;
    }
}
