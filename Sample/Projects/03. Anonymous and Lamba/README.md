## Hard with Lambda

1. Чтобы сравнивать два объекта, нам нужно:

- правило, по которому следует сравнивать

```java
public interface Comparator<T> {
    int compare(T o1, T o2);
}
```

По этому интерфейсу можно создать лямбда выражения для сравнения объектов:

```
Comparator<User> userComparator = (o1, o2) -> o1.getAge() - o2.getAge();
```

В Java уже предусмотрен удобный метод для сравнения int-ов:

```
Comparator<User> userComparator = (o1, o2) -> Integer.compare(o1.getAge(), o2.getAge());
```

По сути, нам нужно просто объяснить Java как вытащить целое число из User:

```java
public interface ToIntFunction<T> {
    int applyAsInt(T value);
}
```

Нам осталось только объяснить, что из User-а нужно вытащить возраст:

```
ToIntFunction<User> getAgeFromUser = user -> user.getAge();
```

Объединим оба пункта:

```
Comparator<User> userComparator = (o1, o2) -> Integer.compare(getAgeFromUser(o1), getAgeFromUser(o2));
```

Запись выше можно получить из записи ниже:

```
return (Comparator<T>)
                (c1, c2) -> Integer.compare(keyExtractor.applyAsInt(c1), keyExtractor.applyAsInt(c2));
```

## Стандартные функциональные интерфейсы

* `Function<T, R>` - `R apply(T t)`
* `Predicate<T>` - `boolean test(T t)`
* `Consumer<T>` - `void accept(T t)`

## Method Reference

* [Документация](https://docs.oracle.com/javase/tutorial/java/javaOO/methodreferences.html)
* [Примеры](https://javatechonline.com/method-reference-java-8/)