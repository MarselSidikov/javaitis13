package com.akvelon.example01;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.Arrays;

public class Main {

    public static void printFieldsOfObject(Object object) {
        Class<?> aClass = object.getClass();

        Field[] fields = aClass.getDeclaredFields();

        for (Field field : fields) {
            System.out.println(Modifier.isPublic(field.getModifiers()) + " " + field.getType() + " " + field.getName());
        }
    }

    public static void printMethodsOfObject(Object object) {
        Class<?> aClass = object.getClass();

        Method[] methods = aClass.getDeclaredMethods();

        for (Method method : methods) {
            System.out.println(method.getReturnType() + " " + method.getName() + " " + Arrays.toString(method.getParameterTypes()));
        }
    }

    public static void printConstructorsOfObject(Object object) {
        Class<?> aClass = object.getClass();

        Constructor<?>[] constructors = aClass.getDeclaredConstructors();

        for (Constructor<?> constructor : constructors) {
            System.out.println(Arrays.toString(constructor.getParameterTypes()));
        }
    }

    public static void main(String[] args) {
	    PasswordValidator validator = new PasswordValidator(10);
        printConstructorsOfObject(validator);
    }
}
