package com.akvelon.framework02;

/**
 * 21.04.2022
 * 10. Reflection
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public class Main {
    public static void main(String[] args) {
        ObjectCreator objectCreator = new ObjectCreator();
        Document document = objectCreator.create(Document.class);
        Bill bill = objectCreator.create(Bill.class);
        System.out.println(document);
        System.out.println(bill);
    }
}
