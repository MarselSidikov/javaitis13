package com.akvelon.framework02;

import java.util.StringJoiner;

/**
 * 21.04.2022
 * 10. Reflection
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public class Bill {
    @DefaultValue(value = "12343")
    private int subscriberNo;
    @DefaultValue(value = "86.7")
    private double sum;

    @Override
    public String toString() {
        return new StringJoiner(", ", Bill.class.getSimpleName() + "[", "]")
                .add("subscriberNo=" + subscriberNo)
                .add("sum=" + sum)
                .toString();
    }
}
