package com.akvelon.framework02;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * 21.04.2022
 * 10. Reflection
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public class ObjectCreator {
    public <T> T create(Class<T> aClass) {

        try {
            Constructor<T> constructor = aClass.getConstructor();

            T result = constructor.newInstance();
            processAnnotations(result);
            return result;

        } catch (ReflectiveOperationException e) {
            throw new IllegalArgumentException(e);
        }
    }

    private <T> void processAnnotations(T object) {
        Class<T> documentClass = (Class<T>) object.getClass();

        Field[] fields = documentClass.getDeclaredFields();

        for (Field field : fields) {
            DefaultValue defaultValue = field.getAnnotation(DefaultValue.class);

            if (defaultValue != null) {
                String value = defaultValue.value();
                Object typedValue = getTypedValue(value, field.getType());
                field.setAccessible(true);
                try {
                    field.set(object, typedValue);
                } catch (IllegalAccessException e) {
                    throw new IllegalArgumentException(e);
                }
            }
        }
    }

    private Object getTypedValue(String arg, Class<?> sourceType) {
        if (sourceType.equals(String.class)) {
            return arg;
        }
        if (sourceType.equals(Boolean.TYPE)) {
            return Boolean.parseBoolean(arg);
        } else if (sourceType.equals(Integer.TYPE)) {
            return Integer.parseInt(arg);
        } else if (sourceType.equals(LocalDate.class)) {
            return LocalDate.parse(arg);
        } else if (sourceType.equals(Double.TYPE)) {
            return Double.parseDouble(arg);
        } else return null;
    }
}
