package com.akvelon.framework02;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.StringJoiner;

/**
 * 21.04.2022
 * 10. Reflection
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public class Document {
    @DefaultValue(value = "Hello!")
    private String title;
    @DefaultValue(value = "2022-10-02")
    private LocalDate createdAt;
    @DefaultValue(value = "false")
    private boolean isProcessed;

    @Override
    public String toString() {
        return new StringJoiner(", ", Document.class.getSimpleName() + "[", "]")
                .add("title='" + title + "'")
                .add("createdAt=" + createdAt)
                .add("isProcessed=" + isProcessed)
                .toString();
    }
}
