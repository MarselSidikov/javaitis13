package com.akvelon.framework01;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

/**
 * 21.04.2022
 * 10. Reflection
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public class ObjectCreator {
    public <T> T create(Class<T> aClass, Object... args) {
        Class<?>[] paramsTypes = new Class<?>[args.length];
        int index = 0;
        for (Object arg : args) {
            paramsTypes[index] = resolveType(arg);
            index++;
        }

        try {
            Constructor<T> constructor = aClass.getConstructor(paramsTypes);
            return constructor.newInstance(args);
        } catch (ReflectiveOperationException e) {
            throw new IllegalArgumentException(e);
        }
    }

    private Class<?> resolveType(Object arg) {
        if (arg.getClass().equals(Boolean.class)) {
            return Boolean.TYPE;
        } else if (arg.getClass().equals(Integer.class)) {
            return Integer.TYPE;
        } else if (arg.getClass().equals(Double.class)) {
            return Double.TYPE;
        } else {
            return arg.getClass();
        }
    }
}
