package com.akvelon.framework01;

import java.util.StringJoiner;

/**
 * 21.04.2022
 * 10. Reflection
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public class Bill {
    private int subscriberNo;
    private double sum;

    public Bill(int subscriberNo, double sum) {
        this.subscriberNo = subscriberNo;
        this.sum = sum;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Bill.class.getSimpleName() + "[", "]")
                .add("subscriberNo=" + subscriberNo)
                .add("sum=" + sum)
                .toString();
    }
}
