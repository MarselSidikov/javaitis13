package com.akvelon.framework01;

import java.time.LocalDateTime;

/**
 * 21.04.2022
 * 10. Reflection
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public class Main {
    public static void main(String[] args) {
        ObjectCreator creator = new ObjectCreator();
        Document document = creator.create(Document.class, "Справка", LocalDateTime.now(), true);
        Bill bill = creator.create(Bill.class, 1123231, 89.5);
        System.out.println(document);
        System.out.println(bill);
    }
}
