package com.akvelon.repositories;

import com.akvelon.models.Pay;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import javax.sql.DataSource;
import java.time.LocalDate;
import java.util.*;

/**
 * 04.05.2022
 * 14. Jdbc Example
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public class PaysRepositoryJdbcTemplateImpl implements PaysRepository {

    //language=SQL
    private static final String SQL_INSERT = "insert into pay (date, description) values (?, ?);";

    //language=SQL
    private static final String SQL_SELECT_BY_ID = "select * from pay where id = :id;";

    //language=SQL
    private static final String SQL_SELECT_ALL = "select * from pay;";

    //language=SQL
    private static final String SQL_UPDATE_BY_ID = "update pay set date = :date, description = :description where id = :id;";

    private final NamedParameterJdbcTemplate template;

    public PaysRepositoryJdbcTemplateImpl(DataSource dataSource) {
        this.template = new NamedParameterJdbcTemplate(dataSource);
    }

    private static final RowMapper<Pay> payRowMapper = (row, rowNumber) -> Pay.builder()
            .date(LocalDate.parse(row.getString("date")))
            .description(row.getString("description"))
            .id(row.getLong("id"))
            .build();

    @Override
    public void save(Pay entity) {
        // TODO: реализовать, Homework13
    }

    @Override
    public void update(Pay entity) {
        Map<String, Object> params = new HashMap<>();
        params.put("description", entity.getDescription());
        params.put("date", entity.getDate().toString());
        params.put("id", entity.getId());
        template.update(SQL_UPDATE_BY_ID, params);
    }

    @Override
    public Optional<Pay> findById(Long id) {
        try {
            return Optional.ofNullable(template.queryForObject(
                    SQL_SELECT_BY_ID,
                    Collections.singletonMap("id", id),
                    payRowMapper));
        } catch (EmptyResultDataAccessException e) {
            return Optional.empty();
        }
    }

    @Override
    public List<Pay> findAll() {
        return template.query(SQL_SELECT_ALL, payRowMapper);
    }

    @Override
    public void delete(Pay entity) {
        // TODO: реализовать, Homework13
    }

    @Override
    public void deleteById(Long aLong) {
        // TODO: реализовать, Homework13
    }
}
