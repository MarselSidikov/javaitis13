package com.akvelon;

import com.akvelon.models.Pay;
import com.akvelon.repositories.PaysRepository;
import com.akvelon.repositories.PaysRepositoryJdbcTemplateImpl;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

import java.time.LocalDate;

/**
 * 06.05.2022
 * 14. Jdbc Example
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public class Main2 {
    public static void main(String[] args) {
        HikariConfig config = new HikariConfig();
        config.setJdbcUrl("jdbc:postgresql://localhost:5432/akvelon_db");
        config.setUsername("postgres");
        config.setPassword("qwerty007");
        config.setMaximumPoolSize(20);

        HikariDataSource dataSource = new HikariDataSource(config);

        PaysRepository paysRepository = new PaysRepositoryJdbcTemplateImpl(dataSource);

        System.out.println(paysRepository.findById(107261L));
        System.out.println(paysRepository.findAll());

        Pay pay = paysRepository.findById(107261L).orElseThrow(IllegalArgumentException::new);
        pay.setDescription("ОБНОВЛЕННЫЙ ПЛАТЕЖ");
        pay.setDate(LocalDate.now().plusDays(10));
        paysRepository.update(pay);
    }
}
