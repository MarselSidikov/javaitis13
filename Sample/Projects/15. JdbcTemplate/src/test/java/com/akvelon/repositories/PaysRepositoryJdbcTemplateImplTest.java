package com.akvelon.repositories;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;

import javax.sql.DataSource;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.*;

/**
 * 12.05.2022
 * 15. JdbcTemplate
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
class PaysRepositoryJdbcTemplateImplTest {

    private PaysRepositoryJdbcTemplateImpl paysRepository;

    @BeforeEach
    void setUp() {
        DataSource dataSource = new EmbeddedDatabaseBuilder()
                .setType(EmbeddedDatabaseType.H2)
                .addScript("schema.sql")
                .addScript("data.sql")
                .build();

        paysRepository = new PaysRepositoryJdbcTemplateImpl(dataSource);
    }

    @Test
    public void find_all_return_correct_result() {
        assertThat(paysRepository.findAll(), hasSize(3));
    }
}